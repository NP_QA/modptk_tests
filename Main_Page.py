import itertools
import logging
import random

import allure
from allure_commons.types import AttachmentType
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

from BaseApp import BasePage


class MainPageLocators:
    # locators_for_main_page_module
    LOCATOR_PTK_SETTINGS_BUTTON_TEXT = (By.XPATH, '//*[contains(text(), "Параметры")]')
    LOCATOR_PTK_FIND_PROJECT_BY_3_LAST_NUMBERS = (By.XPATH, '//*[@id="root"]/div/div[2]/div[2]/a[1]/div[1]/div[1]/p')
    LOCATOR_PTK_SEARCHING_FIELD = (By.XPATH, '//*[@placeholder="Поиск..."]')
    LOCATOR_PTK_TABLE_INVESTMENT_PROGRAMMS = (By.XPATH, '/html/body/div[3]/div[3]/div/div')
    LOCATOR_PTK_CLICK_VERSION_SELECTOR = (By.XPATH, '//div[4]/button/span[1]/span[1]')
    LOCATOR_PTK_CREATE_NEW_VERSION = (By.XPATH, '//*[contains(text(), "Создать новую версию")]')
    LOCATOR_PTK_SELECT_OLD_VERSION = (
        By.XPATH, '//div[2]/div/li/div[1]')
    LOCATOR_PTK_GET_TASK_NAME = (By.XPATH, '//*[@id="root"]/div[1]/div[2]/div[2]/a[1]/div[1]/div[2]/p')
    LOCATOR_PTK_GET_PROJECT_FILIAL = (By.XPATH, '//*[@id="root"]/div[1]/div[2]/div[2]/a[1]/div[1]/div[3]/p')
    LOCATOR_PTK_LIST_OF_ALL_PROJECTS = (By.XPATH, '//*[@id="root"]/div/div[2]/div[2]')
    LOCATOR_PTK_SETTINGS_BUTTON = (By.XPATH, '//span[contains(text(), "Параметры")]')
    LOCATOR_PTK_INVESTMENT_PARAMETERS = (By.XPATH,
                                         '//div[3]/div[3]/div/div/div[1]')
    LOCATOR_PTK_INVESTMENT_INDEX_DEF = (By.XPATH, '//div[@class="MuiGrid-root MuiGrid-container MuiGrid-spacing-xs-2"]')
    LOCATOR_PTK_BODY_OF_MAIN_PAGE = (By.XPATH, '/html/body/div[2]/div[1]')
    LOCATOR_PTK_SKELETON_OF_MAIN_PAGE = (By.XPATH, '/html/body')
    LOCATOR_PTK_CLICK_ON_PAGE_UPPER = (By.XPATH, '//div[@class="btn-up-icon"]')
    LOCATOR_PTK_THREE_DOTS_ICON = (By.XPATH, '//span[@class="three-dots-icon"]')
    LOCATOR_PTK_EDIT_VERSION = (By.XPATH, '//*[@class="Versioning_versioning__item-select__1l4_h select-version-icon"]')
    LOCATOR_PTK_VERSION_HOVER = (By.XPATH, '/html/body/div[2]/div[3]/div/div/div[2]/div[1]')
    LOCATOR_PTK_CLICK_ON_RENAME_BUTTON = (By.XPATH, '//*[contains(text(), "Переименовать")]')
    LOCATOR_PTK_HOVER_RENAMED = (By.XPATH, '/html/body/div[2]/div[3]/div/div/div[2]/div[1]/li/div[1]')
    LOCATOR_PTK_INPUT_RENAME = (By.XPATH, '/html/body/div[2]/div[3]/div/div/div[2]/div[1]/li/div[1]/div/input')
    LOCATOR_PTK_FIND_RENAME_ALERT = (By.XPATH, '//*[@role="alert"]')
    LOCATOR_PTK_NEW_VERSION_NAME = (By.XPATH, '/html/body/div[2]/div[3]/div/div/div[2]/div[1]/li/div[1]/div/input')
    LOCATOR_PTK_SELECT_NON_ACTUAL_VERSION = (
        By.CSS_SELECTOR, 'div.Versioning_versioning__list__20L62 > div:nth-child(2) > li')
    LOCATOR_PTK_SELECT_ACTUAL_VERSION = (By.XPATH, '//*[contains(text(), "Текущая версия")]')
    LOCATOR_PTK_CLICK_RASSCHET_BUTTON = (By.XPATH, '//*[contains(text(), "Рассчитать")]')
    LOCATOR_PTK_CLICK_PERSONAL_PROJECTS = (By.XPATH, '//*[contains(text(), "Личные проекты")]')
    LOCATOR_PTK_CREATE_NEW_PERSONAL_PROJECT = (By.XPATH, '//*[contains(text(), "Новый проект +")]')
    LOCATOR_PTK_CREATE_NEW_PERSONAL_PROJECT_SAVE_BUTTON = (By.XPATH, '//*[@disabled]//span[contains(text(), "Сохранить")]')


class SearchHelperMainPage(BasePage):
    # locators_for_main_page_functions
    def assert_table_of_investments(self):
        try:
            assert self.find_element(MainPageLocators.LOCATOR_PTK_TABLE_INVESTMENT_PROGRAMMS) and self.find_element(
                MainPageLocators.LOCATOR_PTK_INVESTMENT_PARAMETERS) and self.find_element(
                MainPageLocators.LOCATOR_PTK_INVESTMENT_INDEX_DEF)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"List of projects not found"}}')
            with allure.step('Поиск инструментов инвестирования. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск инструментов инвестирования. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"List of projects found"}}')
            with allure.step('Поиск инструментов инвестирования. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск инструментов инвестирования. Успешный',
                              attachment_type=AttachmentType.PNG)

    def personal_project_save_button(self):
        try:
            assert self.find_element(MainPageLocators.LOCATOR_PTK_CREATE_NEW_PERSONAL_PROJECT_SAVE_BUTTON)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"List of projects not found"}}')
            with allure.step('Найдена заблокированная кнопка сохранения проекта. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на кнопку "Личные проекты". Успешный',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"List of projects found"}}')
            with allure.step('Найдена заблокированная кнопка сохранения проекта. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Найдена заблокированная кнопка сохранения проекта. Успешный',
                              attachment_type=AttachmentType.PNG)

    def click_on_personal_projects(self):
        try:
            assert self.find_element(MainPageLocators.LOCATOR_PTK_CLICK_PERSONAL_PROJECTS)
            self.find_clickable_element(MainPageLocators.LOCATOR_PTK_CLICK_PERSONAL_PROJECTS)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"List of projects not found"}}')
            with allure.step('Клик на кнопку "Личные проекты". Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на кнопку "Личные проекты". Успешный',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"List of projects found"}}')
            with allure.step('Клик на кнопку "Личные проекты". Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на кнопку "Личные проекты". Успешный',
                              attachment_type=AttachmentType.PNG)

    def create_new_personal_project(self):
        try:
            assert self.find_element(MainPageLocators.LOCATOR_PTK_CREATE_NEW_PERSONAL_PROJECT)
            self.find_clickable_element(MainPageLocators.LOCATOR_PTK_CREATE_NEW_PERSONAL_PROJECT)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"List of projects not found"}}')
            with allure.step('Клик на кнопку "Новый проект +". Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на кнопку "Новый проект +". Успешный',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"List of projects found"}}')
            with allure.step('Клик на кнопку "Новый проект +". Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на кнопку "Новый проект +". Успешный',
                              attachment_type=AttachmentType.PNG)

    def main_page_click_settings_button(self):
        try:
            assert self.find_element(MainPageLocators.LOCATOR_PTK_SETTINGS_BUTTON).text == "Параметры"
            self.find_clickable_element(
                MainPageLocators.LOCATOR_PTK_SETTINGS_BUTTON)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"List of projects not found"}}')
            with allure.step('Клик на кнопку настроек на главной странице. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на кнопку настроек на главной странице. Успешный',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"List of projects found"}}')
            with allure.step('Клик на кнопку настроек на главной странице. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на кнопку настроек на главной странице. Успешный',
                              attachment_type=AttachmentType.PNG)

    def assert_projects_id_not_equal(self):
        try:
            assert self.find_element(
                MainPageLocators.LOCATOR_PTK_FIND_PROJECT_BY_3_LAST_NUMBERS).text != project_id_new_version
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Проверка того, идентификаторы проектов не одинаковы. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка того, что выбрана СД версия. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Проверка того, идентификаторы проектов не одинаковы. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка того, что выбрана СД версия. Успешный',
                              attachment_type=AttachmentType.PNG)

    def get_project_id(self):
        try:
            global get_project_id
            assert self.find_element(MainPageLocators.LOCATOR_PTK_FIND_PROJECT_BY_3_LAST_NUMBERS)
            get_project_id = self.find_element(
                MainPageLocators.LOCATOR_PTK_FIND_PROJECT_BY_3_LAST_NUMBERS).get_attribute(
                'innerText')
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Получение идентификатора проекта. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Получение идентификатора проекта. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Получение идентификатора проекта. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='ППолучение идентификатора проекта. Успешный',
                              attachment_type=AttachmentType.PNG)

    def select_old_version(self):
        try:
            assert self.find_element(MainPageLocators.LOCATOR_PTK_SELECT_OLD_VERSION)
            self.find_clickable_element(MainPageLocators.LOCATOR_PTK_SELECT_OLD_VERSION)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"List of projects not found"}}')
            with allure.step('Выбор старой версии. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбор старой версии. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"List of projects found"}}')
            with allure.step('Выбор старой версии. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбор старой версии. Успешный',
                              attachment_type=AttachmentType.PNG)

    def click_on_body_of_main_page(self):
        try:
            assert self.find_element(MainPageLocators.LOCATOR_PTK_BODY_OF_MAIN_PAGE)
            self.find_clickable_element(MainPageLocators.LOCATOR_PTK_BODY_OF_MAIN_PAGE)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Клик на тело главной страницы. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на тело главной страницы. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Клик на тело главной страницы. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на тело главной страницы. Успешный',
                              attachment_type=AttachmentType.PNG)

    def click_version_selector(self):
        try:
            assert self.find_element(MainPageLocators.LOCATOR_PTK_CLICK_VERSION_SELECTOR)
            self.find_clickable_element(MainPageLocators.LOCATOR_PTK_CLICK_VERSION_SELECTOR)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"List of projects not found"}}')
            with allure.step('Клик на селектор версий. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на селектор версий. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"List of projects found"}}')
            with allure.step('Клик на селектор версий. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на селектор версий. Успешный',
                              attachment_type=AttachmentType.PNG)

    def find_list_of_projects(self):
        try:
            assert self.find_elements(MainPageLocators.LOCATOR_PTK_LIST_OF_ALL_PROJECTS)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"List of projects not found"}}')
            with allure.step('Поиск списка проектов. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск списка проектов. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"List of projects found"}}')
            with allure.step('Поиск списка проектов. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск списка проектов. Успешный',
                              attachment_type=AttachmentType.PNG)

    def click_three_dots_icon(self):
        try:
            self.find_clickable_element(MainPageLocators.LOCATOR_PTK_THREE_DOTS_ICON)
            assert self.find_element(MainPageLocators.LOCATOR_PTK_SETTINGS_BUTTON_TEXT).text == "Параметры"
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Клик на кнопку с троеточием. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на кнопку с троеточием. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Клик на кнопку с троеточием. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на кнопку с троеточием. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def get_project_id_new_version(self):
        try:
            global project_id_new_version
            assert self.find_element(
                MainPageLocators.LOCATOR_PTK_FIND_PROJECT_BY_3_LAST_NUMBERS)
            project_id_new_version = self.find_element(
                MainPageLocators.LOCATOR_PTK_FIND_PROJECT_BY_3_LAST_NUMBERS).get_attribute('innerText')
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Получение идентификатора проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Получение идентификатора проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Получение идентификатора проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Получение идентификатора проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def scroll_page_down(self):
        try:
            assert self.find_element(MainPageLocators.LOCATOR_PTK_SKELETON_OF_MAIN_PAGE)
            scroll_page_down = self.find_element(MainPageLocators.LOCATOR_PTK_SKELETON_OF_MAIN_PAGE).send_keys(
                Keys.PAGE_DOWN)
            scroll_page_down = self.find_element(MainPageLocators.LOCATOR_PTK_SKELETON_OF_MAIN_PAGE).send_keys(
                Keys.PAGE_DOWN)
            scroll_page_down = self.find_element(MainPageLocators.LOCATOR_PTK_SKELETON_OF_MAIN_PAGE).send_keys(
                Keys.PAGE_DOWN)
            scroll_page_down = self.find_element(MainPageLocators.LOCATOR_PTK_SKELETON_OF_MAIN_PAGE).send_keys(
                Keys.PAGE_DOWN)
            scroll_page_down = self.find_element(MainPageLocators.LOCATOR_PTK_SKELETON_OF_MAIN_PAGE).send_keys(
                Keys.PAGE_DOWN)
            scroll_page_down = self.find_element(MainPageLocators.LOCATOR_PTK_SKELETON_OF_MAIN_PAGE).send_keys(
                Keys.PAGE_DOWN)
            scroll_page_down = self.find_element(MainPageLocators.LOCATOR_PTK_SKELETON_OF_MAIN_PAGE).send_keys(
                Keys.PAGE_DOWN)
            scroll_page_down = self.find_element(MainPageLocators.LOCATOR_PTK_SKELETON_OF_MAIN_PAGE).send_keys(
                Keys.PAGE_DOWN)
            scroll_page_down = self.find_element(MainPageLocators.LOCATOR_PTK_SKELETON_OF_MAIN_PAGE).send_keys(
                Keys.PAGE_DOWN)
            scroll_page_down = self.find_element(MainPageLocators.LOCATOR_PTK_SKELETON_OF_MAIN_PAGE).send_keys(
                Keys.PAGE_DOWN)
            scroll_page_down = self.find_element(MainPageLocators.LOCATOR_PTK_SKELETON_OF_MAIN_PAGE).send_keys(
                Keys.PAGE_DOWN)
            scroll_page_down = self.find_element(MainPageLocators.LOCATOR_PTK_SKELETON_OF_MAIN_PAGE).send_keys(
                Keys.PAGE_DOWN)
            scroll_page_down = self.find_element(MainPageLocators.LOCATOR_PTK_SKELETON_OF_MAIN_PAGE).send_keys(
                Keys.PAGE_DOWN)
            scroll_page_down = self.find_element(MainPageLocators.LOCATOR_PTK_SKELETON_OF_MAIN_PAGE).send_keys(
                Keys.PAGE_DOWN)
            scroll_page_down = self.find_element(MainPageLocators.LOCATOR_PTK_SKELETON_OF_MAIN_PAGE).send_keys(
                Keys.PAGE_DOWN)
            scroll_page_down = self.find_element(MainPageLocators.LOCATOR_PTK_SKELETON_OF_MAIN_PAGE).send_keys(
                Keys.PAGE_DOWN)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Authorization failed"}}')
            with allure.step('Проверка авторизации. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Проверка авторизации. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Authorization granted"}}')
            with allure.step('Проверка авторизации. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Проверка авторизации. Успешный',
                              attachment_type=AttachmentType.PNG)

    def click_on_page_upper(self):
        try:
            assert self.find_element(MainPageLocators.LOCATOR_PTK_CLICK_ON_PAGE_UPPER)
            self.find_clickable_element(MainPageLocators.LOCATOR_PTK_CLICK_ON_PAGE_UPPER)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Page upper not clicked"}}')
            with allure.step('Клик на кнопку подъема страницы. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на кнопку подъема страницы. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Page upper clicked"}}')
            with allure.step('Клик на кнопку подъема страницы. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на кнопку подъема страницы. Успешный',
                              attachment_type=AttachmentType.PNG)

    def get_task_name(self):
        try:
            assert self.find_element(MainPageLocators.LOCATOR_PTK_GET_TASK_NAME).get_attribute('innerText')
            global get_task_name
            get_inner_task_name = self.find_element(MainPageLocators.LOCATOR_PTK_GET_TASK_NAME).get_attribute(
                'innerText')
            get_task_name = (str(get_inner_task_name)[:-3])
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Получение названия проекта. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Получение названия проекта. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Получение названия проекта. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Получение названия проекта. Успешный',
                              attachment_type=AttachmentType.PNG)

    def assert_project_task_name(self):
        try:
            assert self.find_element(MainPageLocators.LOCATOR_PTK_GET_TASK_NAME)
            format_task_name_text = self.find_element(MainPageLocators.LOCATOR_PTK_GET_TASK_NAME).get_attribute(
                'innerText')
            minus_three_symbols = (str(format_task_name_text)[:-3])
            assert get_task_name == minus_three_symbols
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Проверка соответствия текста найденной задачи с искомой. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка соответствия текста найденной задачи с искомой. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Проверка соответствия текста найденной задачи с искомой. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка соответствия текста найденной задачи с искомой. Успешный',
                              attachment_type=AttachmentType.PNG)


    def assert_project_by_1_word(self):
        try:
            assert self.find_element(MainPageLocators.LOCATOR_PTK_GET_TASK_NAME).text == 'Заменой'
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Поиск проекта по одному слову. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск проекта по одному слову. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Поиск проекта по одному слову. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск проекта по одному слову. Успешный',
                              attachment_type=AttachmentType.PNG)

    def get_project_filial(self):
        try:
            global get_project_filial
            assert self.find_element(MainPageLocators.LOCATOR_PTK_GET_PROJECT_FILIAL)
            get_project_filial = self.find_element(MainPageLocators.LOCATOR_PTK_GET_PROJECT_FILIAL).get_attribute(
                'innerText')
            assert self.find_element(MainPageLocators.LOCATOR_PTK_GET_PROJECT_FILIAL).get_attribute(
                'innerText')
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Получение значения филиала проекта. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Получение значения филиала проекта. Успешный',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Получение значения филиала проекта. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Получение значения филиала проекта. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False

    def searching_field_project_filial(self):
        try:
            self.find_element(MainPageLocators.LOCATOR_PTK_SEARCHING_FIELD).send_keys(get_project_filial)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Ввод филиала проекта в поисковое поле. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Ввод филиала проекта в поисковое поле. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Ввод филиала проекта в поисковое поле. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Ввод филиала проекта в поисковое поле. Успешный',
                              attachment_type=AttachmentType.PNG)

    def searching_space_at_the_start_field_project_filial(self):
        try:
            assert self.find_element(MainPageLocators.LOCATOR_PTK_SEARCHING_FIELD).send_keys(Keys.SPACE,
                                                                                             get_project_filial)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Ввод филиала проекта в поисковое поле. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Ввод филиала проекта в поисковое поле. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Поиск проекта с пробелом в начале. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск проекта с пробелом в начале. Успешный',
                              attachment_type=AttachmentType.PNG)

    def searching_space_at_the_end_field_project_filial(self):
        try:
            assert self.find_element(MainPageLocators.LOCATOR_PTK_SEARCHING_FIELD).send_keys(get_project_filial,
                                                                                             Keys.SPACE)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Поиск проекта с пробелом в конце. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск проекта с пробелом в конце. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Поиск проекта с пробелом в конце. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск проекта с пробелом в конце. Успешный',
                              attachment_type=AttachmentType.PNG)

    def searching_filial_with_incorrect_filling(self):
        try:
            assert self.find_element(
                MainPageLocators.LOCATOR_PTK_SEARCHING_FIELD).send_keys('МЭС Урала рандомное заполнение')
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Поиск проекта по филиалу - некорректное заполнение. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск проекта по филиалу - некорректное заполнение. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Поиск проекта по филиалу - некорректное заполнение. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск проекта по филиалу - некорректное заполнение. Успешный',
                              attachment_type=AttachmentType.PNG)

    def assert_project_filial(self):
        try:
            assert self.find_element(
                MainPageLocators.LOCATOR_PTK_GET_PROJECT_FILIAL).text == get_project_filial
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Сравнение филиалов найденного проекта с искомым. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Сравнение филиалов найденного проекта с искомым. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Сравнение филиалов найденного проекта с искомым. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Сравнение филиалов найденного проекта с искомым. Успешный',
                              attachment_type=AttachmentType.PNG)

    def get_task_name_10_symbols(self):
        try:
            global last_10_symbols
            assert self.find_element(MainPageLocators.LOCATOR_PTK_GET_TASK_NAME).get_attribute(
                'innerText')
            get_task_name_10_symbols = self.find_element(MainPageLocators.LOCATOR_PTK_GET_TASK_NAME).get_attribute(
                'innerText')
            last_10_symbols = (str(get_task_name_10_symbols)[:10])
            assert self.find_element(MainPageLocators.LOCATOR_PTK_GET_TASK_NAME).get_attribute(
                'innerText')
            assert (str(get_task_name_10_symbols)[:10])
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Получение десяти символов проекта. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Получение десяти символов проекта. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Получение десяти символов проекта. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Получение десяти символов проекта. Успешный',
                              attachment_type=AttachmentType.PNG)

    def searching_field_task_name_10_symbols(self):
        try:
            self.find_element(MainPageLocators.LOCATOR_PTK_SEARCHING_FIELD).send_keys(last_10_symbols)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Ввод десяти символов проекта в поисковое поле. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Ввод десяти символов проекта в поисковое поле. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Ввод десяти символов проекта в поисковое поле. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Ввод десяти символов проекта в поисковое поле. Успешный',
                              attachment_type=AttachmentType.PNG)

    def assert_task_name_10_symbols(self):
        try:
            assert self.find_element(
                MainPageLocators.LOCATOR_PTK_GET_TASK_NAME)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Сравнение найденного проекта с искомым. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Сравнение найденного проекта с искомым. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Сравнение найденного проекта с искомым. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Сравнение найденного проекта с искомым. Успешный',
                              attachment_type=AttachmentType.PNG)

    def get_task_name_20_symbols(self):
        try:
            global last_20_symbols
            assert self.find_element(MainPageLocators.LOCATOR_PTK_GET_TASK_NAME)
            get_task_name_20_symbols = self.find_element(MainPageLocators.LOCATOR_PTK_GET_TASK_NAME).get_attribute(
                'innerText')
            last_20_symbols = (str(get_task_name_20_symbols)[:20])
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Получение 20 символов из названия проекта. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Получение 20 символов из названия проекта. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Получение 20 символов из названия проекта. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Получение 20 символов из названия проекта. Успешный',
                              attachment_type=AttachmentType.PNG)

    def searching_field_task_name_20_symbols(self):
        try:
            self.find_element(MainPageLocators.LOCATOR_PTK_SEARCHING_FIELD).send_keys(last_20_symbols)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"List of projects not found"}}')
            with allure.step('Поиск проекта по двадцати символам. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск проекта по двадцати символам. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"List of projects found"}}')
            with allure.step('Поиск проекта по двадцати символам. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск проекта по двадцати символам. Успешный',
                              attachment_type=AttachmentType.PNG)

    def assert_task_name_20_symbols(self):
        try:
            assert self.find_element(
                MainPageLocators.LOCATOR_PTK_GET_TASK_NAME)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"List of projects not found"}}')
            with allure.step('Сравнение найденного проекта с искомым по двадцати символам. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Сравнение найденного проекта с искомым по двадцати символам. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"List of projects found"}}')
            with allure.step('Сравнение найденного проекта с искомым по двадцати символам. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Сравнение найденного проекта с искомым по двадцати символам. Успешный',
                              attachment_type=AttachmentType.PNG)

    def go_to_project_card(self):
        try:
            assert self.find_element(MainPageLocators.LOCATOR_PTK_FIND_PROJECT_BY_3_LAST_NUMBERS)
            self.find_clickable_element(MainPageLocators.LOCATOR_PTK_FIND_PROJECT_BY_3_LAST_NUMBERS)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Переход на карту проекта. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Переход на карту проекта. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Переход на карту проекта. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Переход на карту проекта. Успешный',
                              attachment_type=AttachmentType.PNG)

    def searching_field_custom_searching(self, text):
        try:
            assert self.find_element(MainPageLocators.LOCATOR_PTK_SEARCHING_FIELD).send_keys(text)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Ввод произвольного значения в поле поиска. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Ввод произвольного значения в поле поиска. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Ввод произвольного значения в поле поиска. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Ввод произвольного значения в поле поиска. Успешный',
                              attachment_type=AttachmentType.PNG)

    def searching_field(self):
        try:
            self.find_element(MainPageLocators.LOCATOR_PTK_SEARCHING_FIELD).send_keys(last_3_numbers)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Ввод в поисковое поле трех последних цифр проекта. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Ввод в поисковое поле трех последних цифр проекта. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Ввод в поисковое поле трех последних цифр проекта. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Ввод в поисковое поле трех последних цифр проекта. Успешный',
                              attachment_type=AttachmentType.PNG)

    def fill_searching_field(self, item):
        try:
            assert self.find_element(MainPageLocators.LOCATOR_PTK_SEARCHING_FIELD).send_keys(item)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Ввод в поисковое поле произвольного значения. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Ввод в поисковое поле трех последних цифр проекта. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Ввод в поисковое поле произвольного значения. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Ввод в поисковое поле трех последних цифр проекта. Успешный',
                              attachment_type=AttachmentType.PNG)

    def find_projects_by_3_last_numbers(self):
        try:
            assert self.find_element(
                MainPageLocators.LOCATOR_PTK_FIND_PROJECT_BY_3_LAST_NUMBERS)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Проект по трем последним числам найден. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проект по трем последним числам найден. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Проект по трем последним числам найден. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проект по трем последним числам найден. Успешный',
                              attachment_type=AttachmentType.PNG)

    def find_project_by_4_last_numbers(self):
        try:
            global last_4_numbers
            assert self.find_element(
                MainPageLocators.LOCATOR_PTK_FIND_PROJECT_BY_3_LAST_NUMBERS)
            find_project_by_3_last_numbers = self.find_element(
                MainPageLocators.LOCATOR_PTK_FIND_PROJECT_BY_3_LAST_NUMBERS).get_attribute('innerText')
            assert self.find_element(
                MainPageLocators.LOCATOR_PTK_FIND_PROJECT_BY_3_LAST_NUMBERS).get_attribute('innerText')
            last_4_numbers = (str(find_project_by_3_last_numbers)[-4:])
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Поиск проекта по последним четырем цифрам. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск проекта по последним четырем цифрам. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Поиск проекта по последним четырем цифрам. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск проекта по последним четырем цифрам. Успешный',
                              attachment_type=AttachmentType.PNG)

    def searching_field_4_symbols(self):
        try:
            self.find_element(MainPageLocators.LOCATOR_PTK_SEARCHING_FIELD).send_keys(last_4_numbers)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Ввод в поисковое поле последние четыре цифры. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Ввод в поисковое поле последние четыре цифры. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Ввод в поисковое поле последние четыре цифры. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Ввод в поисковое поле последние четыре цифры. Успешный',
                              attachment_type=AttachmentType.PNG)

    def find_projects_by_4_last_numbers(self):
        try:
            assert self.find_element(
                MainPageLocators.LOCATOR_PTK_FIND_PROJECT_BY_3_LAST_NUMBERS)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Поиск проекта по последним четырем цифрам. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск проекта по последним четырем цифрам. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Поиск проекта по последним четырем цифрам. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск проекта по последним четырем цифрам. Успешный',
                              attachment_type=AttachmentType.PNG)

    def find_full_project_id(self):
        try:
            assert self.find_element(
                MainPageLocators.LOCATOR_PTK_FIND_PROJECT_BY_3_LAST_NUMBERS).get_attribute('innerText')
            global find_full_project_id
            find_full_project_id = self.find_element(
                MainPageLocators.LOCATOR_PTK_FIND_PROJECT_BY_3_LAST_NUMBERS).get_attribute('innerText')
            assert self.find_element(
                MainPageLocators.LOCATOR_PTK_FIND_PROJECT_BY_3_LAST_NUMBERS).get_attribute('innerText')
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Получение полного значения идентификатора проекта. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Получение полного значения идентификатора проекта. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Получение полного значения идентификатора проекта. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Получение полного значения идентификатора проекта. Успешный',
                              attachment_type=AttachmentType.PNG)

    def searching_field_full_id(self):
        try:
            self.find_element(MainPageLocators.LOCATOR_PTK_SEARCHING_FIELD).send_keys(find_full_project_id)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Поиск полного значения идентификатора проекта. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Получение полного значения идентификатора проекта. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Поиск полного значения идентификатора проекта. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Получение полного значения идентификатора проекта. Успешный',
                              attachment_type=AttachmentType.PNG)

    def searching_field_full_id_and_space_at_the_end(self):
        try:
            assert self.find_element(MainPageLocators.LOCATOR_PTK_SEARCHING_FIELD).send_keys(find_full_project_id,
                                                                                             Keys.SPACE)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Поиск полного значения идентификатора с пробелом в конце. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск полного значения идентификатора с пробелом в конце. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Поиск полного значения идентификатора с пробелом в конце. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск полного значения идентификатора с пробелом в конце. Успешный',
                              attachment_type=AttachmentType.PNG)

    def searching_field_full_id_and_space_at_the_start(self):
        try:
            assert self.find_element(MainPageLocators.LOCATOR_PTK_SEARCHING_FIELD).send_keys(Keys.SPACE,
                                                                                             find_full_project_id)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Поиск полного значения идентификатора с пробелом в начале. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск полного значения идентификатора с пробелом в начале. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Поиск полного значения идентификатора с пробелом в начале. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск полного значения идентификатора с пробелом в начале. Успешный',
                              attachment_type=AttachmentType.PNG)

    def double_id(self):
        try:
            assert self.find_element(MainPageLocators.LOCATOR_PTK_SEARCHING_FIELD).send_keys(find_full_project_id,
                                                                                             Keys.SPACE,
                                                                                             find_full_project_id)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Поиск полного значения идентификатора дублированный. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск полного значения идентификатора дублированный. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Поиск полного значения идентификатора дублированный. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск полного значения идентификатора дублированный. Успешный',
                              attachment_type=AttachmentType.PNG)

    def searching_field_full_id_and_double_space_at_the_end(self):
        try:
            assert self.find_element(MainPageLocators.LOCATOR_PTK_SEARCHING_FIELD).send_keys(find_full_project_id,
                                                                                             Keys.SPACE,
                                                                                             Keys.SPACE)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Поиск полного значения идентификатора с двойным пробелом в конце. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск полного значения идентификатора с двойным пробелом в конце. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Поиск полного значения идентификатора с двойным пробелом в конце. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск полного значения идентификатора с двойным пробелом в конце. Успешный',
                              attachment_type=AttachmentType.PNG)

    def assert_project_full_id(self):
        try:
            assert self.find_element(
                MainPageLocators.LOCATOR_PTK_FIND_PROJECT_BY_3_LAST_NUMBERS).text == find_full_project_id
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Проверка того, что проект найден по полному идентификатору. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка того, что проект найден по полному идентификатору. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Проверка того, что проект найден по полному идентификатору. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка того, что проект найден по полному идентификатору. Успешный',
                              attachment_type=AttachmentType.PNG)

    def searching_field_task_name(self):
        try:
            self.find_element(MainPageLocators.LOCATOR_PTK_SEARCHING_FIELD).send_keys(get_task_name)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Ввод названия проекта в поисковое поле. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Ввод названия проекта в поисковое поле. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Ввод названия проекта в поисковое поле. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Ввод названия проекта в поисковое поле. Успешный',
                              attachment_type=AttachmentType.PNG)

    def find_project_by_3_last_numbers(self):
        try:
            global last_3_numbers
            assert self.find_element(
                MainPageLocators.LOCATOR_PTK_FIND_PROJECT_BY_3_LAST_NUMBERS)
            find_project_by_3_last_numbers = self.find_element(
                MainPageLocators.LOCATOR_PTK_FIND_PROJECT_BY_3_LAST_NUMBERS).get_attribute('innerText')
            assert self.find_element(
                MainPageLocators.LOCATOR_PTK_FIND_PROJECT_BY_3_LAST_NUMBERS).get_attribute('innerText')
            last_3_numbers = (str(find_project_by_3_last_numbers)[-3:])
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Поиск проекта по трем последним цифрам. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск проекта по трем последним цифрам. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Поиск проекта по трем последним цифрам. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск проекта по трем последним цифрам. Успешный',
                              attachment_type=AttachmentType.PNG)

    def click_create_new_version(self):
        try:
            assert self.find_clickable_element(MainPageLocators.LOCATOR_PTK_CREATE_NEW_VERSION)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Клик на кнопку создания новой версии. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на кнопку создания новой версии. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Клик на кнопку создания новой версии. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на кнопку создания новой версии. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def click_rasschet_button(self):
        try:
            find_rasschet_button = self.find_element(MainPageLocators.LOCATOR_PTK_SELECT_ACTUAL_VERSION)
            assert self.find_element(MainPageLocators.LOCATOR_PTK_CLICK_RASSCHET_BUTTON)
            self.find_clickable_element(MainPageLocators.LOCATOR_PTK_SELECT_ACTUAL_VERSION)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Клик на кнопку рассчета. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на кнопку рассчета. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Клик на кнопку рассчета. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на кнопку рассчета. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def select_actual_version(self):
        try:
            assert self.find_element(MainPageLocators.LOCATOR_PTK_SELECT_ACTUAL_VERSION)
            self.find_clickable_element(MainPageLocators.LOCATOR_PTK_SELECT_ACTUAL_VERSION)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Выбор актуальной версии проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбор актуальной версии проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Выбор актуальной версии проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбор актуальной версии проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def select_non_actual_version(self):
        try:
            assert self.find_element(MainPageLocators.LOCATOR_PTK_SELECT_NON_ACTUAL_VERSION)
            self.find_clickable_element(MainPageLocators.LOCATOR_PTK_SELECT_NON_ACTUAL_VERSION)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Выбор неактуальной версии проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбор неактуальной версии проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Выбор неактуальной версии проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбор неактуальной версии проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def go_to_rename_version(self):
        try:
            assert self.find_element(MainPageLocators.LOCATOR_PTK_VERSION_HOVER)
            select_version_hover = self.find_element(MainPageLocators.LOCATOR_PTK_VERSION_HOVER)
            hover = ActionChains(self.driver).move_to_element(select_version_hover)
            hover.perform()
            assert self.find_element(MainPageLocators.LOCATOR_PTK_EDIT_VERSION)
            self.find_clickable_element(MainPageLocators.LOCATOR_PTK_EDIT_VERSION)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Переход к переименованию версии. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Переход к переименованию версии. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Переход к переименованию версии. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Переход к переименованию версии. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def click_on_rename_button(self):
        try:
            assert self.find_element(MainPageLocators.LOCATOR_PTK_CLICK_ON_RENAME_BUTTON)
            self.find_clickable_element(MainPageLocators.LOCATOR_PTK_CLICK_ON_RENAME_BUTTON)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Клик на кнопку переименования версии. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Клик на кнопку переименования версии. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на кнопку переименования версии. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def send_keys_to_rename(self):
        try:
            letters_names = (
                "а", "б", "в", "г", "д", "ж", "з", "е", "и", "к", "л", "ы", "н", "к", "о", "щ", "х", "ъ", "й", "ц", "у",
                "ф",
                "ч",
                "с", "м", "т", "ь", "д")
            all_combos_names = list(itertools.combinations(letters_names, 7))
            all_combos_names = [''.join(combo) for combo in all_combos_names]
            project_name = random.sample(all_combos_names, 1)[0]
            clear_field = self.find_element(MainPageLocators.LOCATOR_PTK_INPUT_RENAME).send_keys(Keys.CONTROL + 'A',
                                                                                                    Keys.DELETE)
            send_keys = self.find_element(MainPageLocators.LOCATOR_PTK_INPUT_RENAME).send_keys(
                project_name + 'Rename_test')
            click_on_body_of_main_page = self.find_clickable_element(MainPageLocators.LOCATOR_PTK_BODY_OF_MAIN_PAGE)
            assert self.find_element(MainPageLocators.LOCATOR_PTK_FIND_RENAME_ALERT).text == send_keys
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Переименование версии. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Переименование версии. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Переименование версии. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Переименование версии. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def name_new_version(self):
        try:
            letters_names = (
                "а", "б", "в", "г", "д", "ж", "з", "е", "и", "к", "л", "ы", "н", "к", "о", "щ", "х", "ъ", "й", "ц", "у",
                "ф",
                "ч",
                "с", "м", "т", "ь", "д")
            all_combos_names = list(itertools.combinations(letters_names, 7))
            all_combos_names = [''.join(combo) for combo in all_combos_names]
            version_name = random.sample(all_combos_names, 1)[0]
            name_new_version = self.find_element(MainPageLocators.LOCATOR_PTK_NEW_VERSION_NAME).send_keys(
                version_name + 'QA TEST')
            click_on_body_of_main_page = self.find_clickable_element(MainPageLocators.LOCATOR_PTK_BODY_OF_MAIN_PAGE)
            assert self.find_element(MainPageLocators.LOCATOR_PTK_FIND_RENAME_ALERT).text == name_new_version
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Название новой версии. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Название новой версии. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Название новой версии. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Название новой версии. Успешный.',
                              attachment_type=AttachmentType.PNG)


    def invisible_project_card(self):
        try:
            assert self.invisible_element(MainPageLocators.LOCATOR_PTK_GET_TASK_NAME)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Проверка того, что карточка не найдена. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка того, что карточка не найдена. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Проверка того, что карточка не найдена. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка того, что карточка не найдена. Успешный.',
                              attachment_type=AttachmentType.PNG)
    def assert_part_text(self):
        try:
            assert self.find_element(
                MainPageLocators.LOCATOR_PTK_GET_TASK_NAME).text == 'Техперевооружение ПС 500 кВ Магистральная, ' \
                                                                    'ПС 220 кВ Новая, ПС 220 кВ Восточно-Моховая, ' \
                                                                    'ПС 220 кВ Картопья, ПС 500 кВ Пыть-Ях, ' \
                                                                    'ПС 220 кВ Полоцкая в части модернизации 13-ти ' \
                                                                    'выключателей с заменой маслонаполненных вводов ' \
                                                                    'на вводы с твердой изоляцией (77 высоковольтных ' \
                                                                    'ввода), в конце добавляем рандомный текст '
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Поиск части текста в проекте. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск части текста в проекте. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Поиск части текста в проекте. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск части текста в проекте. Успешный',
                              attachment_type=AttachmentType.PNG)