import logging

import allure
import pytest

from Login_Page import SearchHelper
from Main_Page import SearchHelperMainPage


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Chrome. Выбор другой версии продукта.')
@allure.story('Успешный выбор другой версии продукта.')
@allure.severity('Critical')
def test_select_another_product_version_chrome(browser_chrome_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('select_another_product_version.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_chrome_bs)
    ptk_login_page.go_to_login()
    logging.debug('select_another_product_version_chrome. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('select_another_product_version_chrome. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('select_another_product_version_chrome. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('select_another_product_version_chrome. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'select_another_product_version_chrome. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_chrome_bs)
    ptk_main_page.get_project_id_new_version()
    logging.debug(
        'select_another_product_version_chrome. Сохранил идентификатор проекта первого проекта с текущей версии')
    ptk_main_page.click_version_selector()
    ptk_main_page.wait_for_loading()
    logging.debug(
        'select_another_product_version_chrome. Кликнул на селектор версий')
    ptk_main_page.select_old_version()
    logging.debug(
        'select_another_product_version_chrome. Выбрал версию "Версия для СД"')
    ptk_main_page.click_on_body_of_main_page()
    logging.debug(
        'select_another_product_version_chrome. Кликнул на тело страницы, что-бы скрыть селектор версий')
    logging.debug(
        'select_another_product_version_chrome. Убедился в том, что версия "Версия для СД" выбрана в селекторе версий')
    ptk_main_page.assert_projects_id_not_equal()
    logging.debug(
        'select_another_product_version. Убедился, в том, что идентификатор первого проекта с новой версии не совпадает с идентификатором проекта "Версии для СД"')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Chrome. Выбор другой версии продукта.')
@allure.story('Успешный выбор другой версии продукта.')
@allure.severity('Critical')
def test_select_another_product_version_safari(browser_safari_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('select_another_product_version.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_safari_bs)
    ptk_login_page.go_to_login()
    logging.debug('select_another_product_version_safari. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('select_another_product_version_safari. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('select_another_product_version_safari. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('select_another_product_version_safari. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'select_another_product_version_safari. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_safari_bs)
    ptk_main_page.get_project_id_new_version()
    logging.debug(
        'select_another_product_version_safari. Сохранил идентификатор проекта первого проекта с текущей версии')
    ptk_main_page.click_version_selector()
    ptk_main_page.wait_for_loading()
    logging.debug(
        'select_another_product_version_safari. Кликнул на селектор версий')
    ptk_main_page.select_old_version()
    logging.debug(
        'select_another_product_version_safari. Выбрал версию "Версия для СД"')
    ptk_main_page.click_on_body_of_main_page()
    logging.debug(
        'select_another_product_version_safari. Кликнул на тело страницы, что-бы скрыть селектор версий')
    ptk_main_page.assert_projects_id_not_equal()
    logging.debug(
        'select_another_product_version. Убедился, в том, что идентификатор первого проекта с новой версии не совпадает с идентификатором проекта последней версии')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Chrome. Выбор другой версии продукта.')
@allure.story('Успешный выбор другой версии продукта.')
@allure.severity('Critical')
def test_select_another_product_version_edge(browser_edge_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('select_another_product_version.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_edge_bs)
    ptk_login_page.go_to_login()
    logging.debug('select_another_product_version_edge. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('select_another_product_version_edge. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('select_another_product_version_edge. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('select_another_product_version_edge. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'select_another_product_version_edge. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_edge_bs)
    ptk_main_page.get_project_id_new_version()
    logging.debug(
        'select_another_product_version_edge. Сохранил идентификатор проекта первого проекта с текущей версии')
    ptk_main_page.click_version_selector()
    ptk_main_page.wait_for_loading()
    logging.debug(
        'select_another_product_version_edge. Кликнул на селектор версий')
    ptk_main_page.select_old_version()
    logging.debug(
        'select_another_product_version_edge. Выбрал версию "Версия для СД"')
    ptk_main_page.click_on_body_of_main_page()
    logging.debug(
        'select_another_product_version_edge. Кликнул на тело страницы, что-бы скрыть селектор версий')
    ptk_main_page.assert_projects_id_not_equal()
    logging.debug(
        'select_another_product_version. Убедился, в том, что идентификатор первого проекта с новой версии не совпадает с идентификатором проекта последней версии')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Chrome. Выбор другой версии продукта.')
@allure.story('Успешный выбор другой версии продукта.')
@allure.severity('Critical')
def test_select_another_product_version_firefox(browser_firefox_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('select_another_product_version.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_firefox_bs)
    ptk_login_page.go_to_login()
    logging.debug('select_another_product_version_firefox. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('select_another_product_version_firefox. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('select_another_product_version_firefox. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('select_another_product_version_firefox. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'select_another_product_version_firefox. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_firefox_bs)
    ptk_main_page.get_project_id_new_version()
    logging.debug(
        'select_another_product_version_firefox. Сохранил идентификатор проекта первого проекта с текущей версии')
    ptk_main_page.click_version_selector()
    ptk_main_page.wait_for_loading()
    logging.debug(
        'select_another_product_version_firefox. Кликнул на селектор версий')
    ptk_main_page.select_old_version()
    logging.debug(
        'select_another_product_version_firefox. Выбрал старую версию')
    ptk_main_page.click_on_body_of_main_page()
    logging.debug(
        'select_another_product_version_firefox. Кликнул на тело страницы, что-бы скрыть селектор версий')
    ptk_main_page.assert_projects_id_not_equal()
    logging.debug(
        'select_another_product_version. Убедился, в том, что идентификатор первого проекта с новой версии не совпадает с идентификатором проекта последней версии')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Chrome, разрешение 1280х1024. Выбор другой версии продукта.')
@allure.story('Успешный выбор другой версии продукта.')
@allure.severity('Critical')
def test_select_another_product_version_chrome_max_resolution(browser_chrome_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('select_another_product_version.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_chrome_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('select_another_product_version_chrome. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('select_another_product_version_chrome. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('select_another_product_version_chrome. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('select_another_product_version_chrome. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'select_another_product_version_chrome. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_chrome_bs_max_resolution)
    ptk_main_page.get_project_id_new_version()
    logging.debug(
        'select_another_product_version_chrome. Сохранил идентификатор проекта первого проекта с текущей версии')
    ptk_main_page.click_version_selector()
    ptk_main_page.wait_for_loading()
    logging.debug(
        'select_another_product_version_chrome. Кликнул на селектор версий')
    ptk_main_page.select_old_version()
    logging.debug(
        'select_another_product_version_chrome. Выбрал старую версию')
    ptk_main_page.click_on_body_of_main_page()
    logging.debug(
        'select_another_product_version_chrome. Кликнул на тело страницы, что-бы скрыть селектор версий')
    ptk_main_page.assert_projects_id_not_equal()
    logging.debug(
        'select_another_product_version_chrome. Убедился, в том, что идентификатор первого проекта с новой версии не совпадает с идентификатором проекта последней версии')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Safari, разрешение 1280х1024. Выбор другой версии продукта.')
@allure.story('Успешный выбор другой версии продукта.')
@allure.severity('Critical')
def test_select_another_product_version_safari_max_resolution(browser_safari_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('select_another_product_version.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_safari_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('select_another_product_version_safari. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('select_another_product_version_safari. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('select_another_product_version_safari. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('select_another_product_version_safari. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'select_another_product_version_safari. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_safari_bs_max_resolution)
    ptk_main_page.get_project_id_new_version()
    logging.debug(
        'select_another_product_version_safari. Сохранил идентификатор проекта первого проекта с текущей версии')
    ptk_main_page.click_version_selector()
    ptk_main_page.wait_for_loading()
    logging.debug(
        'select_another_product_version_safari. Кликнул на селектор версий')
    ptk_main_page.select_old_version()
    logging.debug(
        'select_another_product_version_safari. Выбрал старую версию')
    ptk_main_page.click_on_body_of_main_page()
    logging.debug(
        'select_another_product_version_safari. Кликнул на тело страницы, что-бы скрыть селектор версий')
    ptk_main_page.assert_projects_id_not_equal()
    logging.debug(
        'select_another_product_version_safari. Убедился, в том, что идентификатор первого проекта с новой версии не совпадает с идентификатором проекта последней версии')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Edge, разрешение 1280х1024. Выбор другой версии продукта.')
@allure.story('Успешный выбор другой версии продукта.')
@allure.severity('Critical')
def test_select_another_product_version_edge_max_resolution(browser_edge_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('select_another_product_version.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_edge_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('select_another_product_version_edge. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('select_another_product_version_edge. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('select_another_product_version_edge. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('select_another_product_version_edge. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'select_another_product_version_edge. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_edge_bs_max_resolution)
    ptk_main_page.get_project_id_new_version()
    logging.debug(
        'select_another_product_version_edge. Сохранил идентификатор проекта первого проекта с текущей версии')
    ptk_main_page.click_version_selector()
    ptk_main_page.wait_for_loading()
    logging.debug(
        'select_another_product_version_edge. Кликнул на селектор версий')
    ptk_main_page.select_old_version()
    logging.debug(
        'select_another_product_version_edge. Выбрал старую версию')
    ptk_main_page.click_on_body_of_main_page()
    logging.debug(
        'select_another_product_version_edge. Кликнул на тело страницы, что-бы скрыть селектор версий')
    ptk_main_page.assert_projects_id_not_equal()
    logging.debug(
        'select_another_product_version_edge. Убедился, в том, что идентификатор первого проекта с новой версии не совпадает с идентификатором проекта последней версии')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Firefox, разрешение 1280х1024. Выбор другой версии продукта.')
@allure.story('Успешный выбор другой версии продукта.')
@allure.severity('Critical')
def test_select_another_product_version_firefox_max_resolution(browser_firefox_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('select_another_product_version.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_firefox_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('select_another_product_version_firefox. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('select_another_product_version_firefox. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('select_another_product_version_firefox. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('select_another_product_version_firefox. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'select_another_product_version_firefox. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_firefox_bs_max_resolution)
    ptk_main_page.get_project_id_new_version()
    logging.debug(
        'select_another_product_version_firefox. Сохранил идентификатор проекта первого проекта с текущей версии')
    ptk_main_page.click_version_selector()
    ptk_main_page.wait_for_loading()
    logging.debug(
        'select_another_product_version_firefox. Кликнул на селектор версий')
    ptk_main_page.select_old_version()
    logging.debug(
        'select_another_product_version_firefox. Выбрал старую версию')
    ptk_main_page.click_on_body_of_main_page()
    logging.debug(
        'select_another_product_version_firefox. Кликнул на тело страницы, что-бы скрыть селектор версий')
    ptk_main_page.assert_projects_id_not_equal()
    logging.debug(
        'select_another_product_version_firefox. Убедился, в том, что идентификатор первого проекта с новой версии не совпадает с идентификатором проекта последней версии')
