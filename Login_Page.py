import logging

import allure
from allure_commons.types import AttachmentType
from selenium.webdriver.common.by import By

from BaseApp import BasePage


class LoginPageLocators:
    # locators_for_auth_module
    LOCATOR_PTK_GO_TO_LOGIN = 'https://dev.modptk.unc-esk.ru/login'
    LOCATOR_PTK_FILL_LOGIN = (By.XPATH, '//div[2]/div/input')
    LOCATOR_PTK_FILL_PASSWORD = (By.XPATH, '//div[3]/div/div/input')
    LOCATOR_PTK_LOGIN_TEXT = (By.XPATH, '//*[@id="root"]/div[1]/form/button')
    LOCATOR_PTK_AUTHORIZATION_ASSERT = (By.XPATH, '//span[contains(text(), "Корректировка ИПР")]')
    LOCATOR_PTK_LOGIN_BUTTON = (By.XPATH, '//span[@class="MuiButton-label" and contains(text(), "Войти")]')

class SearchHelper(BasePage):
    # functions_for_auth_module
    def go_to_login(self):
        try:
            self.driver.get(LoginPageLocators.LOCATOR_PTK_GO_TO_LOGIN)
            assert self.find_element(LoginPageLocators.LOCATOR_PTK_LOGIN_TEXT).text == "Войти"
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Переход на страницу логина. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Переход на страницу логина. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "qq"}}')
            with allure.step('Переход на страницу логина. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Переход на страницу логина. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def fill_admin_login(self):
        try:
            self.find_element(LoginPageLocators.LOCATOR_PTK_FILL_LOGIN).send_keys('user1@unc.ru')
            login_field_for_assert = self.find_element(LoginPageLocators.LOCATOR_PTK_FILL_LOGIN).get_attribute('value')
            assert login_field_for_assert == 'user1@unc.ru'
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Переход на страницу логина. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Переход на страницу логина. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Переход на страницу логина. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Переход на страницу логина. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def fill_admin_password(self):
        try:
            self.find_element(LoginPageLocators.LOCATOR_PTK_FILL_PASSWORD).send_keys('user1@unc.ru')
            password_field_for_assert = self.find_element(LoginPageLocators.LOCATOR_PTK_FILL_PASSWORD).get_attribute(
                'value')
            assert password_field_for_assert == 'user1@unc.ru'
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Заполнение пароля администратора. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Заполнение пароля администратора. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Заполнение пароля администратора. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Заполнение пароля администратора. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def fill_free_login(self, login):
        try:
            self.find_element(LoginPageLocators.LOCATOR_PTK_FILL_LOGIN).send_keys(login)
            free_login_field_for_assert = self.find_element(LoginPageLocators.LOCATOR_PTK_FILL_LOGIN).get_attribute(
                'value')
            assert free_login_field_for_assert != ''
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Заполняем логин произвольным значением. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Заполняем логин произвольным значением. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Заполняем логин произвольным значением. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Заполняем логин произвольным значением. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def fill_free_password(self, password):
        try:
            self.find_element(LoginPageLocators.LOCATOR_PTK_FILL_PASSWORD).send_keys(password)
            free_password_field_for_assert = self.find_element(
                LoginPageLocators.LOCATOR_PTK_FILL_PASSWORD).get_attribute(
                'value')
            assert free_password_field_for_assert != ''
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Заполняем пароль произвольным значением. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Заполняем пароль произвольным значением. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Заполняем пароль произвольным значением. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Заполняем пароль произвольным значением. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def click_login_button(self):
        try:
            assert self.find_element(LoginPageLocators.LOCATOR_PTK_LOGIN_TEXT).text == "Войти"
            self.find_clickable_element(LoginPageLocators.LOCATOR_PTK_LOGIN_BUTTON)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Клик на кнопку логина. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на кнопку логина. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Клик на кнопку логина. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на кнопку логина. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def authorization_assert(self):
        try:
            self.find_element(LoginPageLocators.LOCATOR_PTK_AUTHORIZATION_ASSERT)
            assert self.find_element(LoginPageLocators.LOCATOR_PTK_AUTHORIZATION_ASSERT).text == 'Корректировка ИПР'
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Authorization failed"}}')
            with allure.step('Проверка авторизации. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Проверка авторизации. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Authorization granted"}}')
            with allure.step('Проверка авторизации. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Проверка авторизации. Успешный',
                              attachment_type=AttachmentType.PNG)
