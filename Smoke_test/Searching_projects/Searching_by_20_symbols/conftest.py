import logging

import pytest
from selenium import webdriver


@pytest.fixture()
def browser_chrome_bs():
    try:
        desired_cap = {'browser': 'Chrome', 'browser_version': 'latest', 'os': 'Windows', 'os_version': '10',
                       'resolution': '1024x768', 'acceptSslCerts': True, "seleniumVersion": "3.141.5",
                       'browserstack.use_w3c': True, "local": "false", "debug": "true", "networkLogs": "true",
                       'name': 'Searching_by_20_symbols',
                       'project': 'ModPTK', "browserstack.console": "errors", 'build': 'ModPTK_smoke_test'}

        driver = webdriver.Remote(
            command_executor='https://pupas1:bqRfrAxT3yvkycCQcJhY@hub-cloud.browserstack.com/wd/hub',
            desired_capabilities=desired_cap)
        driver.maximize_window()
    except:
        logging.exception('')
        logging.critical('TEST FAILED')
    finally:
        yield driver
        driver.quit()


@pytest.fixture()
def browser_firefox_bs():
    try:
        desired_cap = {'browser': 'Firefox', 'browser_version': 'latest', 'os': 'Windows', 'os_version': '10',
                       'resolution': '1024x768', 'acceptSslCerts': True, "seleniumVersion": "3.141.5",
                       'browserstack.use_w3c': True, "local": "false", "debug": "true", "networkLogs": "true",
                       'name': 'Searching_by_20_symbols',
                       'project': 'ModPTK', "browserstack.console": "errors", 'build': 'ModPTK_smoke_test'}

        driver = webdriver.Remote(
            command_executor='https://pupas1:bqRfrAxT3yvkycCQcJhY@hub-cloud.browserstack.com/wd/hub',
            desired_capabilities=desired_cap)
        driver.maximize_window()
    except:
        logging.exception('')
        logging.critical('TEST FAILED')
    finally:
        yield driver
        driver.quit()


@pytest.fixture()
def browser_edge_bs():
    try:
        desired_cap = {"browserName": "Edge", 'browser_version': 'latest', 'os': 'Windows', 'os_version': '10',
                       'resolution': '1024x768', 'acceptSslCerts': True, "seleniumVersion": "3.141.5",
                       'browserstack.use_w3c': True, "local": "false", "debug": "true", "networkLogs": "true",
                       'name': 'Searching_by_20_symbols',
                       'project': 'ModPTK', "browserstack.console": "errors", 'build': 'ModPTK_smoke_test'}

        driver = webdriver.Remote(
            command_executor='https://pupas1:bqRfrAxT3yvkycCQcJhY@hub-cloud.browserstack.com/wd/hub',
            desired_capabilities=desired_cap)
        driver.maximize_window()
    except:
        logging.exception('')
        logging.critical('TEST FAILED')
    finally:
        yield driver
        driver.quit()


@pytest.fixture()
def browser_safari_bs():
    try:
        desired_cap = {"browser": "Safari", "browser_version": "14.0", "os": "OS X", "os_version": "Big Sur",
                       'resolution': '1024x768', 'acceptSslCerts': True, "seleniumVersion": "3.141.5",
                       'browserstack.use_w3c': True, "local": "false", "debug": "true", "networkLogs": "true",
                       'name': 'Searching_by_20_symbols',
                       'project': 'ModPTK', "browserstack.console": "errors", 'build': 'ModPTK_smoke_test'}

        driver = webdriver.Remote(
            command_executor='https://pupas1:bqRfrAxT3yvkycCQcJhY@hub-cloud.browserstack.com/wd/hub',
            desired_capabilities=desired_cap)
        driver.maximize_window()
    except:
        logging.exception('')
        logging.critical('TEST FAILED')
    finally:
        yield driver
        driver.quit()


@pytest.fixture()
def browser_chrome_bs_max_resolution():
    try:
        desired_cap = {'browser': 'Chrome', 'browser_version': 'latest', 'os': 'Windows', 'os_version': '10',
                       'resolution': '1280x1024', 'acceptSslCerts': True, "seleniumVersion": "3.141.5",
                       'browserstack.use_w3c': True, "local": "false", "debug": "true", "networkLogs": "true",
                       'name': 'Searching_by_20_symbols',
                       'project': 'ModPTK', "browserstack.console": "errors", 'build': 'ModPTK_smoke_test'}

        driver = webdriver.Remote(
            command_executor='https://pupas1:bqRfrAxT3yvkycCQcJhY@hub-cloud.browserstack.com/wd/hub',
            desired_capabilities=desired_cap)
        driver.maximize_window()
    except:
        logging.exception('')
        logging.critical('TEST FAILED')
    finally:
        yield driver
        driver.quit()


@pytest.fixture()
def browser_firefox_bs_max_resolution():
    try:
        desired_cap = {'browser': 'Firefox', 'browser_version': 'latest', 'os': 'Windows', 'os_version': '10',
                       'resolution': '1280x1024', 'acceptSslCerts': True, "seleniumVersion": "3.141.5",
                       'browserstack.use_w3c': True, "local": "false", "debug": "true", "networkLogs": "true",
                       'name': 'Searching_by_20_symbols',
                       'project': 'ModPTK', "browserstack.console": "errors", 'build': 'ModPTK_smoke_test'}

        driver = webdriver.Remote(
            command_executor='https://pupas1:bqRfrAxT3yvkycCQcJhY@hub-cloud.browserstack.com/wd/hub',
            desired_capabilities=desired_cap)
        driver.maximize_window()
    except:
        logging.exception('')
        logging.critical('TEST FAILED')
    finally:
        yield driver
        driver.quit()


@pytest.fixture()
def browser_edge_bs_max_resolution():
    try:
        desired_cap = {"browserName": "Edge", 'browser_version': 'latest', 'os': 'Windows', 'os_version': '10',
                       'resolution': '1280x1024', 'acceptSslCerts': True, "seleniumVersion": "3.141.5",
                       'browserstack.use_w3c': True, "local": "false", "debug": "true", "networkLogs": "true",
                       'name': 'Searching_by_20_symbols',
                       'project': 'ModPTK', "browserstack.console": "errors", 'build': 'ModPTK_smoke_test'}

        driver = webdriver.Remote(
            command_executor='https://pupas1:bqRfrAxT3yvkycCQcJhY@hub-cloud.browserstack.com/wd/hub',
            desired_capabilities=desired_cap)
        driver.maximize_window()
    except:
        logging.exception('')
        logging.critical('TEST FAILED')
    finally:
        yield driver
        driver.quit()


@pytest.fixture()
def browser_safari_bs_max_resolution():
    try:
        desired_cap = {"browser": "Safari", "browser_version": "14.0", "os": "OS X", "os_version": "Big Sur",
                       'resolution': '1280x1024', 'acceptSslCerts': True, "seleniumVersion": "3.141.5",
                       'browserstack.use_w3c': True, "local": "false", "debug": "true", "networkLogs": "true",
                       'name': 'Searching_by_20_symbols',
                       'project': 'ModPTK', "browserstack.console": "errors", 'build': 'ModPTK_smoke_test'}

        driver = webdriver.Remote(
            command_executor='https://pupas1:bqRfrAxT3yvkycCQcJhY@hub-cloud.browserstack.com/wd/hub',
            desired_capabilities=desired_cap)
        driver.maximize_window()
    except:
        logging.exception('')
        logging.critical('TEST FAILED')
    finally:
        yield driver
        driver.quit()
