import logging

import allure
import pytest

from Login_Page import SearchHelper
from Main_Page import SearchHelperMainPage
from Project_Page import SearchHelperProjectPage


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Chrome. Поиск персонального проекта: по имени.')
@allure.story('Редактирование проекта: название.')
@allure.severity('Critical')
def test_searching_personal_project_by_ID_chrome(browser_chrome_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('searching_personal_project_by_ID.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_chrome_bs)
    ptk_login_page.go_to_login()
    logging.debug('searching_personal_project_by_ID. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('searching_personal_project_by_ID. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('searching_personal_project_by_ID. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('searching_personal_project_by_ID. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'searching_personal_project_by_ID. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_chrome_bs)
    ptk_main_page.click_on_personal_projects()
    logging.debug(
        'searching_personal_project_by_ID. Кликнул на кнопку "Личные проекты"')
    ptk_main_page.create_new_personal_project()
    logging.debug(
        'searching_personal_project_by_ID. Кликнул на кнопку "Новый проект +"')
    ptk_project_page = SearchHelperProjectPage(browser_chrome_bs)
    ptk_project_page.id_personal_project()
    logging.debug(
        'searching_personal_project_by_ID. Указал идентификатор проекта')
    ptk_project_page.name_personal_project()
    logging.debug(
        'searching_personal_project_by_ID. Указал название проекта')
    ptk_project_page.personal_project_dropdown_filial()
    logging.debug(
        'searching_personal_project_by_ID. Кликнул на выпадающее меню филиала')
    ptk_project_page.select_filial_north_west()
    logging.debug(
        'searching_personal_project_by_ID. Выбрал северо-западный МЭС')
    ptk_project_page.find_selected_filial()
    logging.debug(
        'searching_personal_project_by_ID. Убедился, что филиал выбран')
    ptk_project_page.project_card_save_button()
    logging.debug(
        'searching_personal_project_by_ID. Кликнул на кнопку "Сохранить"')
    ptk_project_page.project_created_notification()
    logging.debug(
        'searching_personal_project_by_ID. Получил уведомление о том, что проект создан')
    ptk_project_page.project_edit_icon()
    logging.debug(
        'searching_personal_project_by_ID. Кликнул на кнопку редактирования')
    ptk_project_page.personal_project_random_id()
    logging.debug(
        'searching_personal_project_by_ID. Ввел название')
    ptk_project_page.project_card_save_button()
    logging.debug(
        'searching_personal_project_by_ID. Кликнул на кнопку "Сохранить"')
    ptk_project_page.project_created_notification()
    logging.debug(
        'searching_personal_project_by_ID. Получил уведомление о том, что проект создан')
    ptk_project_page.go_to_table_of_personal_projects()
    logging.debug(
        'searching_personal_project_by_ID. Перешел на страницу личных проектов')
    ptk_project_page.find_random_id_project()
    logging.debug(
        'searching_personal_project_by_ID. В строку поиска ввел идентификатор случайного проекта')
    ptk_project_page.assert_random_id_project_found()
    logging.debug(
        'searching_personal_project_by_ID. Проект со случайным идентификатором найден')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Firefox. Поиск персонального проекта: по имени.')
@allure.story('Редактирование проекта: название.')
@allure.severity('Critical')
def test_searching_personal_project_by_ID_firefox(browser_firefox_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('searching_personal_project_by_ID.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_firefox_bs)
    ptk_login_page.go_to_login()
    logging.debug('searching_personal_project_by_ID. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('searching_personal_project_by_ID. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('searching_personal_project_by_ID. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('searching_personal_project_by_ID. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'searching_personal_project_by_ID. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_firefox_bs)
    ptk_main_page.click_on_personal_projects()
    logging.debug(
        'searching_personal_project_by_ID. Кликнул на кнопку "Личные проекты"')
    ptk_main_page.create_new_personal_project()
    logging.debug(
        'searching_personal_project_by_ID. Кликнул на кнопку "Новый проект +"')
    ptk_project_page = SearchHelperProjectPage(browser_firefox_bs)
    ptk_project_page.id_personal_project()
    logging.debug(
        'searching_personal_project_by_ID. Указал идентификатор проекта')
    ptk_project_page.name_personal_project()
    logging.debug(
        'searching_personal_project_by_ID. Указал название проекта')
    ptk_project_page.personal_project_dropdown_filial()
    logging.debug(
        'searching_personal_project_by_ID. Кликнул на выпадающее меню филиала')
    ptk_project_page.select_filial_north_west()
    logging.debug(
        'searching_personal_project_by_ID. Выбрал северо-западный МЭС')
    ptk_project_page.find_selected_filial()
    logging.debug(
        'searching_personal_project_by_ID. Убедился, что филиал выбран')
    ptk_project_page.project_card_save_button()
    logging.debug(
        'searching_personal_project_by_ID. Кликнул на кнопку "Сохранить"')
    ptk_project_page.project_created_notification()
    logging.debug(
        'searching_personal_project_by_ID. Получил уведомление о том, что проект создан')
    ptk_project_page.project_edit_icon()
    logging.debug(
        'searching_personal_project_by_ID. Кликнул на кнопку редактирования')
    ptk_project_page.personal_project_random_name()
    logging.debug(
        'searching_personal_project_by_ID. Ввел название')
    ptk_project_page.project_card_save_button()
    logging.debug(
        'searching_personal_project_by_ID. Кликнул на кнопку "Сохранить"')
    ptk_project_page.project_created_notification()
    logging.debug(
        'searching_personal_project_by_ID. Получил уведомление о том, что проект создан')
    ptk_project_page.go_to_table_of_personal_projects()
    logging.debug(
        'searching_personal_project_by_ID. Перешел на страницу личных проектов')
    ptk_project_page.find_random_name_project()
    logging.debug(
        'searching_personal_project_by_ID. В строку поиска ввел название случайного проекта')
    ptk_project_page.assert_random_project_found()


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Edge. Поиск персонального проекта: по имени.')
@allure.story('Редактирование проекта: название.')
@allure.severity('Critical')
def test_searching_personal_project_by_ID_edge(browser_edge_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('searching_personal_project_by_ID.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_edge_bs)
    ptk_login_page.go_to_login()
    logging.debug('searching_personal_project_by_ID. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('searching_personal_project_by_ID. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('searching_personal_project_by_ID. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('searching_personal_project_by_ID. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'searching_personal_project_by_ID. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_edge_bs)
    ptk_main_page.click_on_personal_projects()
    logging.debug(
        'searching_personal_project_by_ID. Кликнул на кнопку "Личные проекты"')
    ptk_main_page.create_new_personal_project()
    logging.debug(
        'searching_personal_project_by_ID. Кликнул на кнопку "Новый проект +"')
    ptk_project_page = SearchHelperProjectPage(browser_edge_bs)
    ptk_project_page.id_personal_project()
    logging.debug(
        'searching_personal_project_by_ID. Указал идентификатор проекта')
    ptk_project_page.name_personal_project()
    logging.debug(
        'searching_personal_project_by_ID. Указал название проекта')
    ptk_project_page.personal_project_dropdown_filial()
    logging.debug(
        'searching_personal_project_by_ID. Кликнул на выпадающее меню филиала')
    ptk_project_page.select_filial_north_west()
    logging.debug(
        'searching_personal_project_by_ID. Выбрал северо-западный МЭС')
    ptk_project_page.find_selected_filial()
    logging.debug(
        'searching_personal_project_by_ID. Убедился, что филиал выбран')
    ptk_project_page.project_card_save_button()
    logging.debug(
        'searching_personal_project_by_ID. Кликнул на кнопку "Сохранить"')
    ptk_project_page.project_created_notification()
    logging.debug(
        'searching_personal_project_by_ID. Получил уведомление о том, что проект создан')
    ptk_project_page.project_edit_icon()
    logging.debug(
        'searching_personal_project_by_ID. Кликнул на кнопку редактирования')
    ptk_project_page.personal_project_random_name()
    logging.debug(
        'searching_personal_project_by_ID. Ввел название')
    ptk_project_page.project_card_save_button()
    logging.debug(
        'searching_personal_project_by_ID. Кликнул на кнопку "Сохранить"')
    ptk_project_page.project_created_notification()
    logging.debug(
        'searching_personal_project_by_ID. Получил уведомление о том, что проект создан')
    ptk_project_page.go_to_table_of_personal_projects()
    logging.debug(
        'searching_personal_project_by_ID. Перешел на страницу личных проектов')
    ptk_project_page.find_random_name_project()
    logging.debug(
        'searching_personal_project_by_ID. В строку поиска ввел название случайного проекта')
    ptk_project_page.assert_random_project_found()


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Safari. Поиск персонального проекта: по имени.')
@allure.story('Редактирование проекта: название.')
@allure.severity('Critical')
def test_searching_personal_project_by_ID_safari(browser_safari_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('searching_personal_project_by_ID.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_safari_bs)
    ptk_login_page.go_to_login()
    logging.debug('searching_personal_project_by_ID. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('searching_personal_project_by_ID. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('searching_personal_project_by_ID. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('searching_personal_project_by_ID. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'searching_personal_project_by_ID. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_safari_bs)
    ptk_main_page.click_on_personal_projects()
    logging.debug(
        'searching_personal_project_by_ID. Кликнул на кнопку "Личные проекты"')
    ptk_main_page.create_new_personal_project()
    logging.debug(
        'searching_personal_project_by_ID. Кликнул на кнопку "Новый проект +"')
    ptk_project_page = SearchHelperProjectPage(browser_safari_bs)
    ptk_project_page.id_personal_project()
    logging.debug(
        'searching_personal_project_by_ID. Указал идентификатор проекта')
    ptk_project_page.name_personal_project()
    logging.debug(
        'searching_personal_project_by_ID. Указал название проекта')
    ptk_project_page.personal_project_dropdown_filial()
    logging.debug(
        'searching_personal_project_by_ID. Кликнул на выпадающее меню филиала')
    ptk_project_page.select_filial_north_west()
    logging.debug(
        'searching_personal_project_by_ID. Выбрал северо-западный МЭС')
    ptk_project_page.find_selected_filial()
    logging.debug(
        'searching_personal_project_by_ID. Убедился, что филиал выбран')
    ptk_project_page.project_card_save_button()
    logging.debug(
        'searching_personal_project_by_ID. Кликнул на кнопку "Сохранить"')
    ptk_project_page.project_created_notification()
    logging.debug(
        'searching_personal_project_by_ID. Получил уведомление о том, что проект создан')
    ptk_project_page.project_edit_icon()
    logging.debug(
        'searching_personal_project_by_ID. Кликнул на кнопку редактирования')
    ptk_project_page.personal_project_random_name()
    logging.debug(
        'searching_personal_project_by_ID. Ввел название')
    ptk_project_page.project_card_save_button()
    logging.debug(
        'searching_personal_project_by_ID. Кликнул на кнопку "Сохранить"')
    ptk_project_page.project_created_notification()
    logging.debug(
        'searching_personal_project_by_ID. Получил уведомление о том, что проект создан')
    ptk_project_page.go_to_table_of_personal_projects()
    logging.debug(
        'searching_personal_project_by_ID. Перешел на страницу личных проектов')
    ptk_project_page.find_random_name_project()
    logging.debug(
        'searching_personal_project_by_ID. В строку поиска ввел название случайного проекта')
    ptk_project_page.assert_random_project_found()


@pytest.mark.smoke_test_modptk
@allure.feature(
    'Smoke тестирование на Chrome, разрешение 1280х1024. Поиск персонального проекта: по имени.')
@allure.story('Редактирование проекта: название.')
@allure.severity('Critical')
def test_searching_personal_project_by_ID_chrome_max_resolution(browser_chrome_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('searching_personal_project_by_ID.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_chrome_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('searching_personal_project_by_ID. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('searching_personal_project_by_ID. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('searching_personal_project_by_ID. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('searching_personal_project_by_ID. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'searching_personal_project_by_ID. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_chrome_bs_max_resolution)
    ptk_main_page.click_on_personal_projects()
    logging.debug(
        'searching_personal_project_by_ID. Кликнул на кнопку "Личные проекты"')
    ptk_main_page.create_new_personal_project()
    logging.debug(
        'searching_personal_project_by_ID. Кликнул на кнопку "Новый проект +"')
    ptk_project_page = SearchHelperProjectPage(browser_chrome_bs_max_resolution)
    ptk_project_page.id_personal_project()
    logging.debug(
        'searching_personal_project_by_ID. Указал идентификатор проекта')
    ptk_project_page.name_personal_project()
    logging.debug(
        'searching_personal_project_by_ID. Указал название проекта')
    ptk_project_page.personal_project_dropdown_filial()
    logging.debug(
        'searching_personal_project_by_ID. Кликнул на выпадающее меню филиала')
    ptk_project_page.select_filial_north_west()
    logging.debug(
        'searching_personal_project_by_ID. Выбрал северо-западный МЭС')
    ptk_project_page.find_selected_filial()
    logging.debug(
        'searching_personal_project_by_ID. Убедился, что филиал выбран')
    ptk_project_page.project_card_save_button()
    logging.debug(
        'searching_personal_project_by_ID. Кликнул на кнопку "Сохранить"')
    ptk_project_page.project_created_notification()
    logging.debug(
        'searching_personal_project_by_ID. Получил уведомление о том, что проект создан')
    ptk_project_page.project_edit_icon()
    logging.debug(
        'searching_personal_project_by_ID. Кликнул на кнопку редактирования')
    ptk_project_page.personal_project_random_name()
    logging.debug(
        'searching_personal_project_by_ID. Ввел название')
    ptk_project_page.project_card_save_button()
    logging.debug(
        'searching_personal_project_by_ID. Кликнул на кнопку "Сохранить"')
    ptk_project_page.project_created_notification()
    logging.debug(
        'searching_personal_project_by_ID. Получил уведомление о том, что проект создан')
    ptk_project_page.go_to_table_of_personal_projects()
    logging.debug(
        'searching_personal_project_by_ID. Перешел на страницу личных проектов')
    ptk_project_page.find_random_name_project()
    logging.debug(
        'searching_personal_project_by_ID. В строку поиска ввел название случайного проекта')
    ptk_project_page.assert_random_project_found()


@pytest.mark.smoke_test_modptk
@allure.feature(
    'Smoke тестирование на Firefox, разрешение 1280х1024. Поиск персонального проекта: по имени.')
@allure.story('Редактирование проекта: название.')
@allure.severity('Critical')
def test_searching_personal_project_by_ID_firefox_max_resolution(browser_firefox_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('searching_personal_project_by_ID.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_firefox_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('searching_personal_project_by_ID. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('searching_personal_project_by_ID. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('searching_personal_project_by_ID. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('searching_personal_project_by_ID. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'searching_personal_project_by_ID. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_firefox_bs_max_resolution)
    ptk_main_page.click_on_personal_projects()
    logging.debug(
        'searching_personal_project_by_ID. Кликнул на кнопку "Личные проекты"')
    ptk_main_page.create_new_personal_project()
    logging.debug(
        'searching_personal_project_by_ID. Кликнул на кнопку "Новый проект +"')
    ptk_project_page = SearchHelperProjectPage(browser_firefox_bs_max_resolution)
    ptk_project_page.id_personal_project()
    logging.debug(
        'searching_personal_project_by_ID. Указал идентификатор проекта')
    ptk_project_page.name_personal_project()
    logging.debug(
        'searching_personal_project_by_ID. Указал название проекта')
    ptk_project_page.personal_project_dropdown_filial()
    logging.debug(
        'searching_personal_project_by_ID. Кликнул на выпадающее меню филиала')
    ptk_project_page.select_filial_north_west()
    logging.debug(
        'searching_personal_project_by_ID. Выбрал северо-западный МЭС')
    ptk_project_page.find_selected_filial()
    logging.debug(
        'searching_personal_project_by_ID. Убедился, что филиал выбран')
    ptk_project_page.project_card_save_button()
    logging.debug(
        'searching_personal_project_by_ID. Кликнул на кнопку "Сохранить"')
    ptk_project_page.project_created_notification()
    logging.debug(
        'searching_personal_project_by_ID. Получил уведомление о том, что проект создан')
    ptk_project_page.project_edit_icon()
    logging.debug(
        'searching_personal_project_by_ID. Кликнул на кнопку редактирования')
    ptk_project_page.personal_project_random_name()
    logging.debug(
        'searching_personal_project_by_ID. Ввел название')
    ptk_project_page.project_card_save_button()
    logging.debug(
        'searching_personal_project_by_ID. Кликнул на кнопку "Сохранить"')
    ptk_project_page.project_created_notification()
    logging.debug(
        'searching_personal_project_by_ID. Получил уведомление о том, что проект создан')
    ptk_project_page.go_to_table_of_personal_projects()
    logging.debug(
        'searching_personal_project_by_ID. Перешел на страницу личных проектов')
    ptk_project_page.find_random_name_project()
    logging.debug(
        'searching_personal_project_by_ID. В строку поиска ввел название случайного проекта')
    ptk_project_page.assert_random_project_found()


@pytest.mark.smoke_test_modptk
@allure.feature(
    'Smoke тестирование на Edge, разрешение 1280х1024. Поиск персонального проекта: по имени.')
@allure.story('Редактирование проекта: название.')
@allure.severity('Critical')
def test_searching_personal_project_by_ID_edge_max_resolution(browser_edge_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('searching_personal_project_by_ID.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_edge_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('searching_personal_project_by_ID. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('searching_personal_project_by_ID. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('searching_personal_project_by_ID. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('searching_personal_project_by_ID. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'searching_personal_project_by_ID. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_edge_bs_max_resolution)
    ptk_main_page.click_on_personal_projects()
    logging.debug(
        'searching_personal_project_by_ID. Кликнул на кнопку "Личные проекты"')
    ptk_main_page.create_new_personal_project()
    logging.debug(
        'searching_personal_project_by_ID. Кликнул на кнопку "Новый проект +"')
    ptk_project_page = SearchHelperProjectPage(browser_edge_bs_max_resolution)
    ptk_project_page.id_personal_project()
    logging.debug(
        'searching_personal_project_by_ID. Указал идентификатор проекта')
    ptk_project_page.name_personal_project()
    logging.debug(
        'searching_personal_project_by_ID. Указал название проекта')
    ptk_project_page.personal_project_dropdown_filial()
    logging.debug(
        'searching_personal_project_by_ID. Кликнул на выпадающее меню филиала')
    ptk_project_page.select_filial_north_west()
    logging.debug(
        'searching_personal_project_by_ID. Выбрал северо-западный МЭС')
    ptk_project_page.find_selected_filial()
    logging.debug(
        'searching_personal_project_by_ID. Убедился, что филиал выбран')
    ptk_project_page.project_card_save_button()
    logging.debug(
        'searching_personal_project_by_ID. Кликнул на кнопку "Сохранить"')
    ptk_project_page.project_created_notification()
    logging.debug(
        'searching_personal_project_by_ID. Получил уведомление о том, что проект создан')
    ptk_project_page.project_edit_icon()
    logging.debug(
        'searching_personal_project_by_ID. Кликнул на кнопку редактирования')
    ptk_project_page.personal_project_random_name()
    logging.debug(
        'searching_personal_project_by_ID. Ввел название')
    ptk_project_page.project_card_save_button()
    logging.debug(
        'searching_personal_project_by_ID. Кликнул на кнопку "Сохранить"')
    ptk_project_page.project_created_notification()
    logging.debug(
        'searching_personal_project_by_ID. Получил уведомление о том, что проект создан')
    ptk_project_page.go_to_table_of_personal_projects()
    logging.debug(
        'searching_personal_project_by_ID. Перешел на страницу личных проектов')
    ptk_project_page.find_random_name_project()
    logging.debug(
        'searching_personal_project_by_ID. В строку поиска ввел название случайного проекта')
    ptk_project_page.assert_random_project_found()


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Safari, разрешение 1280х1024. Поиск персонального проекта: по имени.')
@allure.story('Редактирование проекта: название.')
@allure.severity('Critical')
def test_searching_personal_project_by_ID_safari_max_resolution(browser_safari_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('searching_personal_project_by_ID.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_safari_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('searching_personal_project_by_ID. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('searching_personal_project_by_ID. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('searching_personal_project_by_ID. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('searching_personal_project_by_ID. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'searching_personal_project_by_ID. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_safari_bs_max_resolution)
    ptk_main_page.click_on_personal_projects()
    logging.debug(
        'searching_personal_project_by_ID. Кликнул на кнопку "Личные проекты"')
    ptk_main_page.create_new_personal_project()
    logging.debug(
        'searching_personal_project_by_ID. Кликнул на кнопку "Новый проект +"')
    ptk_project_page = SearchHelperProjectPage(browser_safari_bs_max_resolution)
    ptk_project_page.id_personal_project()
    logging.debug(
        'searching_personal_project_by_ID. Указал идентификатор проекта')
    ptk_project_page.name_personal_project()
    logging.debug(
        'searching_personal_project_by_ID. Указал название проекта')
    ptk_project_page.personal_project_dropdown_filial()
    logging.debug(
        'searching_personal_project_by_ID. Кликнул на выпадающее меню филиала')
    ptk_project_page.select_filial_north_west()
    logging.debug(
        'searching_personal_project_by_ID. Выбрал северо-западный МЭС')
    ptk_project_page.find_selected_filial()
    logging.debug(
        'searching_personal_project_by_ID. Убедился, что филиал выбран')
    ptk_project_page.project_card_save_button()
    logging.debug(
        'searching_personal_project_by_ID. Кликнул на кнопку "Сохранить"')
    ptk_project_page.project_created_notification()
    logging.debug(
        'searching_personal_project_by_ID. Получил уведомление о том, что проект создан')
    ptk_project_page.project_edit_icon()
    logging.debug(
        'searching_personal_project_by_ID. Кликнул на кнопку редактирования')
    ptk_project_page.personal_project_random_name()
    logging.debug(
        'searching_personal_project_by_ID. Ввел название')
    ptk_project_page.project_card_save_button()
    logging.debug(
        'searching_personal_project_by_ID. Кликнул на кнопку "Сохранить"')
    ptk_project_page.project_created_notification()
    logging.debug(
        'searching_personal_project_by_ID. Получил уведомление о том, что проект создан')
    ptk_project_page.go_to_table_of_personal_projects()
    logging.debug(
        'searching_personal_project_by_ID. Перешел на страницу личных проектов')
    ptk_project_page.find_random_name_project()
    logging.debug(
        'searching_personal_project_by_ID. В строку поиска ввел название случайного проекта')
    ptk_project_page.assert_random_project_found()
