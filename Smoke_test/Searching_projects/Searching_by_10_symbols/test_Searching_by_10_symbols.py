import logging

import allure
import pytest

from Login_Page import SearchHelper
from Main_Page import SearchHelperMainPage


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Chrome. Поиск по десяти символам проекта.')
@allure.story('Успешный поиск по десяти символам проекта.')
@allure.severity('Critical')
def test_Searching_by_10_symbols_chrome(browser_chrome_bs):
    logger = logging.getLogger('modPTK_logger_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('Searching_by_10_symbols.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_chrome_bs)
    ptk_login_page.go_to_login()
    logging.debug('Searching_by_10_symbols. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('Searching_by_10_symbols. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('Searching_by_10_symbols. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('Searching_by_10_symbols. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug('Searching_by_10_symbols. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_chrome_bs)
    ptk_main_page.get_task_name_10_symbols()
    logging.debug('Searching_by_10_symbols. Нашел первый проект и сохранил последние четыре цифры')
    ptk_main_page.searching_field_task_name_10_symbols()
    logging.debug('Searching_by_10_symbols. Ввел три последние цифры идентификатора проекта')
    ptk_main_page.assert_task_name_10_symbols()
    logging.debug('Searching_by_10_symbols. Нашел проект с соответствующим идентификатором')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Firefox. Поиск по десяти символам проекта.')
@allure.story('Успешный поиск по десяти символам проекта.')
@allure.severity('Critical')
def test_Searching_by_10_symbols_firefox(browser_firefox_bs):
    logger = logging.getLogger('modPTK_logger_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('Searching_by_10_symbols.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_firefox_bs)
    ptk_login_page.go_to_login()
    logging.debug('Searching_by_10_symbols. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('Searching_by_10_symbols. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('Searching_by_10_symbols. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('Searching_by_10_symbols. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug('Searching_by_10_symbols. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_firefox_bs)
    ptk_main_page.get_task_name_10_symbols()
    logging.debug('Searching_by_10_symbols. Нашел первый проект и сохранил последние четыре цифры')
    ptk_main_page.searching_field_task_name_10_symbols()
    logging.debug('Searching_by_10_symbols. Ввел три последние цифры идентификатора проекта')
    ptk_main_page.assert_task_name_10_symbols()
    logging.debug('Searching_by_10_symbols. Нашел проект с соответствующим идентификатором')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Edge. Поиск по десяти символам проекта.')
@allure.story('Успешный поиск по десяти символам проекта.')
@allure.severity('Critical')
def test_Searching_by_10_symbols_edge(browser_edge_bs):
    logger = logging.getLogger('modPTK_logger_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('Searching_by_10_symbols.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_pageptk_login_pageptk_login_page = SearchHelper(browser_edge_bs)
    ptk_login_pageptk_login_pageptk_login_page.go_to_login()
    logging.debug('Searching_by_10_symbols. Перешел на страницу логина')
    ptk_login_pageptk_login_pageptk_login_page.fill_admin_login()
    logging.debug('Searching_by_10_symbols. Заполнил логин')
    ptk_login_pageptk_login_pageptk_login_page.fill_admin_password()
    logging.debug('Searching_by_10_symbols. Заполнил пароль')
    ptk_login_pageptk_login_pageptk_login_page.click_login_button()
    logging.debug('Searching_by_10_symbols. Кликнул кнопку "Войти"')
    ptk_login_pageptk_login_pageptk_login_page.authorization_assert()
    logging.debug('Searching_by_10_symbols. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_edge_bs)
    ptk_main_page.get_task_name_10_symbols()
    logging.debug('Searching_by_10_symbols. Нашел первый проект и сохранил последние четыре цифры')
    ptk_main_page.searching_field_task_name_10_symbols()
    logging.debug('Searching_by_10_symbols. Ввел три последние цифры идентификатора проекта')
    ptk_main_page.assert_task_name_10_symbols()
    logging.debug('Searching_by_10_symbols. Нашел проект с соответствующим идентификатором')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Safari. Поиск по десяти символам проекта.')
@allure.story('Успешный поиск по десяти символам проекта.')
@allure.severity('Critical')
def test_Searching_by_10_symbols_safari(browser_safari_bs):
    logger = logging.getLogger('modPTK_logger_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('Searching_by_10_symbols.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_pageptk_login_page = SearchHelper(browser_safari_bs)
    ptk_login_pageptk_login_page.go_to_login()
    logging.debug('Searching_by_10_symbols. Перешел на страницу логина')
    ptk_login_pageptk_login_page.fill_admin_login()
    logging.debug('Searching_by_10_symbols. Заполнил логин')
    ptk_login_pageptk_login_page.fill_admin_password()
    logging.debug('Searching_by_10_symbols. Заполнил пароль')
    ptk_login_pageptk_login_page.click_login_button()
    logging.debug('Searching_by_10_symbols. Кликнул кнопку "Войти"')
    ptk_login_pageptk_login_page.authorization_assert()
    logging.debug('Searching_by_10_symbols. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_safari_bs)
    ptk_main_page.get_task_name_10_symbols()
    logging.debug('Searching_by_10_symbols. Нашел первый проект и сохранил последние четыре цифры')
    ptk_main_page.searching_field_task_name_10_symbols()
    logging.debug('Searching_by_10_symbols. Ввел три последние цифры идентификатора проекта')
    ptk_main_page.assert_task_name_10_symbols()
    logging.debug('Searching_by_10_symbols. Нашел проект с соответствующим идентификатором')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Chrome, разрешение 1280х1024. Поиск по десяти символам проекта.')
@allure.story('Успешный поиск по десяти символам проекта.')
@allure.severity('Critical')
def test_Searching_by_10_symbols_chrome_max_resolution(browser_chrome_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('Searching_by_10_symbols.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_chrome_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('Searching_by_10_symbols. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('Searching_by_10_symbols. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('Searching_by_10_symbols. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('Searching_by_10_symbols. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug('Searching_by_10_symbols. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_chrome_bs_max_resolution)
    ptk_main_page.get_task_name_10_symbols()
    logging.debug('Searching_by_10_symbols. Нашел первый проект и сохранил последние четыре цифры')
    ptk_main_page.searching_field_task_name_10_symbols()
    logging.debug('Searching_by_10_symbols. Ввел три последние цифры идентификатора проекта')
    ptk_main_page.assert_task_name_10_symbols()
    logging.debug('Searching_by_10_symbols. Нашел проект с соответствующим идентификатором')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Firefox, разрешение 1280х1024. Поиск по десяти символам проекта.')
@allure.story('Успешный поиск по десяти символам проекта.')
@allure.severity('Critical')
def test_Searching_by_10_symbols_firefox_max_resolution(browser_firefox_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('Searching_by_10_symbols.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_firefox_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('Searching_by_10_symbols. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('Searching_by_10_symbols. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('Searching_by_10_symbols. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('Searching_by_10_symbols. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug('Searching_by_10_symbols. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_firefox_bs_max_resolution)
    ptk_main_page.get_task_name_10_symbols()
    logging.debug('Searching_by_10_symbols. Нашел первый проект и сохранил последние четыре цифры')
    ptk_main_page.searching_field_task_name_10_symbols()
    logging.debug('Searching_by_10_symbols. Ввел три последние цифры идентификатора проекта')
    ptk_main_page.assert_task_name_10_symbols()
    logging.debug('Searching_by_10_symbols. Нашел проект с соответствующим идентификатором')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Edge, разрешение 1280х1024. Поиск по десяти символам проекта.')
@allure.story('Успешный поиск по десяти символам проекта.')
@allure.severity('Critical')
def test_Searching_by_10_symbols_edge_max_resolution(browser_edge_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('Searching_by_10_symbols.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_edge_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('Searching_by_10_symbols. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('Searching_by_10_symbols. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('Searching_by_10_symbols. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('Searching_by_10_symbols. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug('Searching_by_10_symbols. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_edge_bs_max_resolution)
    ptk_main_page.get_task_name_10_symbols()
    logging.debug('Searching_by_10_symbols. Нашел первый проект и сохранил последние четыре цифры')
    ptk_main_page.searching_field_task_name_10_symbols()
    logging.debug('Searching_by_10_symbols. Ввел три последние цифры идентификатора проекта')
    ptk_main_page.assert_task_name_10_symbols()
    logging.debug('Searching_by_10_symbols. Нашел проект с соответствующим идентификатором')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Safari, разрешение 1280х1024. Поиск по десяти символам проекта.')
@allure.story('Успешный поиск по десяти символам проекта.')
@allure.severity('Critical')
def test_Searching_by_10_symbols_safari_max_resolution(browser_safari_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('Searching_by_10_symbols.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_safari_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('Searching_by_10_symbols. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('Searching_by_10_symbols. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('Searching_by_10_symbols. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('Searching_by_10_symbols. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug('Searching_by_10_symbols. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_safari_bs_max_resolution)
    ptk_main_page.get_task_name_10_symbols()
    logging.debug('Searching_by_10_symbols. Нашел первый проект и сохранил последние четыре цифры')
    ptk_main_page.searching_field_task_name_10_symbols()
    logging.debug('Searching_by_10_symbols. Ввел три последние цифры идентификатора проекта')
    ptk_main_page.assert_task_name_10_symbols()
    logging.debug('Searching_by_10_symbols. Нашел проект с соответствующим идентификатором')
