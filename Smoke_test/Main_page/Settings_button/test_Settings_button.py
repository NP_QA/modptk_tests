import logging

import allure
import pytest

from Login_Page import SearchHelper
from Main_Page import SearchHelperMainPage


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Chrome. Кнопка настроек.')
@allure.story('Успешный клик на кнопку настроек.')
@allure.severity('Critical')
def test_settings_button_chrome(browser_chrome_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('settings_button.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_chrome_bs)
    ptk_login_page.go_to_login()
    logging.debug('settings_button_chrome. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('settings_button_chrome. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('settings_button_chrome. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('settings_button_chrome. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug('settings_button_chrome. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_chrome_bs)
    ptk_main_page.click_three_dots_icon()
    logging.debug('settings_button_chrome. Кликнул на символ троеточия')
    ptk_main_page.main_page_click_settings_button()
    logging.debug('settings_button_chrome. Кликнул на кнопку параметры')
    ptk_main_page.assert_table_of_investments()
    logging.debug('settings_button_chrome. Нашел все элементы таблицы параметров')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Firefox. Кнопка настроек.')
@allure.story('Успешный клик на кнопку настроек.')
@allure.severity('Critical')
def test_settings_button_firefox(browser_firefox_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('settings_button.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_firefox_bs)
    ptk_login_page.go_to_login()
    logging.debug('settings_button_firefox. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('settings_button_chrome. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('settings_button_chrome. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('settings_button_chrome. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug('settings_button_chrome. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_firefox_bs)
    ptk_main_page.click_three_dots_icon()
    logging.debug('settings_button_chrome. Кликнул на символ троеточия')
    ptk_main_page.main_page_click_settings_button()
    logging.debug('settings_button_chrome. Кликнул на кнопку параметры')
    ptk_main_page.assert_table_of_investments()
    logging.debug('settings_button_chrome. Нашел все элементы таблицы параметров')



@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Edge. Кнопка настроек.')
@allure.story('Успешный клик на кнопку настроек.')
@allure.severity('Critical')
def test_settings_button_edge(browser_edge_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('settings_button.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_edge_bs)
    ptk_login_page.go_to_login()
    logging.debug('settings_button_edge. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('settings_button_chrome. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('settings_button_chrome. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('settings_button_chrome. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug('settings_button_chrome. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_edge_bs)
    ptk_main_page.click_three_dots_icon()
    logging.debug('settings_button_chrome. Кликнул на символ троеточия')
    ptk_main_page.main_page_click_settings_button()
    logging.debug('settings_button_chrome. Кликнул на кнопку параметры')
    ptk_main_page.assert_table_of_investments()
    logging.debug('settings_button_chrome. Нашел все элементы таблицы параметров')



@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Safari. Кнопка настроек.')
@allure.story('Успешный клик на кнопку настроек.')
@allure.severity('Critical')
def test_settings_button_safari(browser_safari_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('settings_button.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_safari_bs)
    ptk_login_page.go_to_login()
    logging.debug('settings_button_safari. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('settings_button_chrome. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('settings_button_chrome. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('settings_button_chrome. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug('settings_button_chrome. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_safari_bs)
    ptk_main_page.click_three_dots_icon()
    logging.debug('settings_button_chrome. Кликнул на символ троеточия')
    ptk_main_page.main_page_click_settings_button()
    logging.debug('settings_button_chrome. Кликнул на кнопку параметры')
    ptk_main_page.assert_table_of_investments()
    logging.debug('settings_button_chrome. Нашел все элементы таблицы параметров')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Chrome, разрешение 1280х1024. Кнопка настроек.')
@allure.story('Успешный клик на кнопку настроек.')
@allure.severity('Critical')
def test_settings_button_chrome_max_resolution(browser_chrome_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('settings_button.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_chrome_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('settings_button_chrome. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('settings_button_chrome. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('settings_button_chrome. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('settings_button_chrome. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug('settings_button_chrome. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_chrome_bs_max_resolution)
    ptk_main_page.click_three_dots_icon()
    logging.debug('settings_button_chrome. Кликнул на символ троеточия')
    ptk_main_page.main_page_click_settings_button()
    logging.debug('settings_button_chrome. Кликнул на кнопку параметры')
    ptk_main_page.assert_table_of_investments()
    logging.debug('settings_button_chrome. Нашел все элементы таблицы параметров')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Firefox, разрешение 1280х1024. Кнопка настроек.')
@allure.story('Успешный клик на кнопку настроек.')
@allure.severity('Critical')
def test_settings_button_firefox_max_resolution(browser_firefox_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('settings_button.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_firefox_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('settings_button_firefox. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('settings_button_chrome. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('settings_button_chrome. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('settings_button_chrome. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug('settings_button_chrome. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_firefox_bs_max_resolution)
    ptk_main_page.click_three_dots_icon()
    logging.debug('settings_button_chrome. Кликнул на символ троеточия')
    ptk_main_page.main_page_click_settings_button()
    logging.debug('settings_button_chrome. Кликнул на кнопку параметры')
    ptk_main_page.assert_table_of_investments()
    logging.debug('settings_button_chrome. Нашел все элементы таблицы параметров')



@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Edge, разрешение 1280х1024. Кнопка настроек.')
@allure.story('Успешный клик на кнопку настроек.')
@allure.severity('Critical')
def test_settings_button_edge_max_resolution(browser_edge_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('settings_button.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_edge_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('settings_button_edge. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('settings_button_chrome. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('settings_button_chrome. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('settings_button_chrome. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug('settings_button_chrome. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_edge_bs_max_resolution)
    ptk_main_page.click_three_dots_icon()
    logging.debug('settings_button_chrome. Кликнул на символ троеточия')
    ptk_main_page.main_page_click_settings_button()
    logging.debug('settings_button_chrome. Кликнул на кнопку параметры')
    ptk_main_page.assert_table_of_investments()
    logging.debug('settings_button_chrome. Нашел все элементы таблицы параметров')



@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Safari, разрешение 1280х1024. Кнопка настроек.')
@allure.story('Успешный клик на кнопку настроек.')
@allure.severity('Critical')
def test_settings_button_safari_max_resolution(browser_safari_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('settings_button.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_safari_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('settings_button_safari. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('settings_button_chrome. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('settings_button_chrome. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('settings_button_chrome. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug('settings_button_chrome. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_safari_bs_max_resolution)
    ptk_main_page.click_three_dots_icon()
    logging.debug('settings_button_chrome. Кликнул на символ троеточия')
    ptk_main_page.main_page_click_settings_button()
    logging.debug('settings_button_chrome. Кликнул на кнопку параметры')
    ptk_main_page.assert_table_of_investments()
    logging.debug('settings_button_chrome. Нашел все элементы таблицы параметров')
