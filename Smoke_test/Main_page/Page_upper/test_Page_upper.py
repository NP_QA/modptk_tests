import logging

import allure
import pytest

from Login_Page import SearchHelper
from Main_Page import SearchHelperMainPage


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Chrome. Подъемник страницы.')
@allure.story('Успешный подъем страницы.')
@allure.severity('Critical')
def test_page_upper_chrome(browser_chrome_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('page_upper.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_chrome_bs)
    ptk_login_page.go_to_login()
    logging.debug('page_upper_chrome. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('page_upper_chrome. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('page_upper_chrome. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('page_upper_chrome. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug('page_upper_chrome. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_chrome_bs)
    ptk_main_page.scroll_page_down()
    logging.debug('page_upper_chrome. Проскролил страницу вниз')
    ptk_main_page.wait_for_loading()
    ptk_main_page.click_on_page_upper()
    logging.debug('page_upper_chrome. Кликнул на подъемник')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Firefox. Подъемник страницы.')
@allure.story('Успешный подъем страницы.')
@allure.severity('Critical')
def test_page_upper_firefox(browser_firefox_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('page_upper.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_firefox_bs)
    ptk_login_page.go_to_login()
    logging.debug('page_upper_firefox. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('page_upper_firefox. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('page_upper_firefox. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('page_upper_firefox. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug('page_upper_firefox. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_firefox_bs)
    ptk_main_page.scroll_page_down()
    logging.debug('page_upper_firefox. Проскролил страницу вниз')
    ptk_main_page.wait_for_loading()
    ptk_main_page.click_on_page_upper()
    logging.debug('page_upper_firefox. Кликнул на подъемник')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Edge. Подъемник страницы.')
@allure.story('Успешный подъем страницы.')
@allure.severity('Critical')
def test_page_upper_edge(browser_edge_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('page_upper.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_edge_bs)
    ptk_login_page.go_to_login()
    logging.debug('page_upper_edge. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('page_upper_edge. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('page_upper_edge. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('page_upper_edge. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug('page_upper_edge. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_edge_bs)
    ptk_main_page.scroll_page_down()
    logging.debug('page_upper_edge. Проскролил страницу вниз')
    ptk_main_page.wait_for_loading()
    ptk_main_page.click_on_page_upper()
    logging.debug('page_upper_edge. Кликнул на подъемник')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Safari. Подъемник страницы.')
@allure.story('Успешный подъем страницы.')
@allure.severity('Critical')
def test_page_upper_safari(browser_safari_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('page_upper.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_safari_bs)
    ptk_login_page.go_to_login()
    logging.debug('page_upper_safari. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('page_upper_safari. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('page_upper_safari. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('page_upper_safari. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug('page_upper_safari. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_safari_bs)
    ptk_main_page.scroll_page_down()
    logging.debug('page_upper_safari. Проскролил страницу вниз')
    ptk_main_page.wait_for_loading()
    ptk_main_page.click_on_page_upper()
    logging.debug('page_upper_safari. Кликнул на подъемник')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Chrome, разрешение 1280х1024. Подъемник страницы.')
@allure.story('Успешный подъем страницы.')
@allure.severity('Critical')
def test_page_upper_chrome_max_resolution(browser_chrome_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('page_upper.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_chrome_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('page_upper_chrome. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('page_upper_chrome. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('page_upper_chrome. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('page_upper_chrome. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug('page_upper_chrome. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_chrome_bs_max_resolution)
    ptk_main_page.scroll_page_down()
    logging.debug('page_upper_safari. Проскролил страницу вниз')
    ptk_main_page.wait_for_loading()
    ptk_main_page.click_on_page_upper()
    logging.debug('page_upper_safari. Кликнул на подъемник')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Firefox, разрешение 1280х1024. Подъемник страницы.')
@allure.story('Успешный подъем страницы.')
@allure.severity('Critical')
def test_page_upper_firefox_max_resolution(browser_firefox_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('page_upper.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_firefox_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('page_upper_firefox. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('page_upper_firefox. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('page_upper_firefox. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('page_upper_firefox. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug('page_upper_firefox. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_firefox_bs_max_resolution)
    ptk_main_page.scroll_page_down()
    logging.debug('page_upper_firefox. Проскролил страницу вниз')
    ptk_main_page.wait_for_loading()
    ptk_main_page.click_on_page_upper()
    logging.debug('page_upper_firefox. Кликнул на подъемник')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Edge, разрешение 1280х1024. Подъемник страницы.')
@allure.story('Успешный подъем страницы.')
@allure.severity('Critical')
def test_page_upper_edge_max_resolution(browser_edge_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('page_upper.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_edge_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('page_upper_edge. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('page_upper_edge. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('page_upper_edge. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('page_upper_edge. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug('page_upper_edge. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_edge_bs_max_resolution)
    ptk_main_page.scroll_page_down()
    logging.debug('page_upper_edge. Проскролил страницу вниз')
    ptk_main_page.wait_for_loading()
    ptk_main_page.click_on_page_upper()
    logging.debug('page_upper_edge. Кликнул на подъемник')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Safari, разрешение 1280х1024. Подъемник страницы.')
@allure.story('Успешный подъем страницы.')
@allure.severity('Critical')
def test_page_upper_safari_max_resolution(browser_safari_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('page_upper.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_safari_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('page_upper_safari. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('page_upper_safari. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('page_upper_safari. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('page_upper_safari. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug('page_upper_safari. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_safari_bs_max_resolution)
    ptk_main_page.scroll_page_down()
    logging.debug('page_upper_safari. Проскролил страницу вниз')
    ptk_main_page.wait_for_loading()
    ptk_main_page.click_on_page_upper()
    logging.debug('page_upper_safari. Кликнул на подъемник')
