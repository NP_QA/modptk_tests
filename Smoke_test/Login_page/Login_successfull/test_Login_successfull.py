import logging

import allure
import pytest

from Login_Page import SearchHelper


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Chrome. Авторизация.')
@allure.story('Успешная авторизация')
@allure.severity('Critical')
def test_admin_login_successfull_chrome(browser_chrome_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('admin_login_successfull.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_chrome_bs)
    ptk_login_page.go_to_login()
    logging.debug('Admin_login_successfull_chrome. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('Admin_login_successfull_chrome. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('Admin_login_successfull_chrome. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('Admin_login_successfull_chrome. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'Admin_login_successfull_chrome. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Firefox. Авторизация.')
@allure.story('Успешная авторизация')
@allure.severity('Critical')
def test_admin_login_successfull_firefox(browser_firefox_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('admin_login_successfull.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_firefox_bs)
    ptk_login_page.go_to_login()
    logging.debug('Admin_login_successfull_firefox. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('Admin_login_successfull_firefox. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('Admin_login_successfull_firefox. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('Admin_login_successfull_firefox. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'Admin_login_successfull_firefox. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Edge. Авторизация.')
@allure.story('Успешная авторизация')
@allure.severity('Critical')
def test_admin_login_successfull_edge(browser_edge_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('admin_login_successfull.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_edge_bs)
    ptk_login_page.go_to_login()
    logging.debug('Admin_login_successfull_edge. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('Admin_login_successfull_edge. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('Admin_login_successfull_edge. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('Admin_login_successfull_edge. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'Admin_login_successfull_edge. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Safari. Авторизация.')
@allure.story('Успешная авторизация')
@allure.severity('Critical')
def test_admin_login_successfull_safari(browser_safari_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('admin_login_successfull.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_safari_bs)
    ptk_login_page.go_to_login()
    logging.debug('Admin_login_successfull_safari. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('Admin_login_successfull_safari. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('Admin_login_successfull_safari. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('Admin_login_successfull_safari. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'Admin_login_successfull_safari. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Chrome, разрешение 1280х1024. Авторизация.')
@allure.story('Успешная авторизация')
@allure.severity('Critical')
def test_admin_login_successfull_chrome_max_resolution(browser_chrome_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('admin_login_successfull.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_chrome_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('Admin_login_successfull_chrome_max_resolution. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('Admin_login_successfull_chrome_max_resolution. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('Admin_login_successfull_chrome_max_resolution. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('Admin_login_successfull_chrome_max_resolution. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug('Admin_login_successfull_chrome_max_resolution. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Firefox, разрешение 1280х1024. Авторизация.')
@allure.story('Успешная авторизация')
@allure.severity('Critical')
def test_admin_login_successfull_firefox_max_resolution(browser_firefox_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('admin_login_successfull.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_firefox_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('Admin_login_successfull_firefox_max_resolution. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('Admin_login_successfull_firefox_max_resolution. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('Admin_login_successfull_firefox_max_resolution. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('Admin_login_successfull_firefox_max_resolution. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'Admin_login_successfull_firefox_max_resolution. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Edge, разрешение 1280х1024. Авторизация.')
@allure.story('Успешная авторизация')
@allure.severity('Critical')
def test_admin_login_successfull_edge_max_resolution(browser_edge_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('admin_login_successfull.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_edge_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('Admin_login_successfull_edge_max_resolution. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('Admin_login_successfull_edge_max_resolution. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('Admin_login_successfull_edge_max_resolution. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('Admin_login_successfull_edge_max_resolution. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'Admin_login_successfull_edge_max_resolution. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Safari, разрешение 1280х1024. Авторизация.')
@allure.story('Успешная авторизация')
@allure.severity('Critical')
def test_admin_login_successfull_safari_max_resolution(browser_safari_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('admin_login_successfull.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_safari_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('Admin_login_successfull_safari_max_resolution. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('Admin_login_successfull_safari_max_resolution. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('Admin_login_successfull_safari_max_resolution. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('Admin_login_successfull_safari_max_resolution. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'Admin_login_successfull_safari_max_resolution. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
