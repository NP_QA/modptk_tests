import logging

import allure
import pytest

from Login_Page import SearchHelper
from Project_Page import SearchHelperProjectPage


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Chrome. Добавления комментария в карточке проекта.')
@allure.story('Успешное добавление комментария в карточке проекта.')
@allure.severity('Critical')
def test_project_card_add_commentary_chrome(browser_chrome_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('add_commentary.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_chrome_bs)
    ptk_login_page.go_to_login()
    logging.debug('project_card_add_commentary. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('project_card_add_commentary. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('project_card_add_commentary. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('project_card_add_commentary. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'project_card_add_commentary. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_project_page = SearchHelperProjectPage(browser_chrome_bs)
    ptk_project_page.go_to_personal_project()
    logging.debug(
        'project_card_add_commentary.Перешел в персональный проект')
    ptk_project_page.project_card_click_comment_icon()
    logging.debug(
        'project_card_add_commentary.Кликнул кнопку добавления комментария')
    ptk_project_page.project_card_click_edit_comment()
    logging.debug(
        'project_card_add_commentary.Кликнул кнопку редактирования комментария')
    ptk_project_page.project_card_fill_comment_textarea()
    logging.debug(
        'project_card_add_commentary.Написал текст комментария')
    ptk_project_page.project_card_save_comment_button()
    logging.debug(
        'project_card_add_commentary.Кликнул на кнопку сохранения')
    ptk_project_page.project_card_close_comment_textarea()
    logging.debug(
        'project_card_add_commentary.Кликнул на кнопку сохранения')
    ptk_project_page.project_card_click_comment_icon()
    logging.debug(
        'project_card_add_commentary.Кликнул кнопку добавления комментария')
    ptk_project_page.project_card_find_comment_text()
    logging.debug(
        'project_card_add_commentary.Убедился, что комментарий записан и сохранен')
    ptk_project_page.project_card_click_edit_comment()
    logging.debug(
        'project_card_add_commentary.Кликнул кнопку редактирования комментария')
    ptk_project_page.project_card_clear_comment_textarea()
    logging.debug(
        'project_card_add_commentary.Очистил комментарий')
    ptk_project_page.project_card_save_comment_button()
    logging.debug(
        'project_card_add_commentary.Кликнул на кнопку сохранения')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Firefox. Добавления комментария в карточке проекта.')
@allure.story('Успешное добавление комментария в карточке проекта.')
@allure.severity('Critical')
def test_project_card_add_commentary_firefox(browser_firefox_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('add_commentary.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_firefox_bs)
    ptk_login_page.go_to_login()
    logging.debug('project_card_add_commentary. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('project_card_add_commentary. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('project_card_add_commentary. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('project_card_add_commentary. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'project_card_add_commentary. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_project_page = SearchHelperProjectPage(browser_firefox_bs)
    ptk_project_page.go_to_personal_project()
    logging.debug(
        'project_card_add_commentary.Перешел в персональный проект')
    ptk_project_page.project_card_click_comment_icon()
    logging.debug(
        'project_card_add_commentary.Кликнул кнопку добавления комментария')
    ptk_project_page.project_card_click_edit_comment()
    logging.debug(
        'project_card_add_commentary.Кликнул кнопку редактирования комментария')
    ptk_project_page.project_card_fill_comment_textarea()
    logging.debug(
        'project_card_add_commentary.Написал текст комментария')
    ptk_project_page.project_card_save_comment_button()
    logging.debug(
        'project_card_add_commentary.Кликнул на кнопку сохранения')
    ptk_project_page.project_card_close_comment_textarea()
    logging.debug(
        'project_card_add_commentary.Кликнул на кнопку сохранения')
    ptk_project_page.project_card_click_comment_icon()
    logging.debug(
        'project_card_add_commentary.Кликнул кнопку добавления комментария')
    ptk_project_page.project_card_find_comment_text()
    logging.debug(
        'project_card_add_commentary.Убедился, что комментарий записан и сохранен')
    ptk_project_page.project_card_click_edit_comment()
    logging.debug(
        'project_card_add_commentary.Кликнул кнопку редактирования комментария')
    ptk_project_page.project_card_clear_comment_textarea()
    logging.debug(
        'project_card_add_commentary.Очистил комментарий')
    ptk_project_page.project_card_save_comment_button()
    logging.debug(
        'project_card_add_commentary.Кликнул на кнопку сохранения')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Edge. Добавления комментария в карточке проекта.')
@allure.story('Успешное добавление комментария в карточке проекта.')
@allure.severity('Critical')
def test_project_card_add_commentary_edge(browser_edge_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('add_commentary.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_edge_bs)
    ptk_login_page.go_to_login()
    logging.debug('project_card_add_commentary. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('project_card_add_commentary. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('project_card_add_commentary. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('project_card_add_commentary. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'project_card_add_commentary. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_project_page = SearchHelperProjectPage(browser_edge_bs)
    ptk_project_page.go_to_personal_project()
    logging.debug(
        'project_card_add_commentary.Перешел в персональный проект')
    ptk_project_page.project_card_click_comment_icon()
    logging.debug(
        'project_card_add_commentary.Кликнул кнопку добавления комментария')
    ptk_project_page.project_card_click_edit_comment()
    logging.debug(
        'project_card_add_commentary.Кликнул кнопку редактирования комментария')
    ptk_project_page.project_card_fill_comment_textarea()
    logging.debug(
        'project_card_add_commentary.Написал текст комментария')
    ptk_project_page.project_card_save_comment_button()
    logging.debug(
        'project_card_add_commentary.Кликнул на кнопку сохранения')
    ptk_project_page.project_card_close_comment_textarea()
    logging.debug(
        'project_card_add_commentary.Кликнул на кнопку сохранения')
    ptk_project_page.project_card_click_comment_icon()
    logging.debug(
        'project_card_add_commentary.Кликнул кнопку добавления комментария')
    ptk_project_page.project_card_find_comment_text()
    logging.debug(
        'project_card_add_commentary.Убедился, что комментарий записан и сохранен')
    ptk_project_page.project_card_click_edit_comment()
    logging.debug(
        'project_card_add_commentary.Кликнул кнопку редактирования комментария')
    ptk_project_page.project_card_clear_comment_textarea()
    logging.debug(
        'project_card_add_commentary.Очистил комментарий')
    ptk_project_page.project_card_save_comment_button()
    logging.debug(
        'project_card_add_commentary.Кликнул на кнопку сохранения')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Safari. Добавления комментария в карточке проекта.')
@allure.story('Успешное добавление комментария в карточке проекта.')
@allure.severity('Critical')
def test_project_card_add_commentary_safari(browser_safari_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('add_commentary.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_safari_bs)
    ptk_login_page.go_to_login()
    logging.debug('project_card_add_commentary. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('project_card_add_commentary. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('project_card_add_commentary. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('project_card_add_commentary. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'project_card_add_commentary. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_project_page = SearchHelperProjectPage(browser_safari_bs)
    ptk_project_page.go_to_personal_project()
    logging.debug(
        'project_card_add_commentary.Перешел в персональный проект')
    ptk_project_page.project_card_click_comment_icon()
    logging.debug(
        'project_card_add_commentary.Кликнул кнопку добавления комментария')
    ptk_project_page.project_card_click_edit_comment()
    logging.debug(
        'project_card_add_commentary.Кликнул кнопку редактирования комментария')
    ptk_project_page.project_card_fill_comment_textarea()
    logging.debug(
        'project_card_add_commentary.Написал текст комментария')
    ptk_project_page.project_card_save_comment_button()
    logging.debug(
        'project_card_add_commentary.Кликнул на кнопку сохранения')
    ptk_project_page.project_card_close_comment_textarea()
    logging.debug(
        'project_card_add_commentary.Кликнул на кнопку сохранения')
    ptk_project_page.project_card_click_comment_icon()
    logging.debug(
        'project_card_add_commentary.Кликнул кнопку добавления комментария')
    ptk_project_page.project_card_find_comment_text()
    logging.debug(
        'project_card_add_commentary.Убедился, что комментарий записан и сохранен')
    ptk_project_page.project_card_click_edit_comment()
    logging.debug(
        'project_card_add_commentary.Кликнул кнопку редактирования комментария')
    ptk_project_page.project_card_clear_comment_textarea()
    logging.debug(
        'project_card_add_commentary.Очистил комментарий')
    ptk_project_page.project_card_save_comment_button()
    logging.debug(
        'project_card_add_commentary.Кликнул на кнопку сохранения')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Chrome, разрешение 1280х1024. Добавления комментария в карточке проекта.')
@allure.story('Успешное добавление комментария в карточке проекта.')
@allure.severity('Critical')
def test_project_card_add_commentary_chrome_max_resolution(browser_chrome_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('add_commentary.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_chrome_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('project_card_add_commentary. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('project_card_add_commentary. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('project_card_add_commentary. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('project_card_add_commentary. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'project_card_add_commentary. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_project_page = SearchHelperProjectPage(browser_chrome_bs_max_resolution)
    ptk_project_page.go_to_personal_project()
    logging.debug(
        'project_card_add_commentary.Перешел в персональный проект')
    ptk_project_page.project_card_click_comment_icon()
    logging.debug(
        'project_card_add_commentary.Кликнул кнопку добавления комментария')
    ptk_project_page.project_card_click_edit_comment()
    logging.debug(
        'project_card_add_commentary.Кликнул кнопку редактирования комментария')
    ptk_project_page.project_card_fill_comment_textarea()
    logging.debug(
        'project_card_add_commentary.Написал текст комментария')
    ptk_project_page.project_card_save_comment_button()
    logging.debug(
        'project_card_add_commentary.Кликнул на кнопку сохранения')
    ptk_project_page.project_card_close_comment_textarea()
    logging.debug(
        'project_card_add_commentary.Кликнул на кнопку сохранения')
    ptk_project_page.project_card_click_comment_icon()
    logging.debug(
        'project_card_add_commentary.Кликнул кнопку добавления комментария')
    ptk_project_page.project_card_find_comment_text()
    logging.debug(
        'project_card_add_commentary.Убедился, что комментарий записан и сохранен')
    ptk_project_page.project_card_click_edit_comment()
    logging.debug(
        'project_card_add_commentary.Кликнул кнопку редактирования комментария')
    ptk_project_page.project_card_clear_comment_textarea()
    logging.debug(
        'project_card_add_commentary.Очистил комментарий')
    ptk_project_page.project_card_save_comment_button()
    logging.debug(
        'project_card_add_commentary.Кликнул на кнопку сохранения')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Firefox, разрешение 1280х1024. Добавления комментария в карточке проекта.')
@allure.story('Успешное добавление комментария в карточке проекта.')
@allure.severity('Critical')
def test_project_card_add_commentary_firefox_max_resolution(browser_firefox_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('add_commentary.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_firefox_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('project_card_add_commentary. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('project_card_add_commentary. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('project_card_add_commentary. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('project_card_add_commentary. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'project_card_add_commentary. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_project_page = SearchHelperProjectPage(browser_firefox_bs_max_resolution)
    ptk_project_page.go_to_personal_project()
    logging.debug(
        'project_card_add_commentary.Перешел в персональный проект')
    ptk_project_page.project_card_click_comment_icon()
    logging.debug(
        'project_card_add_commentary.Кликнул кнопку добавления комментария')
    ptk_project_page.project_card_click_edit_comment()
    logging.debug(
        'project_card_add_commentary.Кликнул кнопку редактирования комментария')
    ptk_project_page.project_card_fill_comment_textarea()
    logging.debug(
        'project_card_add_commentary.Написал текст комментария')
    ptk_project_page.project_card_save_comment_button()
    logging.debug(
        'project_card_add_commentary.Кликнул на кнопку сохранения')
    ptk_project_page.project_card_close_comment_textarea()
    logging.debug(
        'project_card_add_commentary.Кликнул на кнопку сохранения')
    ptk_project_page.project_card_click_comment_icon()
    logging.debug(
        'project_card_add_commentary.Кликнул кнопку добавления комментария')
    ptk_project_page.project_card_find_comment_text()
    logging.debug(
        'project_card_add_commentary.Убедился, что комментарий записан и сохранен')
    ptk_project_page.project_card_click_edit_comment()
    logging.debug(
        'project_card_add_commentary.Кликнул кнопку редактирования комментария')
    ptk_project_page.project_card_clear_comment_textarea()
    logging.debug(
        'project_card_add_commentary.Очистил комментарий')
    ptk_project_page.project_card_save_comment_button()
    logging.debug(
        'project_card_add_commentary.Кликнул на кнопку сохранения')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Edge, разрешение 1280х1024. Добавления комментария в карточке проекта.')
@allure.story('Успешное добавление комментария в карточке проекта.')
@allure.severity('Critical')
def test_project_card_add_commentary_max_resolution(browser_edge_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('add_commentary.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_edge_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('project_card_add_commentary. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('project_card_add_commentary. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('project_card_add_commentary. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('project_card_add_commentary. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'project_card_add_commentary. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_project_page = SearchHelperProjectPage(browser_edge_bs_max_resolution)
    ptk_project_page.go_to_personal_project()
    logging.debug(
        'project_card_add_commentary.Перешел в персональный проект')
    ptk_project_page.project_card_click_comment_icon()
    logging.debug(
        'project_card_add_commentary.Кликнул кнопку добавления комментария')
    ptk_project_page.project_card_click_edit_comment()
    logging.debug(
        'project_card_add_commentary.Кликнул кнопку редактирования комментария')
    ptk_project_page.project_card_fill_comment_textarea()
    logging.debug(
        'project_card_add_commentary.Написал текст комментария')
    ptk_project_page.project_card_save_comment_button()
    logging.debug(
        'project_card_add_commentary.Кликнул на кнопку сохранения')
    ptk_project_page.project_card_close_comment_textarea()
    logging.debug(
        'project_card_add_commentary.Кликнул на кнопку сохранения')
    ptk_project_page.project_card_click_comment_icon()
    logging.debug(
        'project_card_add_commentary.Кликнул кнопку добавления комментария')
    ptk_project_page.project_card_find_comment_text()
    logging.debug(
        'project_card_add_commentary.Убедился, что комментарий записан и сохранен')
    ptk_project_page.project_card_click_edit_comment()
    logging.debug(
        'project_card_add_commentary.Кликнул кнопку редактирования комментария')
    ptk_project_page.project_card_clear_comment_textarea()
    logging.debug(
        'project_card_add_commentary.Очистил комментарий')
    ptk_project_page.project_card_save_comment_button()
    logging.debug(
        'project_card_add_commentary.Кликнул на кнопку сохранения')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Safari, разрешение 1280х1024. Добавления комментария в карточке проекта.')
@allure.story('Успешное добавление комментария в карточке проекта.')
@allure.severity('Critical')
def test_project_card_add_commentary_max_resolution(browser_safari_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('add_commentary.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_safari_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('project_card_add_commentary. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('project_card_add_commentary. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('project_card_add_commentary. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('project_card_add_commentary. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'project_card_add_commentary. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_project_page = SearchHelperProjectPage(browser_safari_bs_max_resolution)
    ptk_project_page.go_to_personal_project()
    logging.debug(
        'project_card_add_commentary.Перешел в персональный проект')
    ptk_project_page.project_card_click_comment_icon()
    logging.debug(
        'project_card_add_commentary.Кликнул кнопку добавления комментария')
    ptk_project_page.project_card_click_edit_comment()
    logging.debug(
        'project_card_add_commentary.Кликнул кнопку редактирования комментария')
    ptk_project_page.project_card_fill_comment_textarea()
    logging.debug(
        'project_card_add_commentary.Написал текст комментария')
    ptk_project_page.project_card_save_comment_button()
    logging.debug(
        'project_card_add_commentary.Кликнул на кнопку сохранения')
    ptk_project_page.project_card_close_comment_textarea()
    logging.debug(
        'project_card_add_commentary.Кликнул на кнопку сохранения')
    ptk_project_page.project_card_click_comment_icon()
    logging.debug(
        'project_card_add_commentary.Кликнул кнопку добавления комментария')
    ptk_project_page.project_card_find_comment_text()
    logging.debug(
        'project_card_add_commentary.Убедился, что комментарий записан и сохранен')
    ptk_project_page.project_card_click_edit_comment()
    logging.debug(
        'project_card_add_commentary.Кликнул кнопку редактирования комментария')
    ptk_project_page.project_card_clear_comment_textarea()
    logging.debug(
        'project_card_add_commentary.Очистил комментарий')
    ptk_project_page.project_card_save_comment_button()
    logging.debug(
        'project_card_add_commentary.Кликнул на кнопку сохранения')
