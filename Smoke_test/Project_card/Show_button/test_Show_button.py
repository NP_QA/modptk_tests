import logging

import allure
import pytest

from Login_Page import SearchHelper
from Project_Page import SearchHelperProjectPage


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Chrome. Кнопка "Показать".')
@allure.story('Успешный клик на кнопку "Показать".')
@allure.severity('Critical')
def test_show_button_chrome(browser_chrome_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('Show_button.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_chrome_bs)
    ptk_login_page.go_to_login()
    logging.debug('Show_button. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('Show_button. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('Show_button. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('Show_button. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'Show_button. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_project_page = SearchHelperProjectPage(browser_chrome_bs)
    ptk_project_page.go_to_another_personal_project()
    logging.debug(
        'Show_button.Перешел на тестовый проект')
    ptk_project_page.project_description_text()
    logging.debug(
        'Show_button.Нашел первую часть текста описания объекта')
    ptk_project_page.show_button()
    logging.debug(
        'Show_button.Кликнул на кнопку "Показать полностью"')
    ptk_project_page.project_description_text_part_2()
    logging.debug(
        'Show_button.Нашел вторую часть текста описания объекта')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Edge. Кнопка "Показать".')
@allure.story('Успешный клик на кнопку "Показать".')
@allure.severity('Critical')
def test_show_button_edge(browser_edge_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('Show_button.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_edge_bs)
    ptk_login_page.go_to_login()
    logging.debug('Show_button. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('Show_button. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('Show_button. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('Show_button. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'Show_button. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_project_page = SearchHelperProjectPage(browser_edge_bs)
    ptk_project_page.go_to_another_personal_project()
    logging.debug(
        'Show_button.Перешел на тестовый проект')
    ptk_project_page.project_description_text()
    logging.debug(
        'Show_button.Нашел первую часть текста описания объекта')
    ptk_project_page.show_button()
    logging.debug(
        'Show_button.Кликнул на кнопку "Показать полностью"')
    ptk_project_page.project_description_text_part_2()
    logging.debug(
        'Show_button.Нашел вторую часть текста описания объекта')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Firefox. Кнопка "Показать".')
@allure.story('Успешный клик на кнопку "Показать".')
@allure.severity('Critical')
def test_show_button_firefox(browser_firefox_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('Show_button.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_firefox_bs)
    ptk_login_page.go_to_login()
    logging.debug('Show_button. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('Show_button. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('Show_button. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('Show_button. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'Show_button. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_project_page = SearchHelperProjectPage(browser_firefox_bs)
    ptk_project_page.go_to_another_personal_project()
    logging.debug(
            'Show_button.Перешел на тестовый проект')
    ptk_project_page.project_description_text()
    logging.debug(
            'Show_button.Нашел первую часть текста описания объекта')
    ptk_project_page.show_button()
    logging.debug(
            'Show_button.Кликнул на кнопку "Показать полностью"')
    ptk_project_page.project_description_text_part_2()
    logging.debug(
            'Show_button.Нашел вторую часть текста описания объекта')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Safari. Кнопка "Показать".')
@allure.story('Успешный клик на кнопку "Показать".')
@allure.severity('Critical')
def test_show_button_safari(browser_safari_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('Show_button.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_safari_bs)
    ptk_login_page.go_to_login()
    logging.debug('Show_button. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('Show_button. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('Show_button. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('Show_button. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'Show_button. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_project_page = SearchHelperProjectPage(browser_safari_bs)
    ptk_project_page.go_to_another_personal_project()
    logging.debug(
            'Show_button.Перешел на тестовый проект')
    ptk_project_page.project_description_text()
    logging.debug(
            'Show_button.Нашел первую часть текста описания объекта')
    ptk_project_page.show_button()
    logging.debug(
            'Show_button.Кликнул на кнопку "Показать полностью"')
    ptk_project_page.project_description_text_part_2()
    logging.debug(
            'Show_button.Нашел вторую часть текста описания объекта')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Chrome, разрешение 1280х1024. Кнопка "Показать".')
@allure.story('Успешный клик на кнопку "Показать".')
@allure.severity('Critical')
def test_show_button_chrome_max_resolution(browser_chrome_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('Show_button.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_chrome_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('Show_button. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('Show_button. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('Show_button. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('Show_button. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'Show_button. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_project_page = SearchHelperProjectPage(browser_chrome_bs_max_resolution)
    ptk_project_page.go_to_another_personal_project()
    logging.debug(
            'Show_button.Перешел на тестовый проект')
    ptk_project_page.project_description_text()
    logging.debug(
            'Show_button.Нашел первую часть текста описания объекта')
    ptk_project_page.show_button()
    logging.debug(
            'Show_button.Кликнул на кнопку "Показать полностью"')
    ptk_project_page.project_description_text_part_2()
    logging.debug(
            'Show_button.Нашел вторую часть текста описания объекта')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Firefox, разрешение 1280х1024. Кнопка "Показать".')
@allure.story('Успешный клик на кнопку "Показать".')
@allure.severity('Critical')
def test_show_button_firefox_max_resolution(browser_firefox_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('Show_button.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_firefox_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('Show_button. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('Show_button. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('Show_button. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('Show_button. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'Show_button. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_project_page = SearchHelperProjectPage(browser_firefox_bs_max_resolution)
    ptk_project_page.go_to_another_personal_project()
    logging.debug(
            'Show_button.Перешел на тестовый проект')
    ptk_project_page.project_description_text()
    logging.debug(
            'Show_button.Нашел первую часть текста описания объекта')
    ptk_project_page.show_button()
    logging.debug(
            'Show_button.Кликнул на кнопку "Показать полностью"')
    ptk_project_page.project_description_text_part_2()
    logging.debug(
            'Show_button.Нашел вторую часть текста описания объекта')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Edge, разрешение 1280х1024. Кнопка "Показать".')
@allure.story('Успешный клик на кнопку "Показать".')
@allure.severity('Critical')
def test_show_button_edge_max_resolution(browser_edge_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('Show_button.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_edge_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('Show_button. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('Show_button. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('Show_button. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('Show_button. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'Show_button. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_project_page = SearchHelperProjectPage(browser_edge_bs_max_resolution)
    ptk_project_page.go_to_another_personal_project()
    logging.debug(
            'Show_button.Перешел на тестовый проект')
    ptk_project_page.project_description_text()
    logging.debug(
            'Show_button.Нашел первую часть текста описания объекта')
    ptk_project_page.show_button()
    logging.debug(
            'Show_button.Кликнул на кнопку "Показать полностью"')
    ptk_project_page.project_description_text_part_2()
    logging.debug(
            'Show_button.Нашел вторую часть текста описания объекта')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Safari, разрешение 1280х1024. Кнопка "Показать".')
@allure.story('Успешный клик на кнопку "Показать".')
@allure.severity('Critical')
def test_show_button_safari_max_resolution(browser_safari_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('Show_button.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_safari_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('Show_button. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('Show_button. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('Show_button. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('Show_button. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'Show_button. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_project_page = SearchHelperProjectPage(browser_safari_bs_max_resolution)
    ptk_project_page.go_to_another_personal_project()
    logging.debug(
            'Show_button.Перешел на тестовый проект')
    ptk_project_page.project_description_text()
    logging.debug(
            'Show_button.Нашел первую часть текста описания объекта')
    ptk_project_page.show_button()
    logging.debug(
            'Show_button.Кликнул на кнопку "Показать полностью"')
    ptk_project_page.project_description_text_part_2()
    logging.debug(
            'Show_button.Нашел вторую часть текста описания объекта')
