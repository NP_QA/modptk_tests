import logging

import allure
import pytest

from Login_Page import SearchHelper
from Project_Page import SearchHelperProjectPage


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Chrome. Проверка закрытия рассчетной панели.')
@allure.story('Успешное закрытие рассчетной панели.')
@allure.severity('Critical')
def test_calculation_canceled_chrome(browser_chrome_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('calculation_canceled.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_chrome_bs)
    ptk_login_page.go_to_login()
    logging.debug('calculation_canceled. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('calculation_canceled. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('calculation_canceled. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('calculation_canceled. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'calculation_canceled. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_project_page = SearchHelperProjectPage(browser_chrome_bs)
    ptk_project_page.get_project_id()
    logging.debug(
        'calculation_canceled.Сохранил идентификатор первого проекта')
    ptk_project_page.go_to_project_card()
    logging.debug(
        'calculation_canceled.Перешел в карточку первого проекта')
    ptk_project_page.assert_project_id()
    logging.debug(
        'calculation_canceled.Находясь в карточке проекта, убедился что попал именно на выбранный проект, сверив идентификатор первого проекта с идентификатором проекта на котором нахожусь')
    ptk_project_page.click_on_calculation()
    logging.debug(
        'calculation_canceled.Кликнул на "Рассчитать"')
    ptk_project_page.cancel_calculation()
    logging.debug(
        'calculation_canceled.Закрыл панель рассчетов')
    ptk_project_page.assert_calculation_canceled()
    logging.debug(
        'calculation_canceled.Убедился, что панель закрыта')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Firefox. Проверка закрытия рассчетной панели.')
@allure.story('Успешное закрытие рассчетной панели.')
@allure.severity('Critical')
def test_calculation_canceled_firefox(browser_firefox_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('calculation_canceled.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_firefox_bs)
    ptk_login_page.go_to_login()
    logging.debug('calculation_canceled. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('calculation_canceled. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('calculation_canceled. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('calculation_canceled. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'calculation_canceled. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_project_page = SearchHelperProjectPage(browser_firefox_bs)
    ptk_project_page.get_project_id()
    logging.debug(
        'calculation_canceled.Сохранил идентификатор первого проекта')
    ptk_project_page.go_to_project_card()
    logging.debug(
        'calculation_canceled.Перешел в карточку первого проекта')
    ptk_project_page.assert_project_id()
    logging.debug(
        'calculation_canceled.Находясь в карточке проекта, убедился что попал именно на выбранный проект, сверив идентификатор первого проекта с идентификатором проекта на котором нахожусь')
    ptk_project_page.click_on_calculation()
    logging.debug(
        'calculation_canceled.Кликнул на "Рассчитать"')
    ptk_project_page.cancel_calculation()
    logging.debug(
        'calculation_canceled.Закрыл панель рассчетов')
    ptk_project_page.assert_calculation_canceled()
    logging.debug(
        'calculation_canceled.Убедился, что панель закрыта')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Edge. Проверка закрытия рассчетной панели.')
@allure.story('Успешное закрытие рассчетной панели.')
@allure.severity('Critical')
def test_calculation_canceled_edge(browser_edge_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('calculation_canceled.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_edge_bs)
    ptk_login_page.go_to_login()
    logging.debug('calculation_canceled. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('calculation_canceled. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('calculation_canceled. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('calculation_canceled. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'calculation_canceled. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_project_page = SearchHelperProjectPage(browser_edge_bs)
    ptk_project_page.get_project_id()
    logging.debug(
        'calculation_canceled.Сохранил идентификатор первого проекта')
    ptk_project_page.go_to_project_card()
    logging.debug(
        'calculation_canceled.Перешел в карточку первого проекта')
    ptk_project_page.assert_project_id()
    logging.debug(
        'calculation_canceled.Находясь в карточке проекта, убедился что попал именно на выбранный проект, сверив идентификатор первого проекта с идентификатором проекта на котором нахожусь')
    ptk_project_page.click_on_calculation()
    logging.debug(
        'calculation_canceled.Кликнул на "Рассчитать"')
    ptk_project_page.cancel_calculation()
    logging.debug(
        'calculation_canceled.Закрыл панель рассчетов')
    ptk_project_page.assert_calculation_canceled()
    logging.debug(
        'calculation_canceled.Убедился, что панель закрыта')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Safari. Проверка закрытия рассчетной панели.')
@allure.story('Успешное закрытие рассчетной панели.')
@allure.severity('Critical')
def test_calculation_canceled_safari(browser_safari_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('calculation_canceled.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_safari_bs)
    ptk_login_page.go_to_login()
    logging.debug('calculation_canceled. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('calculation_canceled. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('calculation_canceled. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('calculation_canceled. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'calculation_canceled. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_project_page = SearchHelperProjectPage(browser_safari_bs)
    ptk_project_page.get_project_id()
    logging.debug(
        'calculation_canceled.Сохранил идентификатор первого проекта')
    ptk_project_page.go_to_project_card()
    logging.debug(
        'calculation_canceled.Перешел в карточку первого проекта')
    ptk_project_page.assert_project_id()
    logging.debug(
        'calculation_canceled.Находясь в карточке проекта, убедился что попал именно на выбранный проект, сверив идентификатор первого проекта с идентификатором проекта на котором нахожусь')
    ptk_project_page.click_on_calculation()
    logging.debug(
        'calculation_canceled.Кликнул на "Рассчитать"')
    ptk_project_page.cancel_calculation()
    logging.debug(
        'calculation_canceled.Закрыл панель рассчетов')
    ptk_project_page.assert_calculation_canceled()
    logging.debug(
        'calculation_canceled.Убедился, что панель закрыта')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Chrome, разрешение 1280х1024. Проверка закрытия рассчетной панели.')
@allure.story('Успешное закрытие рассчетной панели.')
@allure.severity('Critical')
def test_calculation_canceled_chrome_max_resolution(browser_chrome_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('calculation_canceled.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_chrome_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('calculation_canceled. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('calculation_canceled. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('calculation_canceled. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('calculation_canceled. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'calculation_canceled. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_project_page = SearchHelperProjectPage(browser_chrome_bs_max_resolution)
    ptk_project_page.get_project_id()
    logging.debug(
        'calculation_canceled.Сохранил идентификатор первого проекта')
    ptk_project_page.go_to_project_card()
    logging.debug(
        'calculation_canceled.Перешел в карточку первого проекта')
    ptk_project_page.assert_project_id()
    logging.debug(
        'calculation_canceled.Находясь в карточке проекта, убедился что попал именно на выбранный проект, сверив идентификатор первого проекта с идентификатором проекта на котором нахожусь')
    ptk_project_page.click_on_calculation()
    logging.debug(
        'calculation_canceled.Кликнул на "Рассчитать"')
    ptk_project_page.cancel_calculation()
    logging.debug(
        'calculation_canceled.Закрыл панель рассчетов')
    ptk_project_page.assert_calculation_canceled()
    logging.debug(
        'calculation_canceled.Убедился, что панель закрыта')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Firefox, разрешение 1280х1024. Проверка закрытия рассчетной панели.')
@allure.story('Успешное закрытие рассчетной панели.')
@allure.severity('Critical')
def test_calculation_canceled_firefox_max_resolution(browser_firefox_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('calculation_canceled.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_firefox_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('calculation_canceled. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('calculation_canceled. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('calculation_canceled. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('calculation_canceled. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'calculation_canceled. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_project_page = SearchHelperProjectPage(browser_firefox_bs_max_resolution)
    ptk_project_page.get_project_id()
    logging.debug(
        'calculation_canceled.Сохранил идентификатор первого проекта')
    ptk_project_page.go_to_project_card()
    logging.debug(
        'calculation_canceled.Перешел в карточку первого проекта')
    ptk_project_page.assert_project_id()
    logging.debug(
        'calculation_canceled.Находясь в карточке проекта, убедился что попал именно на выбранный проект, сверив идентификатор первого проекта с идентификатором проекта на котором нахожусь')
    ptk_project_page.click_on_calculation()
    logging.debug(
        'calculation_canceled.Кликнул на "Рассчитать"')
    ptk_project_page.cancel_calculation()
    logging.debug(
        'calculation_canceled.Закрыл панель рассчетов')
    ptk_project_page.assert_calculation_canceled()
    logging.debug(
        'calculation_canceled.Убедился, что панель закрыта')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Edge, разрешение 1280х1024. Проверка закрытия рассчетной панели.')
@allure.story('Успешное закрытие рассчетной панели.')
@allure.severity('Critical')
def test_calculation_canceled_edge_max_resolution(browser_edge_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('calculation_canceled.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_edge_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('calculation_canceled. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('calculation_canceled. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('calculation_canceled. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('calculation_canceled. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'calculation_canceled. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_project_page = SearchHelperProjectPage(browser_edge_bs_max_resolution)
    ptk_project_page.get_project_id()
    logging.debug(
        'calculation_canceled.Сохранил идентификатор первого проекта')
    ptk_project_page.go_to_project_card()
    logging.debug(
        'calculation_canceled.Перешел в карточку первого проекта')
    ptk_project_page.assert_project_id()
    logging.debug(
        'calculation_canceled.Находясь в карточке проекта, убедился что попал именно на выбранный проект, сверив идентификатор первого проекта с идентификатором проекта на котором нахожусь')
    ptk_project_page.click_on_calculation()
    logging.debug(
        'calculation_canceled.Кликнул на "Рассчитать"')
    ptk_project_page.cancel_calculation()
    logging.debug(
        'calculation_canceled.Закрыл панель рассчетов')
    ptk_project_page.assert_calculation_canceled()
    logging.debug(
        'calculation_canceled.Убедился, что панель закрыта')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Safari, разрешение 1280х1024. Проверка закрытия рассчетной панели.')
@allure.story('Успешное закрытие рассчетной панели.')
@allure.severity('Critical')
def test_calculation_canceled_safari_max_resolution(browser_safari_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('calculation_canceled.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_safari_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('calculation_canceled. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('calculation_canceled. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('calculation_canceled. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('calculation_canceled. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'calculation_canceled. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_project_page = SearchHelperProjectPage(browser_safari_bs_max_resolution)
    ptk_project_page.get_project_id()
    logging.debug(
        'calculation_canceled.Сохранил идентификатор первого проекта')
    ptk_project_page.go_to_project_card()
    logging.debug(
        'calculation_canceled.Перешел в карточку первого проекта')
    ptk_project_page.assert_project_id()
    logging.debug(
        'calculation_canceled.Находясь в карточке проекта, убедился что попал именно на выбранный проект, сверив идентификатор первого проекта с идентификатором проекта на котором нахожусь')
    ptk_project_page.click_on_calculation()
    logging.debug(
        'calculation_canceled.Кликнул на "Рассчитать"')
    ptk_project_page.cancel_calculation()
    logging.debug(
        'calculation_canceled.Закрыл панель рассчетов')
    ptk_project_page.assert_calculation_canceled()
    logging.debug(
        'calculation_canceled.Убедился, что панель закрыта')
