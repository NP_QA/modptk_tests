import logging

import allure
import pytest

from Login_Page import SearchHelper
from Project_Page import SearchHelperProjectPage


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Chrome. Проверка проекта по трем параметрам.')
@allure.story('Успешная проверка проекта по трем параметрам.')
@allure.severity('Critical')
def test_verify_project_by_3_parameters_chrome(browser_chrome_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('verify_project_by_3_parameters.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_chrome_bs)
    ptk_login_page.go_to_login()
    logging.debug('select_another_product_version. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('select_another_product_version. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('select_another_product_version. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('select_another_product_version. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'select_another_product_version. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_project_page = SearchHelperProjectPage(browser_chrome_bs)
    ptk_project_page.get_project_id()
    logging.debug(
        'select_another_product_version.Сохранил идентификатор первого проекта')
    ptk_project_page.go_to_project_card()
    logging.debug(
        'select_another_product_version.Перешел в карточку первого проекта')
    ptk_project_page.assert_project_id()
    logging.debug(
        'select_another_product_version.Находясь в карточке проекта, убедился что попал именно на выбранный проект, сверив идентификатор первого проекта с идентификатором проекта на котором нахожусь')
    ptk_project_page.click_three_dots_icon()
    logging.debug(
        'select_another_product_version.Кликнул на кнопку троеточия')
    ptk_project_page.project_card_click_verify_project()
    ptk_project_page.click_preparation_of_the_territory()
    ptk_project_page.click_bifurcations_with_cells()
    ptk_project_page.click_other_burifications()
    logging.debug(
        'select_another_product_version.Кликнул на кнопку проверить проект')
    ptk_project_page.project_card_click_verify_project_button()
    logging.debug(
        'select_another_product_version. Кликнул на кнопку Проект')
    ptk_project_page.assert_project_card_verified()


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Firefox. Проверка проекта по трем параметрам.')
@allure.story('Успешная проверка проекта по трем параметрам.')
@allure.severity('Critical')
def test_verify_project_by_3_parameters_firefox(browser_firefox_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('verify_project_by_3_parameters.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_firefox_bs)
    ptk_login_page.go_to_login()
    logging.debug('select_another_product_version. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('select_another_product_version. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('select_another_product_version. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('select_another_product_version. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'select_another_product_version. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_project_page = SearchHelperProjectPage(browser_firefox_bs)
    ptk_project_page.get_project_id()
    logging.debug(
        'select_another_product_version.Сохранил идентификатор первого проекта')
    ptk_project_page.go_to_project_card()
    logging.debug(
        'select_another_product_version.Перешел в карточку первого проекта')
    ptk_project_page.assert_project_id()
    logging.debug(
        'select_another_product_version.Находясь в карточке проекта, убедился что попал именно на выбранный проект, сверив идентификатор первого проекта с идентификатором проекта на котором нахожусь')
    ptk_project_page.click_three_dots_icon()
    logging.debug(
        'select_another_product_version.Кликнул на кнопку троеточия')
    ptk_project_page.project_card_click_verify_project()
    ptk_project_page.click_preparation_of_the_territory()
    ptk_project_page.click_bifurcations_with_cells()
    ptk_project_page.click_other_burifications()
    logging.debug(
        'select_another_product_version.Кликнул на кнопку проверить проект')
    ptk_project_page.project_card_click_verify_project_button()
    logging.debug(
        'select_another_product_version. Кликнул на кнопку Проект')
    ptk_project_page.assert_project_card_verified()


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Edge. Проверка проекта по трем параметрам.')
@allure.story('Успешная проверка проекта по трем параметрам.')
@allure.severity('Critical')
def test_verify_project_by_3_parameters_edge(browser_edge_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('verify_project_by_3_parameters.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_edge_bs)
    ptk_login_page.go_to_login()
    logging.debug('select_another_product_version. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('select_another_product_version. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('select_another_product_version. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('select_another_product_version. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'select_another_product_version. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_project_page = SearchHelperProjectPage(browser_edge_bs)
    ptk_project_page.get_project_id()
    logging.debug(
        'select_another_product_version.Сохранил идентификатор первого проекта')
    ptk_project_page.go_to_project_card()
    logging.debug(
        'select_another_product_version.Перешел в карточку первого проекта')
    ptk_project_page.assert_project_id()
    logging.debug(
        'select_another_product_version.Находясь в карточке проекта, убедился что попал именно на выбранный проект, сверив идентификатор первого проекта с идентификатором проекта на котором нахожусь')
    ptk_project_page.click_three_dots_icon()
    logging.debug(
        'select_another_product_version.Кликнул на кнопку троеточия')
    ptk_project_page.project_card_click_verify_project()
    ptk_project_page.click_preparation_of_the_territory()
    ptk_project_page.click_bifurcations_with_cells()
    ptk_project_page.click_other_burifications()
    logging.debug(
        'select_another_product_version.Кликнул на кнопку проверить проект')
    ptk_project_page.project_card_click_verify_project_button()
    logging.debug(
        'select_another_product_version. Кликнул на кнопку Проект')
    ptk_project_page.assert_project_card_verified()


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Safari. Проверка проекта по трем параметрам.')
@allure.story('Успешная проверка проекта по трем параметрам.')
@allure.severity('Critical')
def test_verify_project_by_3_parameters_safari(browser_safari_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('verify_project_by_3_parameters.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_safari_bs)
    ptk_login_page.go_to_login()
    logging.debug('select_another_product_version. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('select_another_product_version. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('select_another_product_version. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('select_another_product_version. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'select_another_product_version. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_project_page = SearchHelperProjectPage(browser_safari_bs)
    ptk_project_page.get_project_id()
    logging.debug(
        'select_another_product_version.Сохранил идентификатор первого проекта')
    ptk_project_page.go_to_project_card()
    logging.debug(
        'select_another_product_version.Перешел в карточку первого проекта')
    ptk_project_page.assert_project_id()
    logging.debug(
        'select_another_product_version.Находясь в карточке проекта, убедился что попал именно на выбранный проект, сверив идентификатор первого проекта с идентификатором проекта на котором нахожусь')
    ptk_project_page.click_three_dots_icon()
    logging.debug(
        'select_another_product_version.Кликнул на кнопку троеточия')
    ptk_project_page.project_card_click_verify_project()
    ptk_project_page.click_preparation_of_the_territory()
    ptk_project_page.click_bifurcations_with_cells()
    ptk_project_page.click_other_burifications()
    logging.debug(
        'select_another_product_version.Кликнул на кнопку проверить проект')
    ptk_project_page.project_card_click_verify_project_button()
    logging.debug(
        'select_another_product_version. Кликнул на кнопку Проект')
    ptk_project_page.assert_project_card_verified()


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Chrome, разрешение 1280х1024. Проверка проекта по трем параметрам.')
@allure.story('Успешная проверка проекта по трем параметрам.')
@allure.severity('Critical')
def test_verify_project_by_3_parameters_chrome_max_resolution(browser_chrome_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('verify_project_by_3_parameters.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_chrome_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('select_another_product_version. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('select_another_product_version. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('select_another_product_version. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('select_another_product_version. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'select_another_product_version. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_project_page = SearchHelperProjectPage(browser_chrome_bs_max_resolution)
    ptk_project_page.get_project_id()
    logging.debug(
        'select_another_product_version.Сохранил идентификатор первого проекта')
    ptk_project_page.go_to_project_card()
    logging.debug(
        'select_another_product_version.Перешел в карточку первого проекта')
    ptk_project_page.assert_project_id()
    logging.debug(
        'select_another_product_version.Находясь в карточке проекта, убедился что попал именно на выбранный проект, сверив идентификатор первого проекта с идентификатором проекта на котором нахожусь')
    ptk_project_page.click_three_dots_icon()
    logging.debug(
        'select_another_product_version.Кликнул на кнопку троеточия')
    ptk_project_page.project_card_click_verify_project()
    ptk_project_page.click_preparation_of_the_territory()
    ptk_project_page.click_bifurcations_with_cells()
    ptk_project_page.click_other_burifications()
    logging.debug(
        'select_another_product_version.Кликнул на кнопку проверить проект')
    ptk_project_page.project_card_click_verify_project_button()
    logging.debug(
        'select_another_product_version. Кликнул на кнопку Проект')
    ptk_project_page.assert_project_card_verified()


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Firefox, разрешение 1280х1024. Проверка проекта по трем параметрам.')
@allure.story('Успешная проверка проекта по трем параметрам.')
@allure.severity('Critical')
def test_verify_project_by_3_parameters_firefox_max_resolution(browser_firefox_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('verify_project_by_3_parameters.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_firefox_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('select_another_product_version. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('select_another_product_version. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('select_another_product_version. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('select_another_product_version. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'select_another_product_version. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_project_page = SearchHelperProjectPage(browser_firefox_bs_max_resolution)
    ptk_project_page.get_project_id()
    logging.debug(
        'select_another_product_version.Сохранил идентификатор первого проекта')
    ptk_project_page.go_to_project_card()
    logging.debug(
        'select_another_product_version.Перешел в карточку первого проекта')
    ptk_project_page.assert_project_id()
    logging.debug(
        'select_another_product_version.Находясь в карточке проекта, убедился что попал именно на выбранный проект, сверив идентификатор первого проекта с идентификатором проекта на котором нахожусь')
    ptk_project_page.click_three_dots_icon()
    logging.debug(
        'select_another_product_version.Кликнул на кнопку троеточия')
    ptk_project_page.project_card_click_verify_project()
    ptk_project_page.click_preparation_of_the_territory()
    ptk_project_page.click_bifurcations_with_cells()
    ptk_project_page.click_other_burifications()
    logging.debug(
        'select_another_product_version.Кликнул на кнопку проверить проект')
    ptk_project_page.project_card_click_verify_project_button()
    logging.debug(
        'select_another_product_version. Кликнул на кнопку Проект')
    ptk_project_page.assert_project_card_verified()


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Edge, разрешение 1280х1024. Проверка проекта по трем параметрам.')
@allure.story('Успешная проверка проекта по трем параметрам.')
@allure.severity('Critical')
def test_verify_project_by_3_parameters_edge_max_resolution(browser_edge_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('verify_project_by_3_parameters.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_edge_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('select_another_product_version. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('select_another_product_version. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('select_another_product_version. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('select_another_product_version. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'select_another_product_version. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_project_page = SearchHelperProjectPage(browser_edge_bs_max_resolution)
    ptk_project_page.get_project_id()
    logging.debug(
        'select_another_product_version.Сохранил идентификатор первого проекта')
    ptk_project_page.go_to_project_card()
    logging.debug(
        'select_another_product_version.Перешел в карточку первого проекта')
    ptk_project_page.assert_project_id()
    logging.debug(
        'select_another_product_version.Находясь в карточке проекта, убедился что попал именно на выбранный проект, сверив идентификатор первого проекта с идентификатором проекта на котором нахожусь')
    ptk_project_page.click_three_dots_icon()
    logging.debug(
        'select_another_product_version.Кликнул на кнопку троеточия')
    ptk_project_page.project_card_click_verify_project()
    ptk_project_page.click_preparation_of_the_territory()
    ptk_project_page.click_bifurcations_with_cells()
    ptk_project_page.click_other_burifications()
    logging.debug(
        'select_another_product_version.Кликнул на кнопку проверить проект')
    ptk_project_page.project_card_click_verify_project_button()
    logging.debug(
        'select_another_product_version. Кликнул на кнопку Проект')
    ptk_project_page.assert_project_card_verified()


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Safari, разрешение 1280х1024. Проверка проекта по трем параметрам.')
@allure.story('Успешная проверка проекта по трем параметрам.')
@allure.severity('Critical')
def test_verify_project_by_3_parameters_safari_max_resolution(browser_safari_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('verify_project_by_3_parameters.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_safari_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('select_another_product_version. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('select_another_product_version. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('select_another_product_version. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('select_another_product_version. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'select_another_product_version. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_project_page = SearchHelperProjectPage(browser_safari_bs_max_resolution)
    ptk_project_page.get_project_id()
    logging.debug(
        'select_another_product_version.Сохранил идентификатор первого проекта')
    ptk_project_page.go_to_project_card()
    logging.debug(
        'select_another_product_version.Перешел в карточку первого проекта')
    ptk_project_page.assert_project_id()
    logging.debug(
        'select_another_product_version.Находясь в карточке проекта, убедился что попал именно на выбранный проект, сверив идентификатор первого проекта с идентификатором проекта на котором нахожусь')
    ptk_project_page.click_three_dots_icon()
    logging.debug(
        'select_another_product_version.Кликнул на кнопку троеточия')
    ptk_project_page.project_card_click_verify_project()
    ptk_project_page.click_preparation_of_the_territory()
    ptk_project_page.click_bifurcations_with_cells()
    ptk_project_page.click_other_burifications()
    logging.debug(
        'select_another_product_version.Кликнул на кнопку проверить проект')
    ptk_project_page.project_card_click_verify_project_button()
    logging.debug(
        'select_another_product_version. Кликнул на кнопку Проект')
    ptk_project_page.assert_project_card_verified()
