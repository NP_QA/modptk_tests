import logging

import allure
import pytest

from Login_Page import SearchHelper
from Project_Page import SearchHelperProjectPage


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Chrome. Выбор двух регионов для проекта.')
@allure.story('Успешное добавление нескольких регионов для проекта.')
@allure.severity('Critical')
def test_select_2_regions_chrome(browser_chrome_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('select_2_regions.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_chrome_bs)
    ptk_login_page.go_to_login()
    logging.debug('select_region. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('select_region. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('select_region. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('select_region. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    ptk_project_page = SearchHelperProjectPage(browser_chrome_bs)
    ptk_project_page.go_to_personal_project()
    logging.debug(
        'select_region. Перешел в персональный проект')
    ptk_project_page.project_edit_icon()
    logging.debug(
        'select_region. Кликнул на кнопку редактирования проекта')
    ptk_project_page.project_click_region_selector()
    logging.debug(
        'select_region. Кликнул на селектор региона')
    ptk_project_page.project_card_select_altay_region()
    logging.debug(
        'select_region.Кликнул на чек-бокс Алтайского региона')
    ptk_project_page.project_card_select_amur_region()
    ptk_project_page.project_card_click_on_body()
    logging.debug(
        'select_region.Кликнул на тело страницы')
    ptk_project_page.project_card_save_button()
    logging.debug(
        'select_region.Кликнул на кнопку сохранения')
    ptk_project_page.project_card_assert_altay_selected()
    logging.debug(
        'select_region.Убедился, что Алтай добавлен в список')
    ptk_project_page.project_card_assert_amur_selected()
    logging.debug(
        'select_region.Убедился, что Амур добавлен в список')
    ptk_project_page.project_edit_icon()
    logging.debug(
        'select_region. Кликнул на кнопку редактирования проекта')
    ptk_project_page.project_click_region_selector()
    logging.debug(
        'select_region. Кликнул на селектор региона')
    ptk_project_page.project_card_select_altay_region()
    logging.debug(
        'select_region.Кликнул на чек-бокс Алтайского региона')
    ptk_project_page.project_card_select_amur_region()
    logging.debug(
        'select_region.Кликнул на чек-бокс Амурского региона')
    ptk_project_page.project_card_click_on_body()
    ptk_project_page.project_card_save_button()
    logging.debug(
        'select_region.Кликнул на кнопку сохранения')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Firefox. Выбор двух регионов для проекта.')
@allure.story('Успешное добавление нескольких регионов для проекта.')
@allure.severity('Critical')
def test_select_2_regions_firefox(browser_firefox_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('select_2_regions.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_firefox_bs)
    ptk_login_page.go_to_login()
    logging.debug('select_region. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('select_region. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('select_region. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('select_region. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    ptk_project_page = SearchHelperProjectPage(browser_firefox_bs)
    ptk_project_page.go_to_personal_project()
    logging.debug(
        'select_region. Перешел в персональный проект')
    ptk_project_page.project_edit_icon()
    logging.debug(
        'select_region. Кликнул на кнопку редактирования проекта')
    ptk_project_page.project_click_region_selector()
    logging.debug(
        'select_region. Кликнул на селектор региона')
    ptk_project_page.project_card_select_altay_region()
    logging.debug(
        'select_region.Кликнул на чек-бокс Алтайского региона')
    ptk_project_page.project_card_select_amur_region()
    ptk_project_page.project_card_click_on_body()
    logging.debug(
        'select_region.Кликнул на тело страницы')
    ptk_project_page.project_card_save_button()
    logging.debug(
        'select_region.Кликнул на кнопку сохранения')
    ptk_project_page.project_card_assert_altay_selected()
    logging.debug(
        'select_region.Убедился, что Алтай добавлен в список')
    ptk_project_page.project_card_assert_amur_selected()
    logging.debug(
        'select_region.Убедился, что Амур добавлен в список')
    ptk_project_page.project_edit_icon()
    logging.debug(
        'select_region. Кликнул на кнопку редактирования проекта')
    ptk_project_page.project_click_region_selector()
    logging.debug(
        'select_region. Кликнул на селектор региона')
    ptk_project_page.project_card_select_altay_region()
    logging.debug(
        'select_region.Кликнул на чек-бокс Алтайского региона')
    ptk_project_page.project_card_select_amur_region()
    logging.debug(
        'select_region.Кликнул на чек-бокс Амурского региона')
    ptk_project_page.project_card_click_on_body()
    ptk_project_page.project_card_save_button()
    logging.debug(
        'select_region.Кликнул на кнопку сохранения')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Edge. Выбор двух регионов для проекта.')
@allure.story('Успешное добавление нескольких регионов для проекта.')
@allure.severity('Critical')
def test_select_2_regions_edge(browser_edge_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('select_2_regions.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_edge_bs)
    ptk_login_page.go_to_login()
    logging.debug('select_region. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('select_region. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('select_region. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('select_region. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    ptk_project_page = SearchHelperProjectPage(browser_edge_bs)
    ptk_project_page.go_to_personal_project()
    logging.debug(
        'select_region. Перешел в персональный проект')
    ptk_project_page.project_edit_icon()
    logging.debug(
        'select_region. Кликнул на кнопку редактирования проекта')
    ptk_project_page.project_click_region_selector()
    logging.debug(
        'select_region. Кликнул на селектор региона')
    ptk_project_page.project_card_select_altay_region()
    logging.debug(
        'select_region.Кликнул на чек-бокс Алтайского региона')
    ptk_project_page.project_card_select_amur_region()
    ptk_project_page.project_card_click_on_body()
    logging.debug(
        'select_region.Кликнул на тело страницы')
    ptk_project_page.project_card_save_button()
    logging.debug(
        'select_region.Кликнул на кнопку сохранения')
    ptk_project_page.project_card_assert_altay_selected()
    logging.debug(
        'select_region.Убедился, что Алтай добавлен в список')
    ptk_project_page.project_card_assert_amur_selected()
    logging.debug(
        'select_region.Убедился, что Амур добавлен в список')
    ptk_project_page.project_edit_icon()
    logging.debug(
        'select_region. Кликнул на кнопку редактирования проекта')
    ptk_project_page.project_click_region_selector()
    logging.debug(
        'select_region. Кликнул на селектор региона')
    ptk_project_page.project_card_select_altay_region()
    logging.debug(
        'select_region.Кликнул на чек-бокс Алтайского региона')
    ptk_project_page.project_card_select_amur_region()
    logging.debug(
        'select_region.Кликнул на чек-бокс Амурского региона')
    ptk_project_page.project_card_click_on_body()
    ptk_project_page.project_card_save_button()
    logging.debug(
        'select_region.Кликнул на кнопку сохранения')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Safari. Выбор двух регионов для проекта.')
@allure.story('Успешное добавление нескольких регионов для проекта.')
@allure.severity('Critical')
def test_select_2_regions_safari(browser_safari_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('select_2_regions.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_safari_bs)
    ptk_login_page.go_to_login()
    logging.debug('select_region. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('select_region. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('select_region. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('select_region. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    ptk_project_page = SearchHelperProjectPage(browser_safari_bs)
    ptk_project_page.go_to_personal_project()
    logging.debug(
        'select_region. Перешел в персональный проект')
    ptk_project_page.project_edit_icon()
    logging.debug(
        'select_region. Кликнул на кнопку редактирования проекта')
    ptk_project_page.project_click_region_selector()
    logging.debug(
        'select_region. Кликнул на селектор региона')
    ptk_project_page.project_card_select_altay_region()
    logging.debug(
        'select_region.Кликнул на чек-бокс Алтайского региона')
    ptk_project_page.project_card_select_amur_region()
    ptk_project_page.project_card_click_on_body()
    logging.debug(
        'select_region.Кликнул на тело страницы')
    ptk_project_page.project_card_save_button()
    logging.debug(
        'select_region.Кликнул на кнопку сохранения')
    ptk_project_page.project_card_assert_altay_selected()
    logging.debug(
        'select_region.Убедился, что Алтай добавлен в список')
    ptk_project_page.project_card_assert_amur_selected()
    logging.debug(
        'select_region.Убедился, что Амур добавлен в список')
    ptk_project_page.project_edit_icon()
    logging.debug(
        'select_region. Кликнул на кнопку редактирования проекта')
    ptk_project_page.project_click_region_selector()
    logging.debug(
        'select_region. Кликнул на селектор региона')
    ptk_project_page.project_card_select_altay_region()
    logging.debug(
        'select_region.Кликнул на чек-бокс Алтайского региона')
    ptk_project_page.project_card_select_amur_region()
    logging.debug(
        'select_region.Кликнул на чек-бокс Амурского региона')
    ptk_project_page.project_card_click_on_body()
    ptk_project_page.project_card_save_button()
    logging.debug(
        'select_region.Кликнул на кнопку сохранения')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Chrome, разрешение 1280х1024. Выбор двух регионов для проекта.')
@allure.story('Успешное добавление нескольких регионов для проекта.')
@allure.severity('Critical')
def test_select_2_regions_chrome_max_resolution(browser_chrome_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('select_2_regions.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_chrome_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('select_region. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('select_region. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('select_region. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('select_region. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    ptk_project_page = SearchHelperProjectPage(browser_chrome_bs_max_resolution)
    ptk_project_page.go_to_personal_project()
    logging.debug(
        'select_region. Перешел в персональный проект')
    ptk_project_page.project_edit_icon()
    logging.debug(
        'select_region. Кликнул на кнопку редактирования проекта')
    ptk_project_page.project_click_region_selector()
    logging.debug(
        'select_region. Кликнул на селектор региона')
    ptk_project_page.project_card_select_altay_region()
    logging.debug(
        'select_region.Кликнул на чек-бокс Алтайского региона')
    ptk_project_page.project_card_select_amur_region()
    ptk_project_page.project_card_click_on_body()
    logging.debug(
        'select_region.Кликнул на тело страницы')
    ptk_project_page.project_card_save_button()
    logging.debug(
        'select_region.Кликнул на кнопку сохранения')
    ptk_project_page.project_card_assert_altay_selected()
    logging.debug(
        'select_region.Убедился, что Алтай добавлен в список')
    ptk_project_page.project_card_assert_amur_selected()
    logging.debug(
        'select_region.Убедился, что Амур добавлен в список')
    ptk_project_page.project_edit_icon()
    logging.debug(
        'select_region. Кликнул на кнопку редактирования проекта')
    ptk_project_page.project_click_region_selector()
    logging.debug(
        'select_region. Кликнул на селектор региона')
    ptk_project_page.project_card_select_altay_region()
    logging.debug(
        'select_region.Кликнул на чек-бокс Алтайского региона')
    ptk_project_page.project_card_select_amur_region()
    logging.debug(
        'select_region.Кликнул на чек-бокс Амурского региона')
    ptk_project_page.project_card_click_on_body()
    ptk_project_page.project_card_save_button()
    logging.debug(
        'select_region.Кликнул на кнопку сохранения')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Firefox, разрешение 1280х1024. Выбор двух регионов для проекта.')
@allure.story('Успешное добавление нескольких регионов для проекта.')
@allure.severity('Critical')
def test_select_2_regions_firefox_max_resolution(browser_firefox_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('select_2_regions.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_firefox_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('select_region. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('select_region. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('select_region. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('select_region. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    ptk_project_page = SearchHelperProjectPage(browser_firefox_bs_max_resolution)
    ptk_project_page.go_to_personal_project()
    logging.debug(
        'select_region. Перешел в персональный проект')
    ptk_project_page.project_edit_icon()
    logging.debug(
        'select_region. Кликнул на кнопку редактирования проекта')
    ptk_project_page.project_click_region_selector()
    logging.debug(
        'select_region. Кликнул на селектор региона')
    ptk_project_page.project_card_select_altay_region()
    logging.debug(
        'select_region.Кликнул на чек-бокс Алтайского региона')
    ptk_project_page.project_card_select_amur_region()
    ptk_project_page.project_card_click_on_body()
    logging.debug(
        'select_region.Кликнул на тело страницы')
    ptk_project_page.project_card_save_button()
    logging.debug(
        'select_region.Кликнул на кнопку сохранения')
    ptk_project_page.project_card_assert_altay_selected()
    logging.debug(
        'select_region.Убедился, что Алтай добавлен в список')
    ptk_project_page.project_card_assert_amur_selected()
    logging.debug(
        'select_region.Убедился, что Амур добавлен в список')
    ptk_project_page.project_edit_icon()
    logging.debug(
        'select_region. Кликнул на кнопку редактирования проекта')
    ptk_project_page.project_click_region_selector()
    logging.debug(
        'select_region. Кликнул на селектор региона')
    ptk_project_page.project_card_select_altay_region()
    logging.debug(
        'select_region.Кликнул на чек-бокс Алтайского региона')
    ptk_project_page.project_card_select_amur_region()
    logging.debug(
        'select_region.Кликнул на чек-бокс Амурского региона')
    ptk_project_page.project_card_click_on_body()
    ptk_project_page.project_card_save_button()
    logging.debug(
        'select_region.Кликнул на кнопку сохранения')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Edge, разрешение 1280х1024. Выбор двух регионов для проекта.')
@allure.story('Успешное добавление нескольких регионов для проекта.')
@allure.severity('Critical')
def test_select_2_regions_edge_max_resolution(browser_edge_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('select_2_regions.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_edge_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('select_region. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('select_region. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('select_region. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('select_region. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    ptk_project_page = SearchHelperProjectPage(browser_edge_bs_max_resolution)
    ptk_project_page.go_to_personal_project()
    logging.debug(
        'select_region. Перешел в персональный проект')
    ptk_project_page.project_edit_icon()
    logging.debug(
        'select_region. Кликнул на кнопку редактирования проекта')
    ptk_project_page.project_click_region_selector()
    logging.debug(
        'select_region. Кликнул на селектор региона')
    ptk_project_page.project_card_select_altay_region()
    logging.debug(
        'select_region.Кликнул на чек-бокс Алтайского региона')
    ptk_project_page.project_card_select_amur_region()
    ptk_project_page.project_card_click_on_body()
    logging.debug(
        'select_region.Кликнул на тело страницы')
    ptk_project_page.project_card_save_button()
    logging.debug(
        'select_region.Кликнул на кнопку сохранения')
    ptk_project_page.project_card_assert_altay_selected()
    logging.debug(
        'select_region.Убедился, что Алтай добавлен в список')
    ptk_project_page.project_card_assert_amur_selected()
    logging.debug(
        'select_region.Убедился, что Амур добавлен в список')
    ptk_project_page.project_edit_icon()
    logging.debug(
        'select_region. Кликнул на кнопку редактирования проекта')
    ptk_project_page.project_click_region_selector()
    logging.debug(
        'select_region. Кликнул на селектор региона')
    ptk_project_page.project_card_select_altay_region()
    logging.debug(
        'select_region.Кликнул на чек-бокс Алтайского региона')
    ptk_project_page.project_card_select_amur_region()
    logging.debug(
        'select_region.Кликнул на чек-бокс Амурского региона')
    ptk_project_page.project_card_click_on_body()
    ptk_project_page.project_card_save_button()
    logging.debug(
        'select_region.Кликнул на кнопку сохранения')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Safari, разрешение 1280х1024. Выбор двух регионов для проекта.')
@allure.story('Успешное добавление нескольких регионов для проекта.')
@allure.severity('Critical')
def test_select_2_regions_safari_max_resolution(browser_safari_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('select_2_regions.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_safari_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('select_region. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('select_region. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('select_region. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('select_region. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    ptk_project_page = SearchHelperProjectPage(browser_safari_bs_max_resolution)
    ptk_project_page.go_to_personal_project()
    logging.debug(
        'select_region. Перешел в персональный проект')
    ptk_project_page.project_edit_icon()
    logging.debug(
        'select_region. Кликнул на кнопку редактирования проекта')
    ptk_project_page.project_click_region_selector()
    logging.debug(
        'select_region. Кликнул на селектор региона')
    ptk_project_page.project_card_select_altay_region()
    logging.debug(
        'select_region.Кликнул на чек-бокс Алтайского региона')
    ptk_project_page.project_card_select_amur_region()
    ptk_project_page.project_card_click_on_body()
    logging.debug(
        'select_region.Кликнул на тело страницы')
    ptk_project_page.project_card_save_button()
    logging.debug(
        'select_region.Кликнул на кнопку сохранения')
    ptk_project_page.project_card_assert_altay_selected()
    logging.debug(
        'select_region.Убедился, что Алтай добавлен в список')
    ptk_project_page.project_card_assert_amur_selected()
    logging.debug(
        'select_region.Убедился, что Амур добавлен в список')
    ptk_project_page.project_edit_icon()
    logging.debug(
        'select_region. Кликнул на кнопку редактирования проекта')
    ptk_project_page.project_click_region_selector()
    logging.debug(
        'select_region. Кликнул на селектор региона')
    ptk_project_page.project_card_select_altay_region()
    logging.debug(
        'select_region.Кликнул на чек-бокс Алтайского региона')
    ptk_project_page.project_card_select_amur_region()
    logging.debug(
        'select_region.Кликнул на чек-бокс Амурского региона')
    ptk_project_page.project_card_click_on_body()
    ptk_project_page.project_card_save_button()
    logging.debug(
        'select_region.Кликнул на кнопку сохранения')
