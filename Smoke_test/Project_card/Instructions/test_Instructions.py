import logging

import allure
import pytest

from Login_Page import SearchHelper
from Project_Page import SearchHelperProjectPage


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Chrome. Проверка открытия инструкции в проектной карточке.')
@allure.story('Успешное открытие инструкции в проектной карточке.')
@allure.severity('Critical')
def test_instructions_chrome(browser_chrome_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('instructions.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_chrome_bs)
    ptk_login_page.go_to_login()
    logging.debug('instructions. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('instructions. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('instructions. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('instructions. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    ptk_project_page = SearchHelperProjectPage(browser_chrome_bs)
    ptk_project_page.go_to_test_calc()
    logging.debug(
        'instructions.Перешел в проект')
    ptk_project_page.click_on_calculation()
    logging.debug(
        'instructions.Кликнул на "Рассчитать"')
    ptk_project_page.project_card_click_on_plus()
    logging.debug(
        'instructions.Кликнул на "Плюс"')
    ptk_project_page.project_card_select_table_1()
    logging.debug(
        'instructions.Кликнул на "Таблицу 1"')
    ptk_project_page.project_card_select_b_1()
    logging.debug(
        'instructions.Кликнул на "Б1"')
    ptk_project_page.project_card_click_on_instructions()
    logging.debug(
        'instructions.Нашел инструкцию')
    ptk_project_page.project_card_find_instructions()


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Firefox. Проверка открытия инструкции в проектной карточке.')
@allure.story('Успешное открытие инструкции в проектной карточке.')
@allure.severity('Critical')
def test_instructions_firefox(browser_firefox_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('instructions.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_firefox_bs)
    ptk_login_page.go_to_login()
    logging.debug('instructions. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('instructions. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('instructions. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('instructions. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    ptk_project_page = SearchHelperProjectPage(browser_firefox_bs)
    ptk_project_page.go_to_test_calc()
    logging.debug(
        'instructions.Перешел в проект')
    ptk_project_page.click_on_calculation()
    logging.debug(
        'instructions.Кликнул на "Рассчитать"')
    ptk_project_page.project_card_click_on_plus()
    logging.debug(
        'instructions.Кликнул на "Плюс"')
    ptk_project_page.project_card_select_table_1()
    logging.debug(
        'instructions.Кликнул на "Таблицу 1"')
    ptk_project_page.project_card_select_b_1()
    logging.debug(
        'instructions.Кликнул на "Б1"')
    ptk_project_page.project_card_click_on_instructions()
    logging.debug(
        'instructions.Нашел инструкцию')
    ptk_project_page.project_card_find_instructions()


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Edge. Проверка открытия инструкции в проектной карточке.')
@allure.story('Успешное открытие инструкции в проектной карточке.')
@allure.severity('Critical')
def test_instructions_edge(browser_edge_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('instructions.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_edge_bs)
    ptk_login_page.go_to_login()
    logging.debug('instructions. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('instructions. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('instructions. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('instructions. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    ptk_project_page = SearchHelperProjectPage(browser_edge_bs)
    ptk_project_page.go_to_test_calc()
    logging.debug(
        'instructions.Перешел в проект')
    ptk_project_page.click_on_calculation()
    logging.debug(
        'instructions.Кликнул на "Рассчитать"')
    ptk_project_page.project_card_click_on_plus()
    logging.debug(
        'instructions.Кликнул на "Плюс"')
    ptk_project_page.project_card_select_table_1()
    logging.debug(
        'instructions.Кликнул на "Таблицу 1"')
    ptk_project_page.project_card_select_b_1()
    logging.debug(
        'instructions.Кликнул на "Б1"')
    ptk_project_page.project_card_click_on_instructions()
    logging.debug(
        'instructions.Нашел инструкцию')
    ptk_project_page.project_card_find_instructions()


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Safari. Проверка открытия инструкции в проектной карточке.')
@allure.story('Успешное открытие инструкции в проектной карточке.')
@allure.severity('Critical')
def test_instructions_safari(browser_safari_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('instructions.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_safari_bs)
    ptk_login_page.go_to_login()
    logging.debug('instructions. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('instructions. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('instructions. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('instructions. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    ptk_project_page = SearchHelperProjectPage(browser_safari_bs)
    ptk_project_page.go_to_test_calc()
    logging.debug(
        'instructions.Перешел в проект')
    ptk_project_page.click_on_calculation()
    logging.debug(
        'instructions.Кликнул на "Рассчитать"')
    ptk_project_page.project_card_click_on_plus()
    logging.debug(
        'instructions.Кликнул на "Плюс"')
    ptk_project_page.project_card_select_table_1()
    logging.debug(
        'instructions.Кликнул на "Таблицу 1"')
    ptk_project_page.project_card_select_b_1()
    logging.debug(
        'instructions.Кликнул на "Б1"')
    ptk_project_page.project_card_click_on_instructions()
    logging.debug(
        'instructions.Нашел инструкцию')
    ptk_project_page.project_card_find_instructions()


@pytest.mark.smoke_test_modptk
@allure.feature(
    'Smoke тестирование на Chrome, разрешение 1280х1024. Проверка открытия инструкции в проектной карточке.')
@allure.story('Успешное открытие инструкции в проектной карточке.')
@allure.severity('Critical')
def test_instructions_chrome_max_resolution(browser_chrome_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('instructions.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_chrome_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('instructions. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('instructions. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('instructions. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('instructions. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    ptk_project_page = SearchHelperProjectPage(browser_chrome_bs_max_resolution)
    ptk_project_page.go_to_test_calc()
    logging.debug(
        'instructions.Перешел в проект')
    ptk_project_page.click_on_calculation()
    logging.debug(
        'instructions.Кликнул на "Рассчитать"')
    ptk_project_page.project_card_click_on_plus()
    logging.debug(
        'instructions.Кликнул на "Плюс"')
    ptk_project_page.project_card_select_table_1()
    logging.debug(
        'instructions.Кликнул на "Таблицу 1"')
    ptk_project_page.project_card_select_b_1()
    logging.debug(
        'instructions.Кликнул на "Б1"')
    ptk_project_page.project_card_click_on_instructions()
    logging.debug(
        'instructions.Нашел инструкцию')
    ptk_project_page.project_card_find_instructions()


@pytest.mark.smoke_test_modptk
@allure.feature(
    'Smoke тестирование на Firefox, разрешение 1280х1024. Проверка открытия инструкции в проектной карточке.')
@allure.story('Успешное открытие инструкции в проектной карточке.')
@allure.severity('Critical')
def test_instructions_firefox_max_resolution(browser_firefox_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('instructions.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_firefox_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('instructions. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('instructions. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('instructions. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('instructions. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    ptk_project_page = SearchHelperProjectPage(browser_firefox_bs_max_resolution)
    ptk_project_page.go_to_test_calc()
    logging.debug(
        'instructions.Перешел в проект')
    ptk_project_page.click_on_calculation()
    logging.debug(
        'instructions.Кликнул на "Рассчитать"')
    ptk_project_page.project_card_click_on_plus()
    logging.debug(
        'instructions.Кликнул на "Плюс"')
    ptk_project_page.project_card_select_table_1()
    logging.debug(
        'instructions.Кликнул на "Таблицу 1"')
    ptk_project_page.project_card_select_b_1()
    logging.debug(
        'instructions.Кликнул на "Б1"')
    ptk_project_page.project_card_click_on_instructions()
    logging.debug(
        'instructions.Нашел инструкцию')
    ptk_project_page.project_card_find_instructions()


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Edge, разрешение 1280х1024. Проверка открытия инструкции в проектной карточке.')
@allure.story('Успешное открытие инструкции в проектной карточке.')
@allure.severity('Critical')
def test_instructions_edge_max_resolution(browser_edge_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('instructions.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_edge_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('instructions. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('instructions. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('instructions. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('instructions. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    ptk_project_page = SearchHelperProjectPage(browser_edge_bs_max_resolution)
    ptk_project_page.go_to_test_calc()
    logging.debug(
        'instructions.Перешел в проект')
    ptk_project_page.click_on_calculation()
    logging.debug(
        'instructions.Кликнул на "Рассчитать"')
    ptk_project_page.project_card_click_on_plus()
    logging.debug(
        'instructions.Кликнул на "Плюс"')
    ptk_project_page.project_card_select_table_1()
    logging.debug(
        'instructions.Кликнул на "Таблицу 1"')
    ptk_project_page.project_card_select_b_1()
    logging.debug(
        'instructions.Кликнул на "Б1"')
    ptk_project_page.project_card_click_on_instructions()
    logging.debug(
        'instructions.Нашел инструкцию')
    ptk_project_page.project_card_find_instructions()


@pytest.mark.smoke_test_modptk
@allure.feature(
    'Smoke тестирование на Safari, разрешение 1280х1024. Проверка открытия инструкции в проектной карточке.')
@allure.story('Успешное открытие инструкции в проектной карточке.')
@allure.severity('Critical')
def test_instructions_safari_max_resolution(browser_safari_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('instructions.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_safari_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('instructions. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('instructions. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('instructions. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('instructions. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    ptk_project_page = SearchHelperProjectPage(browser_safari_bs_max_resolution)
    ptk_project_page.go_to_test_calc()
    logging.debug(
        'instructions.Перешел в проект')
    ptk_project_page.click_on_calculation()
    logging.debug(
        'instructions.Кликнул на "Рассчитать"')
    ptk_project_page.project_card_click_on_plus()
    logging.debug(
        'instructions.Кликнул на "Плюс"')
    ptk_project_page.project_card_select_table_1()
    logging.debug(
        'instructions.Кликнул на "Таблицу 1"')
    ptk_project_page.project_card_select_b_1()
    logging.debug(
        'instructions.Кликнул на "Б1"')
    ptk_project_page.project_card_click_on_instructions()
    logging.debug(
        'instructions.Нашел инструкцию')
    ptk_project_page.project_card_find_instructions()
