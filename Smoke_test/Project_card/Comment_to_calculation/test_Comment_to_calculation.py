import logging

import allure
import pytest

from Login_Page import SearchHelper
from Project_Page import SearchHelperProjectPage


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Chrome. Комментарий к расчету.')
@allure.story('Успешный комментарий к расчету.')
@allure.severity('Critical')
def test_comment_to_calculation_chrome(browser_chrome_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('calculation_comment.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_chrome_bs)
    ptk_login_page.go_to_login()
    logging.debug('select_another_product_version. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('select_another_product_version. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('select_another_product_version. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('select_another_product_version. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'select_another_product_version. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_project_page = SearchHelperProjectPage(browser_chrome_bs)
    ptk_project_page.go_to_test_calc()
    logging.debug(
        'rascenka_unc_table_1.Перешел на проект для тестирования расчетов')
    ptk_project_page.click_on_calculation()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Рассчитать"')
    ptk_project_page.project_card_click_on_plus()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Плюс"')
    ptk_project_page.project_card_select_table_1()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "таблицу 1"')
    ptk_project_page.project_card_select_b_1()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Б1"')
    ptk_project_page.project_card_select_product()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на продукт')
    ptk_project_page.project_card_fill_amount()
    logging.debug(
        'rascenka_unc_table_1.Заполнил количество продукта')
    ptk_project_page.save_unc_button()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Сохранить"')
    ptk_project_page.comment_calculation()
    ptk_project_page.project_card_assert_value_amount()
    logging.debug(
        'rascenka_unc_table_1.Убедился что количество продукта соответствует введенному')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Firefox. Комментарий к расчету.')
@allure.story('Успешный комментарий к расчету.')
@allure.severity('Critical')
def test_comment_to_calculation_firefox(browser_firefox_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('calculation_comment.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_firefox_bs)
    ptk_login_page.go_to_login()
    logging.debug('select_another_product_version. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('select_another_product_version. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('select_another_product_version. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('select_another_product_version. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'select_another_product_version. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_project_page = SearchHelperProjectPage(browser_firefox_bs)
    ptk_project_page.go_to_test_calc()
    logging.debug(
        'rascenka_unc_table_1.Перешел на проект для тестирования расчетов')
    ptk_project_page.click_on_calculation()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Рассчитать"')
    ptk_project_page.project_card_click_on_plus()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Плюс"')
    ptk_project_page.project_card_select_table_1()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "таблицу 1"')
    ptk_project_page.project_card_select_b_1()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Б1"')
    ptk_project_page.project_card_select_product()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на продукт')
    ptk_project_page.project_card_fill_amount()
    logging.debug(
        'rascenka_unc_table_1.Заполнил количество продукта')
    ptk_project_page.save_unc_button()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Сохранить"')
    ptk_project_page.comment_calculation()
    ptk_project_page.project_card_assert_value_amount()
    logging.debug(
        'rascenka_unc_table_1.Убедился что количество продукта соответствует введенному')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Edge. Комментарий к расчету.')
@allure.story('Успешный комментарий к расчету.')
@allure.severity('Critical')
def test_comment_to_calculation_2_edge(browser_edge_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('calculation_comment.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_edge_bs)
    ptk_login_page.go_to_login()
    logging.debug('select_another_product_version. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('select_another_product_version. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('select_another_product_version. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('select_another_product_version. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'select_another_product_version. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_project_page = SearchHelperProjectPage(browser_edge_bs)
    ptk_project_page.go_to_test_calc()
    logging.debug(
        'rascenka_unc_table_1.Перешел на проект для тестирования расчетов')
    ptk_project_page.click_on_calculation()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Рассчитать"')
    ptk_project_page.project_card_click_on_plus()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Плюс"')
    ptk_project_page.project_card_select_table_1()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "таблицу 1"')
    ptk_project_page.project_card_select_b_1()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Б1"')
    ptk_project_page.project_card_select_product()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на продукт')
    ptk_project_page.project_card_fill_amount()
    logging.debug(
        'rascenka_unc_table_1.Заполнил количество продукта')
    ptk_project_page.save_unc_button()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Сохранить"')
    ptk_project_page.comment_calculation()
    ptk_project_page.project_card_assert_value_amount()
    logging.debug(
        'rascenka_unc_table_1.Убедился что количество продукта соответствует введенному')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Safari. Комментарий к расчету.')
@allure.story('Успешный комментарий к расчету.')
@allure.severity('Critical')
def test_comment_to_calculation_safari(browser_safari_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('calculation_comment.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_safari_bs)
    ptk_login_page.go_to_login()
    logging.debug('select_another_product_version. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('select_another_product_version. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('select_another_product_version. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('select_another_product_version. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'select_another_product_version. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_project_page = SearchHelperProjectPage(browser_safari_bs)
    ptk_project_page.go_to_test_calc()
    logging.debug(
        'rascenka_unc_table_1.Перешел на проект для тестирования расчетов')
    ptk_project_page.click_on_calculation()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Рассчитать"')
    ptk_project_page.project_card_click_on_plus()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Плюс"')
    ptk_project_page.project_card_select_table_1()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "таблицу 1"')
    ptk_project_page.project_card_select_b_1()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Б1"')
    ptk_project_page.project_card_select_product()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на продукт')
    ptk_project_page.project_card_fill_amount()
    logging.debug(
        'rascenka_unc_table_1.Заполнил количество продукта')
    ptk_project_page.save_unc_button()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Сохранить"')
    ptk_project_page.comment_calculation()
    ptk_project_page.project_card_assert_value_amount()
    logging.debug(
        'rascenka_unc_table_1.Убедился что количество продукта соответствует введенному')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Chrome, разрешение 1280х1024. Комментарий к расчету.')
@allure.story('Успешный комментарий к расчету.')
@allure.severity('Critical')
def test_comment_to_calculation_chrome_max_resolution(browser_chrome_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('calculation_comment.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_chrome_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('select_another_product_version. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('select_another_product_version. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('select_another_product_version. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('select_another_product_version. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'select_another_product_version. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_project_page = SearchHelperProjectPage(browser_chrome_bs_max_resolution)
    ptk_project_page.go_to_test_calc()
    logging.debug(
        'rascenka_unc_table_1.Перешел на проект для тестирования расчетов')
    ptk_project_page.click_on_calculation()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Рассчитать"')
    ptk_project_page.project_card_click_on_plus()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Плюс"')
    ptk_project_page.project_card_select_table_1()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "таблицу 1"')
    ptk_project_page.project_card_select_b_1()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Б1"')
    ptk_project_page.project_card_select_product()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на продукт')
    ptk_project_page.project_card_fill_amount()
    logging.debug(
        'rascenka_unc_table_1.Заполнил количество продукта')
    ptk_project_page.save_unc_button()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Сохранить"')
    ptk_project_page.comment_calculation()
    ptk_project_page.project_card_assert_value_amount()
    logging.debug(
        'rascenka_unc_table_1.Убедился что количество продукта соответствует введенному')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Firefox, разрешение 1280х1024. Комментарий к расчету.')
@allure.story('Успешный комментарий к расчету.')
@allure.severity('Critical')
def test_comment_to_calculation_firefox_max_resolution(browser_firefox_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('calculation_comment.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_firefox_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('select_another_product_version. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('select_another_product_version. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('select_another_product_version. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('select_another_product_version. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'select_another_product_version. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_project_page = SearchHelperProjectPage(browser_firefox_bs_max_resolution)
    ptk_project_page.go_to_test_calc()
    logging.debug(
        'rascenka_unc_table_1.Перешел на проект для тестирования расчетов')
    ptk_project_page.click_on_calculation()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Рассчитать"')
    ptk_project_page.project_card_click_on_plus()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Плюс"')
    ptk_project_page.project_card_select_table_1()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "таблицу 1"')
    ptk_project_page.project_card_select_b_1()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Б1"')
    ptk_project_page.project_card_select_product()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на продукт')
    ptk_project_page.project_card_fill_amount()
    logging.debug(
        'rascenka_unc_table_1.Заполнил количество продукта')
    ptk_project_page.save_unc_button()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Сохранить"')
    ptk_project_page.comment_calculation()
    ptk_project_page.project_card_assert_value_amount()
    logging.debug(
        'rascenka_unc_table_1.Убедился что количество продукта соответствует введенному')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Edge, разрешение 1280х1024. Комментарий к расчету.')
@allure.story('Успешный комментарий к расчету.')
@allure.severity('Critical')
def test_comment_to_calculation_edge_max_resolution(browser_edge_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('calculation_comment.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_edge_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('select_another_product_version. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('select_another_product_version. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('select_another_product_version. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('select_another_product_version. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'select_another_product_version. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_project_page = SearchHelperProjectPage(browser_edge_bs_max_resolution)
    ptk_project_page.go_to_test_calc()
    logging.debug(
        'rascenka_unc_table_1.Перешел на проект для тестирования расчетов')
    ptk_project_page.click_on_calculation()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Рассчитать"')
    ptk_project_page.project_card_click_on_plus()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Плюс"')
    ptk_project_page.project_card_select_table_1()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "таблицу 1"')
    ptk_project_page.project_card_select_b_1()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Б1"')
    ptk_project_page.project_card_select_product()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на продукт')
    ptk_project_page.project_card_fill_amount()
    logging.debug(
        'rascenka_unc_table_1.Заполнил количество продукта')
    ptk_project_page.save_unc_button()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Сохранить"')
    ptk_project_page.comment_calculation()
    ptk_project_page.project_card_assert_value_amount()
    logging.debug(
        'rascenka_unc_table_1.Убедился что количество продукта соответствует введенному')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Safari, разрешение 1280х1024. Комментарий к расчету.')
@allure.story('Успешный комментарий к расчету.')
@allure.severity('Critical')
def test_comment_to_calculation_safari_max_resolution(browser_safari_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('calculation_comment.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_safari_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('select_another_product_version. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('select_another_product_version. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('select_another_product_version. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('select_another_product_version. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'select_another_product_version. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_project_page = SearchHelperProjectPage(browser_safari_bs_max_resolution)
    ptk_project_page.go_to_test_calc()
    logging.debug(
        'rascenka_unc_table_1.Перешел на проект для тестирования расчетов')
    ptk_project_page.click_on_calculation()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Рассчитать"')
    ptk_project_page.project_card_click_on_plus()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Плюс"')
    ptk_project_page.project_card_select_table_1()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "таблицу 1"')
    ptk_project_page.project_card_select_b_1()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Б1"')
    ptk_project_page.project_card_select_product()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на продукт')
    ptk_project_page.project_card_fill_amount()
    logging.debug(
        'rascenka_unc_table_1.Заполнил количество продукта')
    ptk_project_page.save_unc_button()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Сохранить"')
    ptk_project_page.comment_calculation()
    ptk_project_page.project_card_assert_value_amount()
    logging.debug(
        'rascenka_unc_table_1.Убедился что количество продукта соответствует введенному')
