import logging

import allure
import pytest

from Login_Page import SearchHelper
from Project_Page import SearchHelperProjectPage


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Chrome. Проверка суммы с НДС.')
@allure.story('Проверка суммы с НДС.')
@allure.severity('Critical')
def test_summ_with_nds_chrome(browser_chrome_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('summ_with_nds.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_chrome_bs)
    ptk_login_page.go_to_login()
    logging.debug('summ_with_nds. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('summ_with_nds. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('summ_with_nds. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('summ_with_nds. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'summ_with_nds. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_project_page = SearchHelperProjectPage(browser_chrome_bs)
    ptk_project_page.get_project_id()
    logging.debug(
        'summ_with_nds.Сохранил идентификатор первого проекта')
    ptk_project_page.go_to_project_card()
    logging.debug(
        'summ_with_nds.Перешел в карточку первого проекта')
    ptk_project_page.assert_project_id()
    logging.debug(
        'summ_with_nds.Находясь в карточке проекта, убедился что попал именно на выбранный проект, сверив идентификатор первого проекта с идентификатором проекта на котором нахожусь')
    ptk_project_page.click_on_calculation()
    logging.debug(
        'summ_with_nds.Кликнул на "Рассчитать"')
    ptk_project_page.summa_s_nds()
    logging.debug(
        'summ_with_nds.Изменил сумму с НДС')
    ptk_project_page.cancel_calculation()
    logging.debug(
        'summ_with_nds.Закрыл блок рассчетов')
    ptk_project_page.assert_summa_s_nds()
    logging.debug(
        'summ_with_nds.Убедился, что сумма изменилась')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Firefox. Проверка суммы с НДС.')
@allure.story('Проверка суммы с НДС.')
@allure.severity('Critical')
def test_summ_with_nds_firefox(browser_firefox_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('summ_with_nds.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_firefox_bs)
    ptk_login_page.go_to_login()
    logging.debug('summ_with_nds. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('summ_with_nds. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('summ_with_nds. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('summ_with_nds. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'summ_with_nds. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_project_page = SearchHelperProjectPage(browser_firefox_bs)
    ptk_project_page.get_project_id()
    logging.debug(
        'summ_with_nds.Сохранил идентификатор первого проекта')
    ptk_project_page.go_to_project_card()
    logging.debug(
        'summ_with_nds.Перешел в карточку первого проекта')
    ptk_project_page.assert_project_id()
    logging.debug(
        'summ_with_nds.Находясь в карточке проекта, убедился что попал именно на выбранный проект, сверив идентификатор первого проекта с идентификатором проекта на котором нахожусь')
    ptk_project_page.click_on_calculation()
    logging.debug(
        'summ_with_nds.Кликнул на "Рассчитать"')
    ptk_project_page.summa_s_nds()
    logging.debug(
        'summ_with_nds.Изменил сумму с НДС')
    ptk_project_page.cancel_calculation()
    logging.debug(
        'summ_with_nds.Закрыл блок рассчетов')
    ptk_project_page.assert_summa_s_nds()
    logging.debug(
        'summ_with_nds.Убедился, что сумма изменилась')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Edge. Проверка суммы с НДС.')
@allure.story('Проверка суммы с НДС.')
@allure.severity('Critical')
def test_summ_with_nds_edge(browser_edge_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('summ_with_nds.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_edge_bs)
    ptk_login_page.go_to_login()
    logging.debug('summ_with_nds. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('summ_with_nds. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('summ_with_nds. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('summ_with_nds. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'summ_with_nds. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_project_page = SearchHelperProjectPage(browser_edge_bs)
    ptk_project_page.get_project_id()
    logging.debug(
        'summ_with_nds.Сохранил идентификатор первого проекта')
    ptk_project_page.go_to_project_card()
    logging.debug(
        'summ_with_nds.Перешел в карточку первого проекта')
    ptk_project_page.assert_project_id()
    logging.debug(
        'summ_with_nds.Находясь в карточке проекта, убедился что попал именно на выбранный проект, сверив идентификатор первого проекта с идентификатором проекта на котором нахожусь')
    ptk_project_page.click_on_calculation()
    logging.debug(
        'summ_with_nds.Кликнул на "Рассчитать"')
    ptk_project_page.summa_s_nds()
    logging.debug(
        'summ_with_nds.Изменил сумму с НДС')
    ptk_project_page.cancel_calculation()
    logging.debug(
        'summ_with_nds.Закрыл блок рассчетов')
    ptk_project_page.assert_summa_s_nds()
    logging.debug(
        'summ_with_nds.Убедился, что сумма изменилась')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Safari. Проверка суммы с НДС.')
@allure.story('Проверка суммы с НДС.')
@allure.severity('Critical')
def test_summ_with_nds_safari(browser_safari_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('summ_with_nds.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_safari_bs)
    ptk_login_page.go_to_login()
    logging.debug('summ_with_nds. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('summ_with_nds. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('summ_with_nds. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('summ_with_nds. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'summ_with_nds. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_project_page = SearchHelperProjectPage(browser_safari_bs)
    ptk_project_page.get_project_id()
    logging.debug(
        'summ_with_nds.Сохранил идентификатор первого проекта')
    ptk_project_page.go_to_project_card()
    logging.debug(
        'summ_with_nds.Перешел в карточку первого проекта')
    ptk_project_page.assert_project_id()
    logging.debug(
        'summ_with_nds.Находясь в карточке проекта, убедился что попал именно на выбранный проект, сверив идентификатор первого проекта с идентификатором проекта на котором нахожусь')
    ptk_project_page.click_on_calculation()
    logging.debug(
        'summ_with_nds.Кликнул на "Рассчитать"')
    ptk_project_page.summa_s_nds()
    logging.debug(
        'summ_with_nds.Очистил поле "Сумма с НДС"')
    ptk_project_page.send_keys_to_nds()
    logging.debug(
        'summ_with_nds.Изменил сумму с НДС')
    ptk_project_page.cancel_calculation()
    logging.debug(
        'summ_with_nds.Закрыл блок рассчетов')
    ptk_project_page.assert_summa_s_nds()
    logging.debug(
        'summ_with_nds.Убедился, что сумма изменилась')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Chrome, разрешение 1280х10240. Проверка суммы с НДС.')
@allure.story('Проверка суммы с НДС.')
@allure.severity('Critical')
def test_summ_with_nds_chrome_max_resolution(browser_chrome_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('summ_with_nds.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_chrome_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('summ_with_nds. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('summ_with_nds. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('summ_with_nds. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('summ_with_nds. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'summ_with_nds. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_project_page = SearchHelperProjectPage(browser_chrome_bs_max_resolution)
    ptk_project_page.get_project_id()
    logging.debug(
        'summ_with_nds.Сохранил идентификатор первого проекта')
    ptk_project_page.go_to_project_card()
    logging.debug(
        'summ_with_nds.Перешел в карточку первого проекта')
    ptk_project_page.assert_project_id()
    logging.debug(
        'summ_with_nds.Находясь в карточке проекта, убедился что попал именно на выбранный проект, сверив идентификатор первого проекта с идентификатором проекта на котором нахожусь')
    ptk_project_page.click_on_calculation()
    logging.debug(
        'summ_with_nds.Кликнул на "Рассчитать"')
    ptk_project_page.summa_s_nds()
    logging.debug(
        'summ_with_nds.Изменил сумму с НДС')
    ptk_project_page.cancel_calculation()
    logging.debug(
        'summ_with_nds.Закрыл блок рассчетов')
    ptk_project_page.assert_summa_s_nds()
    logging.debug(
        'summ_with_nds.Убедился, что сумма изменилась')

@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Firefox, разрешение 1280х10240. Проверка суммы с НДС.')
@allure.story('Проверка суммы с НДС.')
@allure.severity('Critical')
def test_summ_with_nds_firefox_max_resolution(browser_firefox_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('summ_with_nds.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_firefox_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('summ_with_nds. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('summ_with_nds. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('summ_with_nds. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('summ_with_nds. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'summ_with_nds. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_project_page = SearchHelperProjectPage(browser_firefox_bs_max_resolution)
    ptk_project_page.get_project_id()
    logging.debug(
        'summ_with_nds.Сохранил идентификатор первого проекта')
    ptk_project_page.go_to_project_card()
    logging.debug(
        'summ_with_nds.Перешел в карточку первого проекта')
    ptk_project_page.assert_project_id()
    logging.debug(
        'summ_with_nds.Находясь в карточке проекта, убедился что попал именно на выбранный проект, сверив идентификатор первого проекта с идентификатором проекта на котором нахожусь')
    ptk_project_page.click_on_calculation()
    logging.debug(
        'summ_with_nds.Кликнул на "Рассчитать"')
    ptk_project_page.summa_s_nds()
    logging.debug(
        'summ_with_nds.Изменил сумму с НДС')
    ptk_project_page.cancel_calculation()
    logging.debug(
        'summ_with_nds.Закрыл блок рассчетов')
    ptk_project_page.assert_summa_s_nds()
    logging.debug(
        'summ_with_nds.Убедился, что сумма изменилась')

@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Edge, разрешение 1280х10240. Проверка суммы с НДС.')
@allure.story('Проверка суммы с НДС.')
@allure.severity('Critical')
def test_summ_with_nds_edge_max_resolution(browser_edge_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('summ_with_nds.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_edge_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('summ_with_nds. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('summ_with_nds. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('summ_with_nds. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('summ_with_nds. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'summ_with_nds. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_project_page = SearchHelperProjectPage(browser_edge_bs_max_resolution)
    ptk_project_page.get_project_id()
    logging.debug(
        'summ_with_nds.Сохранил идентификатор первого проекта')
    ptk_project_page.go_to_project_card()
    logging.debug(
        'summ_with_nds.Перешел в карточку первого проекта')
    ptk_project_page.assert_project_id()
    logging.debug(
        'summ_with_nds.Находясь в карточке проекта, убедился что попал именно на выбранный проект, сверив идентификатор первого проекта с идентификатором проекта на котором нахожусь')
    ptk_project_page.click_on_calculation()
    logging.debug(
        'summ_with_nds.Кликнул на "Рассчитать"')
    ptk_project_page.summa_s_nds()
    logging.debug(
        'summ_with_nds.Изменил сумму с НДС')
    ptk_project_page.cancel_calculation()
    logging.debug(
        'summ_with_nds.Закрыл блок рассчетов')
    ptk_project_page.assert_summa_s_nds()
    logging.debug(
        'summ_with_nds.Убедился, что сумма изменилась')

@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Safari, разрешение 1280х10240. Проверка суммы с НДС.')
@allure.story('Проверка суммы с НДС.')
@allure.severity('Critical')
def test_summ_with_nds_safari_max_resolution(browser_safari_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('summ_with_nds.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_safari_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('summ_with_nds. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('summ_with_nds. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('summ_with_nds. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('summ_with_nds. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'summ_with_nds. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_project_page = SearchHelperProjectPage(browser_safari_bs_max_resolution)
    ptk_project_page.get_project_id()
    logging.debug(
        'summ_with_nds.Сохранил идентификатор первого проекта')
    ptk_project_page.go_to_project_card()
    logging.debug(
        'summ_with_nds.Перешел в карточку первого проекта')
    ptk_project_page.assert_project_id()
    logging.debug(
        'summ_with_nds.Находясь в карточке проекта, убедился что попал именно на выбранный проект, сверив идентификатор первого проекта с идентификатором проекта на котором нахожусь')
    ptk_project_page.click_on_calculation()
    logging.debug(
        'summ_with_nds.Кликнул на "Рассчитать"')
    ptk_project_page.summa_s_nds()
    logging.debug(
        'summ_with_nds.Очистил поле "Сумма с НДС"')
    ptk_project_page.send_keys_to_nds()
    logging.debug(
        'summ_with_nds.Изменил сумму с НДС')
    ptk_project_page.cancel_calculation()
    logging.debug(
        'summ_with_nds.Закрыл блок рассчетов')
    ptk_project_page.assert_summa_s_nds()
    logging.debug(
        'summ_with_nds.Убедился, что сумма изменилась')