import logging

import allure
import pytest

from Login_Page import SearchHelper
from Main_Page import SearchHelperMainPage
from Project_Page import SearchHelperProjectPage


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Chrome. Создание личного проекта, ввод филиала.')
@allure.story('Создание личного проекта')
@allure.severity('Critical')
def test_creating_personal_project_filling_filial_chrome(browser_chrome_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('creating_personal_project_filling_filial.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_chrome_bs)
    ptk_login_page.go_to_login()
    logging.debug('creating_personal_project_filling_filial. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('creating_personal_project_filling_filial. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('creating_personal_project_filling_filial. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('creating_personal_project_filling_filial. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'creating_personal_project_filling_filial. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_chrome_bs)
    ptk_main_page.click_on_personal_projects()
    logging.debug(
        'creating_personal_project_filling_filial. Кликнул на кнопку "Личные проекты"')
    ptk_main_page.create_new_personal_project()
    logging.debug(
        'creating_personal_project_filling_filial. Кликнул на кнопку "Новый проект +"')
    ptk_project_page = SearchHelperProjectPage(browser_chrome_bs)
    ptk_project_page.personal_project_dropdown_filial()
    logging.debug(
        'creating_personal_project_filling_filial. Кликнул на выпадающее меню филиала')
    ptk_project_page.select_filial_north_west()
    logging.debug(
        'creating_personal_project_filling_filial. Кликнул на филиал МЭС Северо-Запад')
    ptk_project_page.find_selected_filial()
    logging.debug(
        'creating_personal_project_filling_filial. Филиал выбран МЭС Северо-Запад')
    ptk_project_page.personal_project_save_disabled()
    logging.debug(
        'creating_personal_project_filling_id. Кнопка "Сохранить" отключена')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Firefox. Создание личного проекта, ввод филиала.')
@allure.story('Создание личного проекта')
@allure.severity('Critical')
def test_creating_personal_project_filling_filial_firefox(browser_firefox_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('creating_personal_project_filling_filial.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_firefox_bs)
    ptk_login_page.go_to_login()
    logging.debug('creating_personal_project_filling_filial. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('creating_personal_project_filling_filial. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('creating_personal_project_filling_filial. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('creating_personal_project_filling_filial. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'creating_personal_project_filling_filial. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_firefox_bs)
    ptk_main_page.click_on_personal_projects()
    logging.debug(
        'creating_personal_project_filling_filial. Кликнул на кнопку "Личные проекты"')
    ptk_main_page.create_new_personal_project()
    logging.debug(
        'creating_personal_project_filling_filial. Кликнул на кнопку "Новый проект +"')
    ptk_project_page = SearchHelperProjectPage(browser_firefox_bs)
    ptk_project_page.personal_project_dropdown_filial()
    logging.debug(
        'creating_personal_project_filling_filial. Кликнул на выпадающее меню филиала')
    ptk_project_page.select_filial_north_west()
    logging.debug(
        'creating_personal_project_filling_filial. Кликнул на филиал МЭС Северо-Запад')
    ptk_project_page.find_selected_filial()
    logging.debug(
        'creating_personal_project_filling_filial. Филиал выбран МЭС Северо-Запад')
    ptk_project_page.personal_project_save_disabled()
    logging.debug(
        'creating_personal_project_filling_id. Кнопка "Сохранить" отключена')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Edge. Создание личного проекта, ввод филиала.')
@allure.story('Создание личного проекта')
@allure.severity('Critical')
def test_creating_personal_project_filling_filial_edge(browser_edge_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('creating_personal_project_filling_filial.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_edge_bs)
    ptk_login_page.go_to_login()
    logging.debug('creating_personal_project_filling_filial. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('creating_personal_project_filling_filial. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('creating_personal_project_filling_filial. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('creating_personal_project_filling_filial. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'creating_personal_project_filling_filial. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_edge_bs)
    ptk_main_page.click_on_personal_projects()
    logging.debug(
        'creating_personal_project_filling_filial. Кликнул на кнопку "Личные проекты"')
    ptk_main_page.create_new_personal_project()
    logging.debug(
        'creating_personal_project_filling_filial. Кликнул на кнопку "Новый проект +"')
    ptk_project_page = SearchHelperProjectPage(browser_edge_bs)
    ptk_project_page.personal_project_dropdown_filial()
    logging.debug(
        'creating_personal_project_filling_filial. Кликнул на выпадающее меню филиала')
    ptk_project_page.select_filial_north_west()
    logging.debug(
        'creating_personal_project_filling_filial. Кликнул на филиал МЭС Северо-Запад')
    ptk_project_page.find_selected_filial()
    logging.debug(
        'creating_personal_project_filling_filial. Филиал выбран МЭС Северо-Запад')
    ptk_project_page.personal_project_save_disabled()
    logging.debug(
        'creating_personal_project_filling_id. Кнопка "Сохранить" отключена')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Safari. Создание личного проекта, ввод филиала.')
@allure.story('Создание личного проекта')
@allure.severity('Critical')
def test_creating_personal_project_filling_filial_safari(browser_safari_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('creating_personal_project_filling_filial.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_safari_bs)
    ptk_login_page.go_to_login()
    logging.debug('creating_personal_project_filling_filial. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('creating_personal_project_filling_filial. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('creating_personal_project_filling_filial. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('creating_personal_project_filling_filial. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'creating_personal_project_filling_filial. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_safari_bs)
    ptk_main_page.click_on_personal_projects()
    logging.debug(
        'creating_personal_project_filling_filial. Кликнул на кнопку "Личные проекты"')
    ptk_main_page.create_new_personal_project()
    logging.debug(
        'creating_personal_project_filling_filial. Кликнул на кнопку "Новый проект +"')
    ptk_project_page = SearchHelperProjectPage(browser_safari_bs)
    ptk_project_page.personal_project_dropdown_filial()
    logging.debug(
        'creating_personal_project_filling_filial. Кликнул на выпадающее меню филиала')
    ptk_project_page.select_filial_north_west()
    logging.debug(
        'creating_personal_project_filling_filial. Кликнул на филиал МЭС Северо-Запад')
    ptk_project_page.find_selected_filial()
    logging.debug(
        'creating_personal_project_filling_filial. Филиал выбран МЭС Северо-Запад')
    ptk_project_page.personal_project_save_disabled()
    logging.debug(
        'creating_personal_project_filling_id. Кнопка "Сохранить" отключена')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Chrome, разрешение 1280х1024. Создание личного проекта, ввод филиала.')
@allure.story('Создание личного проекта')
@allure.severity('Critical')
def test_creating_personal_project_filling_filial_chrome_max_resolution(browser_chrome_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('creating_personal_project_filling_filial.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_chrome_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('creating_personal_project_filling_filial. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('creating_personal_project_filling_filial. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('creating_personal_project_filling_filial. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('creating_personal_project_filling_filial. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'creating_personal_project_filling_filial. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_chrome_bs_max_resolution)
    ptk_main_page.click_on_personal_projects()
    logging.debug(
        'creating_personal_project_filling_filial. Кликнул на кнопку "Личные проекты"')
    ptk_main_page.create_new_personal_project()
    logging.debug(
        'creating_personal_project_filling_filial. Кликнул на кнопку "Новый проект +"')
    ptk_project_page = SearchHelperProjectPage(browser_chrome_bs_max_resolution)
    ptk_project_page.personal_project_dropdown_filial()
    logging.debug(
        'creating_personal_project_filling_filial. Кликнул на выпадающее меню филиала')
    ptk_project_page.select_filial_north_west()
    logging.debug(
        'creating_personal_project_filling_filial. Кликнул на филиал МЭС Северо-Запад')
    ptk_project_page.find_selected_filial()
    logging.debug(
        'creating_personal_project_filling_filial. Филиал выбран МЭС Северо-Запад')
    ptk_project_page.personal_project_save_disabled()
    logging.debug(
        'creating_personal_project_filling_id. Кнопка "Сохранить" отключена')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Firefox, разрешение 1280х1024. Создание личного проекта, ввод филиала.')
@allure.story('Создание личного проекта')
@allure.severity('Critical')
def test_creating_personal_project_filling_filial_firefox_max_resolution(browser_firefox_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('creating_personal_project_filling_filial.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_firefox_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('creating_personal_project_filling_filial. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('creating_personal_project_filling_filial. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('creating_personal_project_filling_filial. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('creating_personal_project_filling_filial. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'creating_personal_project_filling_filial. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_firefox_bs_max_resolution)
    ptk_main_page.click_on_personal_projects()
    logging.debug(
        'creating_personal_project_filling_filial. Кликнул на кнопку "Личные проекты"')
    ptk_main_page.create_new_personal_project()
    logging.debug(
        'creating_personal_project_filling_filial. Кликнул на кнопку "Новый проект +"')
    ptk_project_page = SearchHelperProjectPage(browser_firefox_bs_max_resolution)
    ptk_project_page.personal_project_dropdown_filial()
    logging.debug(
        'creating_personal_project_filling_filial. Кликнул на выпадающее меню филиала')
    ptk_project_page.select_filial_north_west()
    logging.debug(
        'creating_personal_project_filling_filial. Кликнул на филиал МЭС Северо-Запад')
    ptk_project_page.find_selected_filial()
    logging.debug(
        'creating_personal_project_filling_filial. Филиал выбран МЭС Северо-Запад')
    ptk_project_page.personal_project_save_disabled()
    logging.debug(
        'creating_personal_project_filling_id. Кнопка "Сохранить" отключена')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Edge, разрешение 1280х1024. Создание личного проекта, ввод филиала.')
@allure.story('Создание личного проекта')
@allure.severity('Critical')
def test_creating_personal_project_filling_filial_edge_max_resolution(browser_edge_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('creating_personal_project_filling_filial.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_edge_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('creating_personal_project_filling_filial. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('creating_personal_project_filling_filial. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('creating_personal_project_filling_filial. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('creating_personal_project_filling_filial. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'creating_personal_project_filling_filial. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_edge_bs_max_resolution)
    ptk_main_page.click_on_personal_projects()
    logging.debug(
        'creating_personal_project_filling_filial. Кликнул на кнопку "Личные проекты"')
    ptk_main_page.create_new_personal_project()
    logging.debug(
        'creating_personal_project_filling_filial. Кликнул на кнопку "Новый проект +"')
    ptk_project_page = SearchHelperProjectPage(browser_edge_bs_max_resolution)
    ptk_project_page.personal_project_dropdown_filial()
    logging.debug(
        'creating_personal_project_filling_filial. Кликнул на выпадающее меню филиала')
    ptk_project_page.select_filial_north_west()
    logging.debug(
        'creating_personal_project_filling_filial. Кликнул на филиал МЭС Северо-Запад')
    ptk_project_page.find_selected_filial()
    logging.debug(
        'creating_personal_project_filling_filial. Филиал выбран МЭС Северо-Запад')
    ptk_project_page.personal_project_save_disabled()
    logging.debug(
        'creating_personal_project_filling_id. Кнопка "Сохранить" отключена')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Safari, разрешение 1280х1024. Создание личного проекта, ввод филиала.')
@allure.story('Создание личного проекта')
@allure.severity('Critical')
def test_creating_personal_project_filling_filial_safari_max_resolution(browser_safari_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('creating_personal_project_filling_filial.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_safari_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('creating_personal_project_filling_filial. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('creating_personal_project_filling_filial. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('creating_personal_project_filling_filial. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('creating_personal_project_filling_filial. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'creating_personal_project_filling_filial. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_safari_bs_max_resolution)
    ptk_main_page.click_on_personal_projects()
    logging.debug(
        'creating_personal_project_filling_filial. Кликнул на кнопку "Личные проекты"')
    ptk_main_page.create_new_personal_project()
    logging.debug(
        'creating_personal_project_filling_filial. Кликнул на кнопку "Новый проект +"')
    ptk_project_page = SearchHelperProjectPage(browser_safari_bs_max_resolution)
    ptk_project_page.personal_project_dropdown_filial()
    logging.debug(
        'creating_personal_project_filling_filial. Кликнул на выпадающее меню филиала')
    ptk_project_page.select_filial_north_west()
    logging.debug(
        'creating_personal_project_filling_filial. Кликнул на филиал МЭС Северо-Запад')
    ptk_project_page.find_selected_filial()
    logging.debug(
        'creating_personal_project_filling_filial. Филиал выбран МЭС Северо-Запад')
    ptk_project_page.personal_project_save_disabled()
    logging.debug(
        'creating_personal_project_filling_id. Кнопка "Сохранить" отключена')