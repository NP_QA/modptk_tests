import logging

import allure
import pytest

from Login_Page import SearchHelper
from Main_Page import SearchHelperMainPage
from Project_Page import SearchHelperProjectPage


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Chrome. Редактирование личного проекта: идентификатор.')
@allure.story('Редактирование личного проекта: идентификатор.')
@allure.severity('Critical')
def test_edit_id_personal_project_chrome(browser_chrome_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('edit_id_personal_project.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_chrome_bs)
    ptk_login_page.go_to_login()
    logging.debug('edit_id_personal_project. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('edit_id_personal_project. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('edit_id_personal_project. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('edit_id_personal_project. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'edit_id_personal_project. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_chrome_bs)
    ptk_main_page.click_on_personal_projects()
    logging.debug(
        'edit_id_personal_project. Кликнул на кнопку "Личные проекты"')
    ptk_main_page.create_new_personal_project()
    logging.debug(
        'edit_id_personal_project. Кликнул на кнопку "Новый проект +"')
    ptk_project_page = SearchHelperProjectPage(browser_chrome_bs)
    ptk_project_page.id_personal_project()
    logging.debug(
        'edit_id_personal_project. Указал идентификатор проекта')
    ptk_project_page.name_personal_project()
    logging.debug(
        'edit_id_personal_project. Указал название проекта')
    ptk_project_page.personal_project_dropdown_filial()
    logging.debug(
        'edit_id_personal_project. Кликнул на выпадающее меню филиала')
    ptk_project_page.select_filial_north_west()
    logging.debug(
        'edit_id_personal_project. Выбрал северо-западный МЭС')
    ptk_project_page.find_selected_filial()
    logging.debug(
        'edit_id_personal_project. Убедился, что филиал выбран')
    ptk_project_page.project_card_save_button()
    logging.debug(
        'edit_id_personal_project. Кликнул на кнопку "Сохранить"')
    ptk_project_page.project_created_notification()
    logging.debug(
        'edit_id_personal_project. Получил уведомление о том, что проект создан')
    ptk_project_page.project_edit_icon()
    logging.debug(
        'edit_id_personal_project. Кликнул на кнопку редактирования')
    ptk_project_page.free_id_personal_project('Случайный идентификатор')
    logging.debug(
        'edit_id_personal_project. Ввел идентификатор')
    ptk_project_page.project_card_save_button()
    logging.debug(
        'edit_id_personal_project. Кликнул на кнопку "Сохранить"')
    ptk_project_page.project_created_notification()
    logging.debug(
        'edit_id_personal_project. Получил уведомление о том, что проект создан')
    ptk_project_page.assert_free_id_personal_project()
    logging.debug(
        'edit_id_personal_project. Убедился, что идентификатор совпадает измененному')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Firefox.Редактирование личного проекта: идентификатор.')
@allure.story('Редактирование личного проекта: идентификатор.')
@allure.severity('Critical')
def test_edit_id_personal_project_firefox(browser_firefox_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('edit_id_personal_project.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_firefox_bs)
    ptk_login_page.go_to_login()
    logging.debug('edit_id_personal_project. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('edit_id_personal_project. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('edit_id_personal_project. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('edit_id_personal_project. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'edit_id_personal_project. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_firefox_bs)
    ptk_main_page.click_on_personal_projects()
    logging.debug(
        'edit_id_personal_project. Кликнул на кнопку "Личные проекты"')
    ptk_main_page.create_new_personal_project()
    logging.debug(
        'edit_id_personal_project. Кликнул на кнопку "Новый проект +"')
    ptk_project_page = SearchHelperProjectPage(browser_firefox_bs)
    ptk_project_page.id_personal_project()
    logging.debug(
        'edit_id_personal_project. Указал идентификатор проекта')
    ptk_project_page.name_personal_project()
    logging.debug(
        'edit_id_personal_project. Указал название проекта')
    ptk_project_page.personal_project_dropdown_filial()
    logging.debug(
        'edit_id_personal_project. Кликнул на выпадающее меню филиала')
    ptk_project_page.select_filial_north_west()
    logging.debug(
        'edit_id_personal_project. Выбрал северо-западный МЭС')
    ptk_project_page.find_selected_filial()
    logging.debug(
        'edit_id_personal_project. Убедился, что филиал выбран')
    ptk_project_page.project_card_save_button()
    logging.debug(
        'edit_id_personal_project. Кликнул на кнопку "Сохранить"')
    ptk_project_page.project_created_notification()
    logging.debug(
        'edit_id_personal_project. Получил уведомление о том, что проект создан')
    ptk_project_page.project_edit_icon()
    logging.debug(
        'edit_id_personal_project. Кликнул на кнопку редактирования')
    ptk_project_page.free_id_personal_project('Случайный идентификатор')
    logging.debug(
        'edit_id_personal_project. Ввел идентификатор')
    ptk_project_page.project_card_save_button()
    logging.debug(
        'edit_id_personal_project. Кликнул на кнопку "Сохранить"')
    ptk_project_page.project_created_notification()
    logging.debug(
        'edit_id_personal_project. Получил уведомление о том, что проект создан')
    ptk_project_page.assert_free_id_personal_project()
    logging.debug(
        'edit_id_personal_project. Убедился, что идентификатор совпадает измененному')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Edge.Редактирование личного проекта: идентификатор.')
@allure.story('Редактирование личного проекта: идентификатор.')
@allure.severity('Critical')
def test_edit_id_personal_project_edge(browser_edge_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('edit_id_personal_project.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_edge_bs)
    ptk_login_page.go_to_login()
    logging.debug('edit_id_personal_project. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('edit_id_personal_project. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('edit_id_personal_project. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('edit_id_personal_project. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'edit_id_personal_project. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_edge_bs)
    ptk_main_page.click_on_personal_projects()
    logging.debug(
        'edit_id_personal_project. Кликнул на кнопку "Личные проекты"')
    ptk_main_page.create_new_personal_project()
    logging.debug(
        'edit_id_personal_project. Кликнул на кнопку "Новый проект +"')
    ptk_project_page = SearchHelperProjectPage(browser_edge_bs)
    ptk_project_page.id_personal_project()
    logging.debug(
        'edit_id_personal_project. Указал идентификатор проекта')
    ptk_project_page.name_personal_project()
    logging.debug(
        'edit_id_personal_project. Указал название проекта')
    ptk_project_page.personal_project_dropdown_filial()
    logging.debug(
        'edit_id_personal_project. Кликнул на выпадающее меню филиала')
    ptk_project_page.select_filial_north_west()
    logging.debug(
        'edit_id_personal_project. Выбрал северо-западный МЭС')
    ptk_project_page.find_selected_filial()
    logging.debug(
        'edit_id_personal_project. Убедился, что филиал выбран')
    ptk_project_page.project_card_save_button()
    logging.debug(
        'edit_id_personal_project. Кликнул на кнопку "Сохранить"')
    ptk_project_page.project_created_notification()
    logging.debug(
        'edit_id_personal_project. Получил уведомление о том, что проект создан')
    ptk_project_page.project_edit_icon()
    logging.debug(
        'edit_id_personal_project. Кликнул на кнопку редактирования')
    ptk_project_page.free_id_personal_project('Случайный идентификатор')
    logging.debug(
        'edit_id_personal_project. Ввел идентификатор')
    ptk_project_page.project_card_save_button()
    logging.debug(
        'edit_id_personal_project. Кликнул на кнопку "Сохранить"')
    ptk_project_page.project_created_notification()
    logging.debug(
        'edit_id_personal_project. Получил уведомление о том, что проект создан')
    ptk_project_page.assert_free_id_personal_project()
    logging.debug(
        'edit_id_personal_project. Убедился, что идентификатор совпадает измененному')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Safari.Редактирование личного проекта: идентификатор.')
@allure.story('Редактирование личного проекта: идентификатор.')
@allure.severity('Critical')
def test_edit_id_personal_project_safari(browser_safari_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('edit_id_personal_project.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_safari_bs)
    ptk_login_page.go_to_login()
    logging.debug('edit_id_personal_project. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('edit_id_personal_project. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('edit_id_personal_project. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('edit_id_personal_project. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'edit_id_personal_project. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_safari_bs)
    ptk_main_page.click_on_personal_projects()
    logging.debug(
        'edit_id_personal_project. Кликнул на кнопку "Личные проекты"')
    ptk_main_page.create_new_personal_project()
    logging.debug(
        'edit_id_personal_project. Кликнул на кнопку "Новый проект +"')
    ptk_project_page = SearchHelperProjectPage(browser_safari_bs)
    ptk_project_page.id_personal_project()
    logging.debug(
        'edit_id_personal_project. Указал идентификатор проекта')
    ptk_project_page.name_personal_project()
    logging.debug(
        'edit_id_personal_project. Указал название проекта')
    ptk_project_page.personal_project_dropdown_filial()
    logging.debug(
        'edit_id_personal_project. Кликнул на выпадающее меню филиала')
    ptk_project_page.select_filial_north_west()
    logging.debug(
        'edit_id_personal_project. Выбрал северо-западный МЭС')
    ptk_project_page.find_selected_filial()
    logging.debug(
        'edit_id_personal_project. Убедился, что филиал выбран')
    ptk_project_page.project_card_save_button()
    logging.debug(
        'edit_id_personal_project. Кликнул на кнопку "Сохранить"')
    ptk_project_page.project_created_notification()
    logging.debug(
        'edit_id_personal_project. Получил уведомление о том, что проект создан')
    ptk_project_page.project_edit_icon()
    logging.debug(
        'edit_id_personal_project. Кликнул на кнопку редактирования')
    ptk_project_page.free_id_personal_project('Случайный идентификатор')
    logging.debug(
        'edit_id_personal_project. Ввел идентификатор')
    ptk_project_page.project_card_save_button()
    logging.debug(
        'edit_id_personal_project. Кликнул на кнопку "Сохранить"')
    ptk_project_page.project_created_notification()
    logging.debug(
        'edit_id_personal_project. Получил уведомление о том, что проект создан')
    ptk_project_page.assert_free_id_personal_project()
    logging.debug(
        'edit_id_personal_project. Убедился, что идентификатор совпадает измененному')

@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Chrome, разрешение 1280х1024.Редактирование личного проекта: идентификатор.')
@allure.story('Редактирование личного проекта: идентификатор.')
@allure.severity('Critical')
def test_edit_id_personal_project_chrome_max_resolution(browser_chrome_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('edit_id_personal_project.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_chrome_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('edit_id_personal_project. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('edit_id_personal_project. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('edit_id_personal_project. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('edit_id_personal_project. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'edit_id_personal_project. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_chrome_bs_max_resolution)
    ptk_main_page.click_on_personal_projects()
    logging.debug(
        'edit_id_personal_project. Кликнул на кнопку "Личные проекты"')
    ptk_main_page.create_new_personal_project()
    logging.debug(
        'edit_id_personal_project. Кликнул на кнопку "Новый проект +"')
    ptk_project_page = SearchHelperProjectPage(browser_chrome_bs_max_resolution)
    ptk_project_page.id_personal_project()
    logging.debug(
        'edit_id_personal_project. Указал идентификатор проекта')
    ptk_project_page.name_personal_project()
    logging.debug(
        'edit_id_personal_project. Указал название проекта')
    ptk_project_page.personal_project_dropdown_filial()
    logging.debug(
        'edit_id_personal_project. Кликнул на выпадающее меню филиала')
    ptk_project_page.select_filial_north_west()
    logging.debug(
        'edit_id_personal_project. Выбрал северо-западный МЭС')
    ptk_project_page.find_selected_filial()
    logging.debug(
        'edit_id_personal_project. Убедился, что филиал выбран')
    ptk_project_page.project_card_save_button()
    logging.debug(
        'edit_id_personal_project. Кликнул на кнопку "Сохранить"')
    ptk_project_page.project_created_notification()
    logging.debug(
        'edit_id_personal_project. Получил уведомление о том, что проект создан')
    ptk_project_page.project_edit_icon()
    logging.debug(
        'edit_id_personal_project. Кликнул на кнопку редактирования')
    ptk_project_page.free_id_personal_project('Случайный идентификатор')
    logging.debug(
        'edit_id_personal_project. Ввел идентификатор')
    ptk_project_page.project_card_save_button()
    logging.debug(
        'edit_id_personal_project. Кликнул на кнопку "Сохранить"')
    ptk_project_page.project_created_notification()
    logging.debug(
        'edit_id_personal_project. Получил уведомление о том, что проект создан')
    ptk_project_page.assert_free_id_personal_project()
    logging.debug(
        'edit_id_personal_project. Убедился, что идентификатор совпадает измененному')


@pytest.mark.smoke_test_modptk
@allure.feature(
    'Smoke тестирование на Firefox, разрешение 1280х1024.Редактирование личного проекта: идентификатор.')
@allure.story('Редактирование личного проекта: идентификатор.')
@allure.severity('Critical')
def test_edit_id_personal_project_firefox_max_resolution(browser_firefox_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('edit_id_personal_project.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_firefox_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('edit_id_personal_project. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('edit_id_personal_project. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('edit_id_personal_project. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('edit_id_personal_project. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'edit_id_personal_project. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_firefox_bs_max_resolution)
    ptk_main_page.click_on_personal_projects()
    logging.debug(
        'edit_id_personal_project. Кликнул на кнопку "Личные проекты"')
    ptk_main_page.create_new_personal_project()
    logging.debug(
        'edit_id_personal_project. Кликнул на кнопку "Новый проект +"')
    ptk_project_page = SearchHelperProjectPage(browser_firefox_bs_max_resolution)
    ptk_project_page.id_personal_project()
    logging.debug(
        'edit_id_personal_project. Указал идентификатор проекта')
    ptk_project_page.name_personal_project()
    logging.debug(
        'edit_id_personal_project. Указал название проекта')
    ptk_project_page.personal_project_dropdown_filial()
    logging.debug(
        'edit_id_personal_project. Кликнул на выпадающее меню филиала')
    ptk_project_page.select_filial_north_west()
    logging.debug(
        'edit_id_personal_project. Выбрал северо-западный МЭС')
    ptk_project_page.find_selected_filial()
    logging.debug(
        'edit_id_personal_project. Убедился, что филиал выбран')
    ptk_project_page.project_card_save_button()
    logging.debug(
        'edit_id_personal_project. Кликнул на кнопку "Сохранить"')
    ptk_project_page.project_created_notification()
    logging.debug(
        'edit_id_personal_project. Получил уведомление о том, что проект создан')
    ptk_project_page.project_edit_icon()
    logging.debug(
        'edit_id_personal_project. Кликнул на кнопку редактирования')
    ptk_project_page.free_id_personal_project('Случайный идентификатор')
    logging.debug(
        'edit_id_personal_project. Ввел идентификатор')
    ptk_project_page.project_card_save_button()
    logging.debug(
        'edit_id_personal_project. Кликнул на кнопку "Сохранить"')
    ptk_project_page.project_created_notification()
    logging.debug(
        'edit_id_personal_project. Получил уведомление о том, что проект создан')
    ptk_project_page.assert_free_id_personal_project()
    logging.debug(
        'edit_id_personal_project. Убедился, что идентификатор совпадает измененному')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Edge, разрешение 1280х1024.Редактирование личного проекта: идентификатор.')
@allure.story('Редактирование личного проекта: идентификатор.')
@allure.severity('Critical')
def test_edit_id_personal_project_edge_max_resolution(browser_edge_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('edit_id_personal_project.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_edge_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('edit_id_personal_project. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('edit_id_personal_project. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('edit_id_personal_project. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('edit_id_personal_project. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'edit_id_personal_project. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_edge_bs_max_resolution)
    ptk_main_page.click_on_personal_projects()
    logging.debug(
        'edit_id_personal_project. Кликнул на кнопку "Личные проекты"')
    ptk_main_page.create_new_personal_project()
    logging.debug(
        'edit_id_personal_project. Кликнул на кнопку "Новый проект +"')
    ptk_project_page = SearchHelperProjectPage(browser_edge_bs_max_resolution)
    ptk_project_page.id_personal_project()
    logging.debug(
        'edit_id_personal_project. Указал идентификатор проекта')
    ptk_project_page.name_personal_project()
    logging.debug(
        'edit_id_personal_project. Указал название проекта')
    ptk_project_page.personal_project_dropdown_filial()
    logging.debug(
        'edit_id_personal_project. Кликнул на выпадающее меню филиала')
    ptk_project_page.select_filial_north_west()
    logging.debug(
        'edit_id_personal_project. Выбрал северо-западный МЭС')
    ptk_project_page.find_selected_filial()
    logging.debug(
        'edit_id_personal_project. Убедился, что филиал выбран')
    ptk_project_page.project_card_save_button()
    logging.debug(
        'edit_id_personal_project. Кликнул на кнопку "Сохранить"')
    ptk_project_page.project_created_notification()
    logging.debug(
        'edit_id_personal_project. Получил уведомление о том, что проект создан')
    ptk_project_page.project_edit_icon()
    logging.debug(
        'edit_id_personal_project. Кликнул на кнопку редактирования')
    ptk_project_page.free_id_personal_project('Случайный идентификатор')
    logging.debug(
        'edit_id_personal_project. Ввел идентификатор')
    ptk_project_page.project_card_save_button()
    logging.debug(
        'edit_id_personal_project. Кликнул на кнопку "Сохранить"')
    ptk_project_page.project_created_notification()
    logging.debug(
        'edit_id_personal_project. Получил уведомление о том, что проект создан')
    ptk_project_page.assert_free_id_personal_project()
    logging.debug(
        'edit_id_personal_project. Убедился, что идентификатор совпадает измененному')

@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Safari, разрешение 1280х1024.Редактирование личного проекта: идентификатор.')
@allure.story('Редактирование личного проекта: идентификатор.')
@allure.severity('Critical')
def test_edit_id_personal_project_safari_max_resolution(browser_safari_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('edit_id_personal_project.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_safari_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('edit_id_personal_project. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('edit_id_personal_project. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('edit_id_personal_project. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('edit_id_personal_project. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'edit_id_personal_project. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_safari_bs_max_resolution)
    ptk_main_page.click_on_personal_projects()
    logging.debug(
        'edit_id_personal_project. Кликнул на кнопку "Личные проекты"')
    ptk_main_page.create_new_personal_project()
    logging.debug(
        'edit_id_personal_project. Кликнул на кнопку "Новый проект +"')
    ptk_project_page = SearchHelperProjectPage(browser_safari_bs_max_resolution)
    ptk_project_page.id_personal_project()
    logging.debug(
        'edit_id_personal_project. Указал идентификатор проекта')
    ptk_project_page.name_personal_project()
    logging.debug(
        'edit_id_personal_project. Указал название проекта')
    ptk_project_page.personal_project_dropdown_filial()
    logging.debug(
        'edit_id_personal_project. Кликнул на выпадающее меню филиала')
    ptk_project_page.select_filial_north_west()
    logging.debug(
        'edit_id_personal_project. Выбрал северо-западный МЭС')
    ptk_project_page.find_selected_filial()
    logging.debug(
        'edit_id_personal_project. Убедился, что филиал выбран')
    ptk_project_page.project_card_save_button()
    logging.debug(
        'edit_id_personal_project. Кликнул на кнопку "Сохранить"')
    ptk_project_page.project_created_notification()
    logging.debug(
        'edit_id_personal_project. Получил уведомление о том, что проект создан')
    ptk_project_page.project_edit_icon()
    logging.debug(
        'edit_id_personal_project. Кликнул на кнопку редактирования')
    ptk_project_page.free_id_personal_project('Случайный идентификатор')
    logging.debug(
        'edit_id_personal_project. Ввел идентификатор')
    ptk_project_page.project_card_save_button()
    logging.debug(
        'edit_id_personal_project. Кликнул на кнопку "Сохранить"')
    ptk_project_page.project_created_notification()
    logging.debug(
        'edit_id_personal_project. Получил уведомление о том, что проект создан')
    ptk_project_page.assert_free_id_personal_project()
    logging.debug(
        'edit_id_personal_project. Убедился, что идентификатор совпадает измененному')