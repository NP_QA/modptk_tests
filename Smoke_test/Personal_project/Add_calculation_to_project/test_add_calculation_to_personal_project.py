import logging

import allure
import pytest

from Login_Page import SearchHelper
from Main_Page import SearchHelperMainPage
from Project_Page import SearchHelperProjectPage


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Chrome. Добавление расчета в личный проект.')
@allure.story('Добавление расчета в личный проект')
@allure.severity('Critical')
def test_add_calculation_to_personal_project_chrome(browser_chrome_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('add_calculation_to_personal_project.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_chrome_bs)
    ptk_login_page.go_to_login()
    logging.debug('add_calculation_to_personal_project. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('add_calculation_to_personal_project. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('add_calculation_to_personal_project. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('add_calculation_to_personal_project. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'add_calculation_to_personal_project. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_chrome_bs)
    ptk_main_page.click_on_personal_projects()
    logging.debug(
        'add_calculation_to_personal_project. Кликнул на кнопку "Личные проекты"')
    ptk_main_page.create_new_personal_project()
    logging.debug(
        'add_calculation_to_personal_project. Кликнул на кнопку "Новый проект +"')
    ptk_project_page = SearchHelperProjectPage(browser_chrome_bs)
    ptk_project_page.id_personal_project()
    logging.debug(
        'add_calculation_to_personal_project. Указал идентификатор проекта')
    ptk_project_page.name_personal_project()
    logging.debug(
        'add_calculation_to_personal_project. Указал название проекта')
    ptk_project_page.personal_project_dropdown_filial()
    logging.debug(
        'add_calculation_to_personal_project. Кликнул на выпадающее меню филиала')
    ptk_project_page.select_filial_north_west()
    logging.debug(
        'add_calculation_to_personal_project. Выбрал северо-западный МЭС')
    ptk_project_page.find_selected_filial()
    logging.debug(
        'add_calculation_to_personal_project. Убедился, что филиал выбран')
    ptk_project_page.project_card_save_button()
    logging.debug(
        'add_calculation_to_personal_project. Кликнул на кнопку "Сохранить"')
    ptk_project_page.project_created_notification()
    logging.debug(
        'add_calculation_to_personal_project. Получил уведомление о том, что проект создан')
    ptk_project_page.click_on_calculation()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Рассчитать"')
    ptk_project_page.project_card_click_on_plus()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Плюс"')
    ptk_project_page.project_card_select_table_1()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "таблицу 1"')
    ptk_project_page.project_card_select_b_1()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Б1"')
    ptk_project_page.project_card_select_product()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на продукт')
    ptk_project_page.project_card_fill_amount()
    logging.debug(
        'rascenka_unc_table_1.Заполнил количество продукта')
    ptk_project_page.save_unc_button()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Сохранить"')
    ptk_project_page.project_card_assert_value_amount()
    logging.debug(
        'rascenka_unc_table_1.Убедился что количество продукта соответствует введенному')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Firefox. Добавление расчета в личный проект.')
@allure.story('Добавление расчета в личный проект')
@allure.severity('Critical')
def test_add_calculation_to_personal_project_firefox(browser_firefox_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('add_calculation_to_personal_project.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_firefox_bs)
    ptk_login_page.go_to_login()
    logging.debug('add_calculation_to_personal_project. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('add_calculation_to_personal_project. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('add_calculation_to_personal_project. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('add_calculation_to_personal_project. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'add_calculation_to_personal_project. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_firefox_bs)
    ptk_main_page.click_on_personal_projects()
    logging.debug(
        'add_calculation_to_personal_project. Кликнул на кнопку "Личные проекты"')
    ptk_main_page.create_new_personal_project()
    logging.debug(
        'add_calculation_to_personal_project. Кликнул на кнопку "Новый проект +"')
    ptk_project_page = SearchHelperProjectPage(browser_firefox_bs)
    ptk_project_page.id_personal_project()
    logging.debug(
        'add_calculation_to_personal_project. Указал идентификатор проекта')
    ptk_project_page.name_personal_project()
    logging.debug(
        'add_calculation_to_personal_project. Указал название проекта')
    ptk_project_page.personal_project_dropdown_filial()
    logging.debug(
        'add_calculation_to_personal_project. Кликнул на выпадающее меню филиала')
    ptk_project_page.select_filial_north_west()
    logging.debug(
        'add_calculation_to_personal_project. Выбрал северо-западный МЭС')
    ptk_project_page.find_selected_filial()
    logging.debug(
        'add_calculation_to_personal_project. Убедился, что филиал выбран')
    ptk_project_page.project_card_save_button()
    logging.debug(
        'add_calculation_to_personal_project. Кликнул на кнопку "Сохранить"')
    ptk_project_page.project_created_notification()
    logging.debug(
        'add_calculation_to_personal_project. Получил уведомление о том, что проект создан')
    ptk_project_page.click_on_calculation()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Рассчитать"')
    ptk_project_page.project_card_click_on_plus()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Плюс"')
    ptk_project_page.project_card_select_table_1()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "таблицу 1"')
    ptk_project_page.project_card_select_b_1()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Б1"')
    ptk_project_page.project_card_select_product()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на продукт')
    ptk_project_page.project_card_fill_amount()
    logging.debug(
        'rascenka_unc_table_1.Заполнил количество продукта')
    ptk_project_page.save_unc_button()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Сохранить"')
    ptk_project_page.project_card_assert_value_amount()
    logging.debug(
        'rascenka_unc_table_1.Убедился что количество продукта соответствует введенному')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Edge. Добавление расчета в личный проект.')
@allure.story('Добавление расчета в личный проект')
@allure.severity('Critical')
def test_add_calculation_to_personal_project_edge(browser_edge_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('add_calculation_to_personal_project.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_edge_bs)
    ptk_login_page.go_to_login()
    logging.debug('add_calculation_to_personal_project. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('add_calculation_to_personal_project. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('add_calculation_to_personal_project. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('add_calculation_to_personal_project. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'add_calculation_to_personal_project. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_edge_bs)
    ptk_main_page.click_on_personal_projects()
    logging.debug(
        'add_calculation_to_personal_project. Кликнул на кнопку "Личные проекты"')
    ptk_main_page.create_new_personal_project()
    logging.debug(
        'add_calculation_to_personal_project. Кликнул на кнопку "Новый проект +"')
    ptk_project_page = SearchHelperProjectPage(browser_edge_bs)
    ptk_project_page.id_personal_project()
    logging.debug(
        'add_calculation_to_personal_project. Указал идентификатор проекта')
    ptk_project_page.name_personal_project()
    logging.debug(
        'add_calculation_to_personal_project. Указал название проекта')
    ptk_project_page.personal_project_dropdown_filial()
    logging.debug(
        'add_calculation_to_personal_project. Кликнул на выпадающее меню филиала')
    ptk_project_page.select_filial_north_west()
    logging.debug(
        'add_calculation_to_personal_project. Выбрал северо-западный МЭС')
    ptk_project_page.find_selected_filial()
    logging.debug(
        'add_calculation_to_personal_project. Убедился, что филиал выбран')
    ptk_project_page.project_card_save_button()
    logging.debug(
        'add_calculation_to_personal_project. Кликнул на кнопку "Сохранить"')
    ptk_project_page.project_created_notification()
    logging.debug(
        'add_calculation_to_personal_project. Получил уведомление о том, что проект создан')
    ptk_project_page.click_on_calculation()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Рассчитать"')
    ptk_project_page.project_card_click_on_plus()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Плюс"')
    ptk_project_page.project_card_select_table_1()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "таблицу 1"')
    ptk_project_page.project_card_select_b_1()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Б1"')
    ptk_project_page.project_card_select_product()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на продукт')
    ptk_project_page.project_card_fill_amount()
    logging.debug(
        'rascenka_unc_table_1.Заполнил количество продукта')
    ptk_project_page.save_unc_button()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Сохранить"')
    ptk_project_page.project_card_assert_value_amount()
    logging.debug(
        'rascenka_unc_table_1.Убедился что количество продукта соответствует введенному')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Safari. Добавление расчета в личный проект.')
@allure.story('Добавление расчета в личный проект')
@allure.severity('Critical')
def test_add_calculation_to_personal_project_safari(browser_safari_bs):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('add_calculation_to_personal_project.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_safari_bs)
    ptk_login_page.go_to_login()
    logging.debug('add_calculation_to_personal_project. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('add_calculation_to_personal_project. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('add_calculation_to_personal_project. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('add_calculation_to_personal_project. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'add_calculation_to_personal_project. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_safari_bs)
    ptk_main_page.click_on_personal_projects()
    logging.debug(
        'add_calculation_to_personal_project. Кликнул на кнопку "Личные проекты"')
    ptk_main_page.create_new_personal_project()
    logging.debug(
        'add_calculation_to_personal_project. Кликнул на кнопку "Новый проект +"')
    ptk_project_page = SearchHelperProjectPage(browser_safari_bs)
    ptk_project_page.id_personal_project()
    logging.debug(
        'add_calculation_to_personal_project. Указал идентификатор проекта')
    ptk_project_page.name_personal_project()
    logging.debug(
        'add_calculation_to_personal_project. Указал название проекта')
    ptk_project_page.personal_project_dropdown_filial()
    logging.debug(
        'add_calculation_to_personal_project. Кликнул на выпадающее меню филиала')
    ptk_project_page.select_filial_north_west()
    logging.debug(
        'add_calculation_to_personal_project. Выбрал северо-западный МЭС')
    ptk_project_page.find_selected_filial()
    logging.debug(
        'add_calculation_to_personal_project. Убедился, что филиал выбран')
    ptk_project_page.project_card_save_button()
    logging.debug(
        'add_calculation_to_personal_project. Кликнул на кнопку "Сохранить"')
    ptk_project_page.project_created_notification()
    logging.debug(
        'add_calculation_to_personal_project. Получил уведомление о том, что проект создан')
    ptk_project_page.click_on_calculation()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Рассчитать"')
    ptk_project_page.project_card_click_on_plus()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Плюс"')
    ptk_project_page.project_card_select_table_1()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "таблицу 1"')
    ptk_project_page.project_card_select_b_1()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Б1"')
    ptk_project_page.project_card_select_product()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на продукт')
    ptk_project_page.project_card_fill_amount()
    logging.debug(
        'rascenka_unc_table_1.Заполнил количество продукта')
    ptk_project_page.save_unc_button()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Сохранить"')
    ptk_project_page.project_card_assert_value_amount()
    logging.debug(
        'rascenka_unc_table_1.Убедился что количество продукта соответствует введенному')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Chrome, разрешение 1280х1024. Добавление расчета в личный проект.')
@allure.story('Добавление расчета в личный проект')
@allure.severity('Critical')
def test_add_calculation_to_personal_project_chrome_max_resolution(browser_chrome_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('add_calculation_to_personal_project.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_chrome_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('add_calculation_to_personal_project. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('add_calculation_to_personal_project. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('add_calculation_to_personal_project. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('add_calculation_to_personal_project. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'add_calculation_to_personal_project. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_chrome_bs_max_resolution)
    ptk_main_page.click_on_personal_projects()
    logging.debug(
        'add_calculation_to_personal_project. Кликнул на кнопку "Личные проекты"')
    ptk_main_page.create_new_personal_project()
    logging.debug(
        'add_calculation_to_personal_project. Кликнул на кнопку "Новый проект +"')
    ptk_project_page = SearchHelperProjectPage(browser_chrome_bs_max_resolution)
    ptk_project_page.id_personal_project()
    logging.debug(
        'add_calculation_to_personal_project. Указал идентификатор проекта')
    ptk_project_page.name_personal_project()
    logging.debug(
        'add_calculation_to_personal_project. Указал название проекта')
    ptk_project_page.personal_project_dropdown_filial()
    logging.debug(
        'add_calculation_to_personal_project. Кликнул на выпадающее меню филиала')
    ptk_project_page.select_filial_north_west()
    logging.debug(
        'add_calculation_to_personal_project. Выбрал северо-западный МЭС')
    ptk_project_page.find_selected_filial()
    logging.debug(
        'add_calculation_to_personal_project. Убедился, что филиал выбран')
    ptk_project_page.project_card_save_button()
    logging.debug(
        'add_calculation_to_personal_project. Кликнул на кнопку "Сохранить"')
    ptk_project_page.project_created_notification()
    logging.debug(
        'add_calculation_to_personal_project. Получил уведомление о том, что проект создан')
    ptk_project_page.click_on_calculation()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Рассчитать"')
    ptk_project_page.project_card_click_on_plus()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Плюс"')
    ptk_project_page.project_card_select_table_1()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "таблицу 1"')
    ptk_project_page.project_card_select_b_1()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Б1"')
    ptk_project_page.project_card_select_product()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на продукт')
    ptk_project_page.project_card_fill_amount()
    logging.debug(
        'rascenka_unc_table_1.Заполнил количество продукта')
    ptk_project_page.save_unc_button()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Сохранить"')
    ptk_project_page.project_card_assert_value_amount()
    logging.debug(
        'rascenka_unc_table_1.Убедился что количество продукта соответствует введенному')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Firefox, разрешение 1280х1024. Добавление расчета в личный проект.')
@allure.story('Добавление расчета в личный проект')
@allure.severity('Critical')
def test_add_calculation_to_personal_project_firefox_max_resolution(browser_firefox_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('add_calculation_to_personal_project.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_firefox_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('add_calculation_to_personal_project. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('add_calculation_to_personal_project. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('add_calculation_to_personal_project. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('add_calculation_to_personal_project. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'add_calculation_to_personal_project. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_firefox_bs_max_resolution)
    ptk_main_page.click_on_personal_projects()
    logging.debug(
        'add_calculation_to_personal_project. Кликнул на кнопку "Личные проекты"')
    ptk_main_page.create_new_personal_project()
    logging.debug(
        'add_calculation_to_personal_project. Кликнул на кнопку "Новый проект +"')
    ptk_project_page = SearchHelperProjectPage(browser_firefox_bs_max_resolution)
    ptk_project_page.id_personal_project()
    logging.debug(
        'add_calculation_to_personal_project. Указал идентификатор проекта')
    ptk_project_page.name_personal_project()
    logging.debug(
        'add_calculation_to_personal_project. Указал название проекта')
    ptk_project_page.personal_project_dropdown_filial()
    logging.debug(
        'add_calculation_to_personal_project. Кликнул на выпадающее меню филиала')
    ptk_project_page.select_filial_north_west()
    logging.debug(
        'add_calculation_to_personal_project. Выбрал северо-западный МЭС')
    ptk_project_page.find_selected_filial()
    logging.debug(
        'add_calculation_to_personal_project. Убедился, что филиал выбран')
    ptk_project_page.project_card_save_button()
    logging.debug(
        'add_calculation_to_personal_project. Кликнул на кнопку "Сохранить"')
    ptk_project_page.project_created_notification()
    logging.debug(
        'add_calculation_to_personal_project. Получил уведомление о том, что проект создан')
    ptk_project_page.click_on_calculation()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Рассчитать"')
    ptk_project_page.project_card_click_on_plus()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Плюс"')
    ptk_project_page.project_card_select_table_1()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "таблицу 1"')
    ptk_project_page.project_card_select_b_1()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Б1"')
    ptk_project_page.project_card_select_product()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на продукт')
    ptk_project_page.project_card_fill_amount()
    logging.debug(
        'rascenka_unc_table_1.Заполнил количество продукта')
    ptk_project_page.save_unc_button()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Сохранить"')
    ptk_project_page.project_card_assert_value_amount()
    logging.debug(
        'rascenka_unc_table_1.Убедился что количество продукта соответствует введенному')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Edge, разрешение 1280х1024. Добавление расчета в личный проект.')
@allure.story('Добавление расчета в личный проект')
@allure.severity('Critical')
def test_add_calculation_to_personal_project_edge_max_resolution(browser_edge_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('add_calculation_to_personal_project.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_edge_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('add_calculation_to_personal_project. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('add_calculation_to_personal_project. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('add_calculation_to_personal_project. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('add_calculation_to_personal_project. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'add_calculation_to_personal_project. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_edge_bs_max_resolution)
    ptk_main_page.click_on_personal_projects()
    logging.debug(
        'add_calculation_to_personal_project. Кликнул на кнопку "Личные проекты"')
    ptk_main_page.create_new_personal_project()
    logging.debug(
        'add_calculation_to_personal_project. Кликнул на кнопку "Новый проект +"')
    ptk_project_page = SearchHelperProjectPage(browser_edge_bs_max_resolution)
    ptk_project_page.id_personal_project()
    logging.debug(
        'add_calculation_to_personal_project. Указал идентификатор проекта')
    ptk_project_page.name_personal_project()
    logging.debug(
        'add_calculation_to_personal_project. Указал название проекта')
    ptk_project_page.personal_project_dropdown_filial()
    logging.debug(
        'add_calculation_to_personal_project. Кликнул на выпадающее меню филиала')
    ptk_project_page.select_filial_north_west()
    logging.debug(
        'add_calculation_to_personal_project. Выбрал северо-западный МЭС')
    ptk_project_page.find_selected_filial()
    logging.debug(
        'add_calculation_to_personal_project. Убедился, что филиал выбран')
    ptk_project_page.project_card_save_button()
    logging.debug(
        'add_calculation_to_personal_project. Кликнул на кнопку "Сохранить"')
    ptk_project_page.project_created_notification()
    logging.debug(
        'add_calculation_to_personal_project. Получил уведомление о том, что проект создан')
    ptk_project_page.click_on_calculation()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Рассчитать"')
    ptk_project_page.project_card_click_on_plus()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Плюс"')
    ptk_project_page.project_card_select_table_1()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "таблицу 1"')
    ptk_project_page.project_card_select_b_1()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Б1"')
    ptk_project_page.project_card_select_product()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на продукт')
    ptk_project_page.project_card_fill_amount()
    logging.debug(
        'rascenka_unc_table_1.Заполнил количество продукта')
    ptk_project_page.save_unc_button()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Сохранить"')
    ptk_project_page.project_card_assert_value_amount()
    logging.debug(
        'rascenka_unc_table_1.Убедился что количество продукта соответствует введенному')


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Safari, разрешение 1280х1024. Добавление расчета в личный проект.')
@allure.story('Добавление расчета в личный проект')
@allure.severity('Critical')
def test_add_calculation_to_personal_project_safari_max_resolution(browser_safari_bs_max_resolution):
    logger = logging.getLogger('modPTK_logger')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('add_calculation_to_personal_project.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    ptk_login_page = SearchHelper(browser_safari_bs_max_resolution)
    ptk_login_page.go_to_login()
    logging.debug('add_calculation_to_personal_project. Перешел на страницу логина')
    ptk_login_page.fill_admin_login()
    logging.debug('add_calculation_to_personal_project. Заполнил логин')
    ptk_login_page.fill_admin_password()
    logging.debug('add_calculation_to_personal_project. Заполнил пароль')
    ptk_login_page.click_login_button()
    logging.debug('add_calculation_to_personal_project. Кликнул кнопку "Войти"')
    ptk_login_page.authorization_assert()
    logging.debug(
        'add_calculation_to_personal_project. Убедился в успешной авторизации, найдя элемент "Корректировка ИПР"')
    ptk_main_page = SearchHelperMainPage(browser_safari_bs_max_resolution)
    ptk_main_page.click_on_personal_projects()
    logging.debug(
        'add_calculation_to_personal_project. Кликнул на кнопку "Личные проекты"')
    ptk_main_page.create_new_personal_project()
    logging.debug(
        'add_calculation_to_personal_project. Кликнул на кнопку "Новый проект +"')
    ptk_project_page = SearchHelperProjectPage(browser_safari_bs_max_resolution)
    ptk_project_page.id_personal_project()
    logging.debug(
        'add_calculation_to_personal_project. Указал идентификатор проекта')
    ptk_project_page.name_personal_project()
    logging.debug(
        'add_calculation_to_personal_project. Указал название проекта')
    ptk_project_page.personal_project_dropdown_filial()
    logging.debug(
        'add_calculation_to_personal_project. Кликнул на выпадающее меню филиала')
    ptk_project_page.select_filial_north_west()
    logging.debug(
        'add_calculation_to_personal_project. Выбрал северо-западный МЭС')
    ptk_project_page.find_selected_filial()
    logging.debug(
        'add_calculation_to_personal_project. Убедился, что филиал выбран')
    ptk_project_page.project_card_save_button()
    logging.debug(
        'add_calculation_to_personal_project. Кликнул на кнопку "Сохранить"')
    ptk_project_page.project_created_notification()
    logging.debug(
        'add_calculation_to_personal_project. Получил уведомление о том, что проект создан')
    ptk_project_page.click_on_calculation()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Рассчитать"')
    ptk_project_page.project_card_click_on_plus()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Плюс"')
    ptk_project_page.project_card_select_table_1()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "таблицу 1"')
    ptk_project_page.project_card_select_b_1()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Б1"')
    ptk_project_page.project_card_select_product()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на продукт')
    ptk_project_page.project_card_fill_amount()
    logging.debug(
        'rascenka_unc_table_1.Заполнил количество продукта')
    ptk_project_page.save_unc_button()
    logging.debug(
        'rascenka_unc_table_1.Кликнул на "Сохранить"')
    ptk_project_page.project_card_assert_value_amount()
    logging.debug(
        'rascenka_unc_table_1.Убедился что количество продукта соответствует введенному')
