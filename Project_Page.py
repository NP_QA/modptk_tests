import itertools
import logging
import random
import time

import allure
from allure_commons.types import AttachmentType
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

from BaseApp import BasePage


class ProjectPageLocators:
    # locators_for_project_page_module
    LOCATOR_PTK_CLICK_ON_RENAME_BUTTON = (By.XPATH, '//*[contains(text(), "Переименовать")]')
    LOCATOR_PTK_HOVER_RENAMED = (By.XPATH, '/html/body/div[2]/div[3]/div/div/div[2]/div[1]/li/div[1]')
    LOCATOR_PTK_INPUT_RENAME = (By.XPATH, '/html/body/div[2]/div[3]/div/div/div[2]/div[1]/li/div[1]/div/input')
    LOCATOR_PTK_FIND_RENAME_ALERT = (By.XPATH, '//*[@role="alert"]')
    LOCATOR_PTK_SHOW_BUTTON = (By.XPATH, '//button[contains(text(), "Показать полностью")]')
    LOCATOR_PTK_FIND_PROJECT_BY_3_LAST_NUMBERS = (By.XPATH, '//*[@id="root"]/div/div[2]/div[2]/a[1]/div[1]/div[1]/p')
    LOCATOR_PTK_PROJECT_DESCRIPTION_TEXT = (By.XPATH,
                                            '//*[contains(text(), "Тестовое описание кнопки показать полностью. ")]')
    LOCATOR_PTK_PROJECT_DESCRIPTION_TEXT_PART_2 = (By.XPATH,
                                                   '//*[contains(text(), "Тестовое описание кнопки показать '
                                                   'полностью. Тестовое описание кнопки показать полностью. Тестовое '
                                                   'описание кнопки показать полностью. Тестовое описание кнопки '
                                                   'показать полностью. Тестовое описание кнопки показать полностью. '
                                                   'Тестовое описание кнопки показать полностью. Тестовое описание '
                                                   'кнопки показать полностью. Тестовое описание кнопки показать '
                                                   'полностью. Тестовое описание кнопки показать полностью.")]')
    LOCATOR_PTK_GO_TO_TEST_PROJECT = 'https://dev.modptk.unc-esk.ru/correction-ipr/project/2060'
    LOCATOR_PTK_GO_TO_PERSONAL_PROJECT = 'https://dev.modptk.unc-esk.ru/personal-projects/project/28'
    LOCATOR_PTK_GO_TO_PERSONAL_PROJECT_2 = 'https://dev.modptk.unc-esk.ru/personal-projects/project/52'
    LOCATOR_PTK_GO_TO_TABLE_OF_PERSONAL_PROJECTS = 'https://dev.modptk.unc-esk.ru/personal-projects'
    LOCATOR_PTK_FIRST_CLICK_DOWNLOAD_XLSX = (
        By.XPATH, '//*[contains(text(), "Форма 20")]')
    LOCATOR_PTK_SECOND_CLICK_DOWNLOAD_XLSX = (By.XPATH, '//button[2]//span[contains(text(), "Выгрузить")]')
    LOCATOR_PTK_SELECT_REGION = (By.XPATH, '//input[@class="MuiInputBase-input MuiFilledInput-input"]')
    LOCATOR_PTK_SELECT_ALTAY_REGION = (By.XPATH, '//ul//div//li[1][contains(text(), "Алтайский край")]')
    LOCATOR_PTK_SELECT_AMUR_REGION = (By.XPATH, '//ul//div//li[2][contains(text(), "Амурская обл.")]')
    LOCATOR_PTK_CHECK_ALTAY_SELECTED = (
        By.XPATH, '//*[contains(text(), "Алтайский край;")]')
    LOCATOR_PTK_THREE_DOTS_ICON = (By.XPATH, '//span[@class="three-dots-icon"]')
    LOCATOR_PTK_CHECK_AMUR_SELECTED = (
        By.XPATH, '//*[contains(text(), "Амурская обл.;")]')
    LOCATOR_PTK_CLICK_ON_BODY = (By.XPATH, '/html/body/div[2]/div[1]')
    LOCATOR_PTK_COMMENTARY = (
        By.XPATH, '//div[3]//div[2]//div/div//input[@class="MuiInputBase-input MuiFilledInput-input"]')
    LOCATOR_PTK_COMMENT = (By.XPATH, '//*[@id="root"]/div[1]/div[2]/div[3]/div[2]/div/div/input')
    LOCATOR_PTK_CLICK_ON_CALCULATION = (
        By.XPATH, '//span[@class="MuiButton-label" and contains(text(), "Рассчитать")]')
    LOCATOR_PTK_CALCULATION_UNC = (
        By.XPATH,
        '//*[contains(text(), "Сумма расценок УНЦ без НДС")]')
    LOCATOR_PTK_CALCULATION_UNC_I_NDS = (By.XPATH,
                                         '//*[contains(text(), "Сумма ненормируемых затрат с НДС")]')
    LOCATOR_PTK_CALCULATION_UNC_PROGNOZ = (By.XPATH,
                                           '//*[contains(text(), "Стоимость по УНЦ в прогнозных ценах")]')
    LOCATOR_PTK_CALCULATION_BLOCK_1 = (By.XPATH, '//div[@class="table__head table__head-fixed"]')
    LOCATOR_PTK_CALCULATION_TITLE = (
        By.XPATH, '//div[@class="title" and contains(text(), "Расчёт стоимости по УНЦ")]')
    LOCATOR_PTK_CANCEL_CALCULATION = (By.XPATH, '//span[@class="close-icon"]')
    LOCATOR_PTK_SUMMA_S_NDS = (
        By.XPATH, '//div[2]/div[1]/div[2]/div/div//input[@class="MuiInputBase-input MuiFilledInput-input"]')
    LOCATOR_PTK_ASSERT_SUMMA_S_NDS = (By.XPATH, '//*[@id="root"]/div[1]/div[3]/div[2]/div[1]/div[2]/span[2]')
    LOCATOR_PTK_EDITED_SUMMA_S_NDS = (
        By.XPATH, '//*[@id="root"]/div[1]/div[3]/div[2]/div[1]/div[2]/div[1]/div/input')
    LOCATOR_PTK_CLICK_ON_PLUS = (By.XPATH, '//*[@class="plus-icon"]')
    LOCATOR_PTK_SELECT_TABLE_1 = (By.CSS_SELECTOR, 'div.unc-pricing-tables__list > div:nth-child(1) > div')
    LOCATOR_PTK_SELECT_B_1 = (By.CSS_SELECTOR,
                              'div.MuiCollapse-container.unc-pricing-item__detail.MuiCollapse-entered > div > div > '
                              'div > div:nth-child(1) > div')
    LOCATOR_PTK_SELECT_PRODUCT = (By.XPATH, '//table/tbody/tr[1]/td[4]')
    LOCATOR_PTK_FILL_AMOUNT = (
        By.XPATH, '//div[3]//div//div[2]//div[1]//div[2]//div[1]//div//input')
    LOCATOR_PTK_ASSERT_VALUE_AMOUNT = (
        By.XPATH, '//*[@id="root"]/div[1]/div[3]/div[3]/div/div/div/div[1]/div[2]/div[2]/div[4]/div/div/input')
    LOCATOR_PTK_SAVE_BUTTON = (
        By.XPATH,
        '//button[@class="MuiButtonBase-root MuiButton-root jss1 MuiButton-contained MuiButton-containedPrimary '
        'MuiButton-disableElevation"]')
    LOCATOR_PTK_SAVE_UNC_BUTTON = (
        By.XPATH, '/html/body/div[2]/div[3]/div/div[3]/div/div[3]/button[2]/span[1]')
    LOCATOR_PTK_PICK_TABLE_2 = (By.XPATH, '/html/body/div[2]/div[3]/div[3]/div/div[2]/div[2]/div[1]/span')
    LOCATOR_PTK_PICK_TABLE_3 = (
        By.XPATH, '/html/body/div[2]/div[3]/div/div[3]/div/div[2]/div[3]/div[1]/span')
    LOCATOR_PTK_FILL_AMOUNT_TABLE_2 = (
        By.CSS_SELECTOR,
        'div.MuiFormControl-root.MuiTextField-root.jss5.\'.changing-unc-table-data__body-count\' > div > input')
    LOCATOR_PTK_FILL_AMOUNT_TABLE_3 = (
        By.XPATH, '/html/body/div[2]/div[3]/div/div[3]/div/div[2]/div[3]/div[2]/div[1]/div/input')
    LOCATOR_PTK_ASSERT_VALUE_AMOUNT_2 = (By.XPATH, '//div[2]/div[4]/div[4]/div/div/input')
    LOCATOR_PTK_CALCULATION_TEST_CARD = 'https://dev.modptk.unc-esk.ru/correction-ipr/project/3843'
    LOCATOR_PTK_DELETE = (
        By.XPATH, '//*[@id="root"]/div[1]/div[3]/div[3]/div/div/div/div[1]/div[2]/div[2]/div[12]/button')
    LOCATOR_PTK_DELETE_2 = (
        By.XPATH, '//*[@id="root"]/div[1]/div[3]/div[3]/div/div/div/div[1]/div[2]/div[4]/div[12]/button')
    LOCATOR_PTK_ASSERT_VALUE_AMOUNT_3 = (
        By.XPATH, '//*[@id="root"]/div[1]/div[3]/div[3]/div/div/div/div[1]/div[2]/div[6]/div[4]/div/div/input')
    LOCATOR_PTK_DELETE_3 = (
        By.XPATH, '//*[@id="root"]/div[1]/div[3]/div[3]/div/div/div/div[1]/div[2]/div[6]/div[12]/button')
    LOCATOR_PTK_PICK_TABLE_4 = (
        By.XPATH, '/html/body/div[2]/div[3]/div/div[3]/div/div[2]/div[4]/div[1]/span')
    LOCATOR_PTK_FILL_AMOUNT_4 = (
        By.XPATH, '/html/body/div[2]/div[3]/div/div[3]/div/div[2]/div[4]/div[2]/div[1]/div/input')
    LOCATOR_PTK_ASSERT_VALUE_AMOUNT_4 = (
        By.XPATH, '//*[@id="root"]/div[1]/div[3]/div[3]/div/div/div/div[1]/div[2]/div[8]/div[4]/div/div/input')
    LOCATOR_PTK_DELETE_4 = (
        By.XPATH, '//*[@id="root"]/div[1]/div[3]/div[3]/div/div/div/div[1]/div[2]/div[8]/div[12]/button')
    LOCATOR_PTK_PICK_TABLE_5 = (
        By.XPATH, '/html/body/div[2]/div[3]/div/div[3]/div/div[2]/div[5]/div[1]/span')
    LOCATOR_PTK_FILL_AMOUNT_5 = (
        By.XPATH, '/html/body/div[2]/div[3]/div/div[3]/div/div[2]/div[5]/div[2]/div[1]/div/input')
    LOCATOR_PTK_ASSERT_VALUE_AMOUNT_5 = (
        By.XPATH, '//*[@id="root"]/div[1]/div[3]/div[3]/div/div/div/div[1]/div[2]/div[10]/div[4]/div/div/input')
    LOCATOR_PTK_DELETE_5 = (
        By.XPATH, '//*[@class="delete-icon"]')
    LOCATOR_PTK_ASSERT_DELETED = (
        By.XPATH, '//div[10][@class="table__body-row table__body-nothing" and contains(text(), "Отсутствует")]')
    LOCATOR_PTK_CLICK_INSTRUCTIONS = (By.XPATH, '//div[2]//div[3]//div[2]//div[1]//button')
    LOCATOR_PTK_FIND_ISTRUCTIONS = (By.XPATH,
                                    '//div[@class="selected-unc-table__instructions visible" and contains(text(), '
                                    '"К таблицам В1, В2. В УНЦ ячейки выключателя НУ 6-750 кВ включено: стоимость '
                                    'оборудования (выключатель (ячейка с выключателем, КРУН) или альтернативное '
                                    'решение (компактный модуль), элементы управления ячейкой, разъединители, '
                                    'трансформаторы тока (далее – ТТ) (в том числе цифровые ТТ), трансформаторы '
                                    'напряжения  (далее – ТН) (в том числе цифровые ТН), ограничители перенапряжений '
                                    '(далее – ОПН), шкафы РЗА, автоматика управления выключателем, блоки управления '
                                    'приводами разъединителей, шкафы наружной установки (обогрева выключателя, '
                                    'питания приводов разъединителей, обогрева приводов разъединителей, '
                                    'зажимов выключателя), приборы учета и измерения электроэнергии КРУН), '
                                    'стоимость строительно-монтажных работ (в том числе демонтаж существующего '
                                    'оборудования) с учетом стоимости используемого материала (устройство '
                                    'фундаментов, опорных стоек и металлоконструкций, порталов, ошиновки, кабельного '
                                    'хозяйства,заземления), а также сопутствующие затраты. К таблицам В1-В5. При '
                                    'отличных от представленных максимальных (минимальных) характеристик типовых '
                                    'технологических решений (номинальный ток, номинальный ток отключения)УНЦ '
                                    'принимается равным максимальному (минимальному) значению УНЦ соответствующего '
                                    'напряжения технологического решения, указанного в таблице.")]')
    LOCATOR_PTK_ADD_COMMENT = (By.XPATH, '//input[@class="MuiInputBase-input MuiInput-input"]')

    LOCATOR_PTK_CLICK_ARCHIVE_FORM_20 = (By.XPATH, '//span[contains(text(), "Архив форм 20")]')
    LOCATOR_PTK_ARCHIVE_FORM_20_SEARCHING_BY_ID = (By.XPATH, '//input[@class="IdsInserter_inserterInput__37LaO"]')
    LOCATOR_PTK_DOWNLOAD_BUTTON = (By.XPATH, '//*[contains(text(), "Выгрузить")]')
    LOCATOR_PTK_CLICK_EXCEL_FORM = (By.XPATH, '//span[contains(text(), "Excel")]')
    LOCATOR_PTK_BACK_TO_MAIN_PAGE_ARROW = (By.XPATH, '//span[@class="arrow-left-icon"]')
    LOCATOR_PTK_REGION_DISABLED = (By.XPATH, '//*[@id="root"]/div[1]/div[2]/div[3]/div[1]/div/div/input[@disabled]')
    LOCATOR_PTK_COMMENT_DISABLED = (By.XPATH, '//*[@id="root"]/div[1]/div[2]/div[3]/div[2]/div/div/input[@disabled]')
    LOCATOR_PTK_NDS_DISABLED = (
        By.XPATH, '//*[@id="root"]/div[1]/div[3]/div[2]/div[1]/div[2]/div[1]/div/input[@disabled]')
    LOCATOR_PTK_INFO_BLOCK = (By.XPATH, '//*[@class="info-block__value"]')
    LOCATOR_PTK_CLICK_REGION_SELECTOR = (By.XPATH, '//div[4]/div/div/input')
    LOCATOR_PTK_CLICK_REGION_SELECTOR_AFTER_SAVE = (By.XPATH, '//div[4]//div//div//input')
    LOCATOR_PTK_COMMENT_ICON = (By.XPATH, '//*[@class="comment-icon"]')
    LOCATOR_PTK_EDIT_COMMENT = (By.XPATH, '//*[contains(text(), "Редактировать")]')
    LOCATOR_PTK_COMMENT_TEXTAREA = (By.XPATH, '//div[2]/div[2]/div/textarea')
    LOCATOR_PTK_SAVE_COMMENT_BUTTON = (By.XPATH, '//*[contains(text(), "Сохранить")]')
    LOCATOR_PTK_VERIFY_PROJECT = (By.XPATH, '//*[contains(text(), "Проверить проект")]')
    LOCATOR_PTK_CLICK_VERIFY_PROJECT = (
        By.XPATH, '//span[@class="MuiButton-label" and contains(text(), "Проверить")]')
    LOCATOR_PTK_PREPARATION_OF_THE_TERRITORY = (By.XPATH, '//div/label[6]/span[1]')
    LOCATOR_PTK_BIFURCATIONS_WITH_CELLS = (By.XPATH, '//div/label[5]/span[1]')
    LOCATOR_PTK_OTHER_BURIFICATION = (By.XPATH, '//div/label[4]/span[1]')
    LOCATOR_PTK_VERIFY_TABLE = (
        By.XPATH, '//div[3]/div/div/div[2]/div[1]/span[2][contains(text(), "Ошибки:")]')
    LOCATOR_PTK_COMMENT_CALCULATION = (By.XPATH, '//div[2]/div[10]/div/div/input')
    LOCATOR_PTK_ASSERT_PROJECT_ID_IN_CARD = (By.XPATH, '//*[@id="root"]/div[1]/div[1]/div/div[1]/div')
    LOCATOR_PTK_RASSCHET_BUTTON = (By.XPATH, '//*[contains(text(), "Рассчитать")]')
    LOCATOR_PTK_EDIT_ICON = (By.XPATH, '//span[@class="edit-icon"]')
    LOCATOR_PTK_SELECT_NON_ACTUAL_VERSION = (
        By.CSS_SELECTOR, 'div.Versioning_versioning__list__20L62 > div:nth-child(2) > li')
    LOCATOR_PTK_VERSION_HOVER = (By.XPATH, '/html/body/div[2]/div[3]/div/div/div[2]/div[1]')
    LOCATOR_PTK_EDIT_VERSION = (By.XPATH, '//*[@class="Versioning_versioning__item-select__1l4_h select-version-icon"]')
    LOCATOR_PTK_SETTINGS_BUTTON_TEXT = (By.XPATH, '//*[contains(text(), "Доп. функции")]')
    LOCATOR_PTK_FIRST_CLICK_DOWNLOAD_TECH_PARAMS = (By.XPATH, '//*[contains(text(), "Тех. параметры")]')
    LOCATOR_PTK_FIRST_CLICK_DOWNLOAD_SUMMARY_TABLE = (By.XPATH, '//*[contains(text(), "Сводная таблица")]')
    LOCATOR_PTK_FIRST_CLICK_DOWNLOAD_UNC_OA = (By.XPATH, '//*[contains(text(), "УНЦ ОА")]')
    LOCATOR_PTK_FIRST_CLICK_DOWNLOAD_SD = (By.XPATH, '//*[contains(text(), "СД")]')
    LOCATOR_PTK_FIRST_CLICK_DOWNLOAD_EQUIPMENT = (By.XPATH, '//*[contains(text(), "Оборудование")]')
    LOCATOR_PTK_CALCULATION_TABLE_1_CLEAR = (By.XPATH, '//div[2]/div[1]/div[2][@class="table__collapse-total title"]')
    LOCATOR_PTK_CALCULATION_TABLE_2_CLEAR = (By.XPATH, '//div[3]/div[2][@class="table__collapse-total title"]')
    LOCATOR_PTK_CALCULATION_TABLE_3_CLEAR = (By.XPATH, '//div[5]/div[2][@class="table__collapse-total title"]')
    LOCATOR_PTK_CALCULATION_TABLE_4_CLEAR = (By.XPATH, '//div[7]/div[2][@class="table__collapse-total title"]')
    LOCATOR_PTK_CALCULATION_TABLE_5_CLEAR = (By.XPATH, '//div[9]/div[2][@class="table__collapse-total title"]')
    LOCATOR_PTK_PERSONAL_PROJECT_SAVE_BUTTON_DISABLED = (
        By.XPATH, '//button[@disabled]//span[contains(text(), "Сохранить")]')
    LOCATOR_PTK_FILL_NAME_PERSONAL_PROJECT = (
        By.XPATH, '//div[2]/div/input[@class="MuiInputBase-input MuiFilledInput-input"]')
    LOCATOR_PTK_FILL_ID_PERSONAL_PROJECT = (By.XPATH, "//div[1]/div/div/input")
    LOCATOR_PTK_FILL_FILIAL_PERSONAL_PROJECT = (By.XPATH, '//div[3]/div/div/input')
    LOCATOR_PTK_SELECT_FILIAL_NORTH_WEST = (By.XPATH, '//li[2]/span[1]')
    LOCATOR_PTK_NORTH_WEST_FILIAL_SELECTED = (By.XPATH, '//*[@value="МЭС Северо-Запада"]')
    LOCATOR_PTK_PROJECT_CREATED_NOTIFICATION = (By.XPATH,
                                                '//span[contains(text(), "Создатель проекта")]')
    LOCATOR_PTK_SELECT_FILIAL_URAL = (By.XPATH, '//div/li[3]')
    LOCATOR_PTK_PROJECT_NAME = (By.XPATH, '//*[@class="MuiCollapse-wrapperInner"]')
    LOCATOR_PTK_PROJECT_ID = (By.XPATH, '//div[1]/div/div[1]/span[2]')
    LOCATOR_PTK_URAL_FILIAL_SELECTED = (By.XPATH, '//*[@value="МЭС Урала"]')
    LOCATOR_PTK_SEARCHING_FIELD = (By.XPATH, '//*[@placeholder="Поиск..."]')
    LOCATOR_PTK_TABLE_PROJECT_NAME = (By.XPATH, '//div[1]/div[2]/p')
    LOCATOR_PTK_TABLE_PROJECT_ID = (By.XPATH, '//div[1]/div[1]/p')
    LOCATOR_PTK_YEAR_AND_QUARTER = (By.XPATH, '//div[3]/div[1]/div[2]/div/div/div/input')
    LOCATOR_PTK_OBJECT_ANALOGUE = (By.XPATH, '//div[4]/div/div/div/div[2]/div/div/div/input')
    LOCATOR_PTK_PSD_OBJECT_ANALOGUE = (By.XPATH, '//div[3]/div/div/div/input')
    LOCATOR_PTK_CALCULATE_FULL_COST = (By.XPATH, '//div[4]/div[1]/button')
    LOCATOR_PTK_CALCULATE_PIR = (By.XPATH, '//div[5]/div[1]/div/div/input')
    LOCATOR_PTK_CALCULATE_SMR = (By.XPATH, '//div[5]/div[2]/div/div/input')
    LOCATOR_PTK_CALCULATE_EQUIPMENTS = (By.XPATH, '//div[5]/div[3]/div/div/input')
    LOCATOR_PTK_CALCULATE_OTHER = (By.XPATH, '//div[5]/div[4]/div/div/input')

class SearchHelperProjectPage(BasePage):
    # locators_for_project_page_functions
    def go_to_table_of_personal_projects(self):
        try:
            self.driver.get(ProjectPageLocators.LOCATOR_PTK_GO_TO_TABLE_OF_PERSONAL_PROJECTS)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Переход на страницу личных проектов. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Переход на страницу личных проектов. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Переход на страницу личных проектов. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Переход на страницу личных проектов. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def select_year_and_quarter(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_YEAR_AND_QUARTER)
            self.find_clickable_element(ProjectPageLocators.LOCATOR_PTK_YEAR_AND_QUARTER)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Выбор года и квартала. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбор года и квартала. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Выбор года и квартала. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбор года и квартала. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def send_keys_other(self, integer):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_CALCULATE_OTHER)
            self.find_element(ProjectPageLocators.LOCATOR_PTK_CALCULATE_OTHER).send_keys(integer)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Передал значение в поле "Прочие". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Передал значение в поле "Прочие". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Передал значение в поле "Прочие". Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Передал значение в поле "Прочие". Успешный.',
                              attachment_type=AttachmentType.PNG)

    def send_keys_smr(self, integer):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_CALCULATE_SMR)
            self.find_element(ProjectPageLocators.LOCATOR_PTK_CALCULATE_SMR).send_keys(integer)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Передал значение в поле "СМР". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Передал значение в поле "СМР". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Передал значение в поле "СМР". Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Передал значение в поле "СМР". Успешный.',
                              attachment_type=AttachmentType.PNG)

    def send_keys_equipments(self, integer):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_CALCULATE_EQUIPMENTS)
            self.find_element(ProjectPageLocators.LOCATOR_PTK_CALCULATE_EQUIPMENTS).send_keys(integer)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Передал значение в поле "Оборудование". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Передал значение в поле "Оборудование". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Передал значение в поле "Оборудование". Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Передал значение в поле "Оборудование". Успешный.',
                              attachment_type=AttachmentType.PNG)

    def send_keys_pir(self, integer):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_CALCULATE_PIR)
            self.find_element(ProjectPageLocators.LOCATOR_PTK_CALCULATE_PIR).send_keys(integer)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Передал значение в поле "ПИР". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Передал значение в поле "ПИР". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Передал значение в поле "ПИР". Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Передал значение в поле "ПИР". Успешный.',
                              attachment_type=AttachmentType.PNG)

    def click_full_cost(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_CALCULATE_FULL_COST)
            self.find_clickable_element(ProjectPageLocators.LOCATOR_PTK_CALCULATE_FULL_COST)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Клик на кнопку "Рассчитать" блока РПС. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на кнопку "Рассчитать" блока РПС. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Клик на кнопку "Рассчитать" блока РПС. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на кнопку "Рассчитать" блока РПС. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def send_keys_object_analogue(self, integer):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_OBJECT_ANALOGUE)
            self.find_element(ProjectPageLocators.LOCATOR_PTK_OBJECT_ANALOGUE).send_keys(integer)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Передал значение в поле Наименование объекта-аналога. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Передал значение в поле Наименование объекта-аналога. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Передал значение в поле Наименование объекта-аналога. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Передал значение в поле Наименование объекта-аналога. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def send_keys_psd_object_analogue(self, integer):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_PSD_OBJECT_ANALOGUE)
            self.find_element(ProjectPageLocators.LOCATOR_PTK_PSD_OBJECT_ANALOGUE).send_keys(integer)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Передал значение в поле ПСД объекта-аналога. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Передал значение в поле ПСД объекта-аналога. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Передал значение в поле ПСД объекта-аналога. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Передал значение в поле ПСД объекта-аналога. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def find_random_id_project(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_SEARCHING_FIELD)
            self.find_element(ProjectPageLocators.LOCATOR_PTK_SEARCHING_FIELD).send_keys(random_project_id)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Ввод в строку поиска идентификатор случайного проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Ввод в строку поиска идентификатор случайного проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Ввод в строку поиска идентификатор случайного проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Ввод в строку поиска идентификатор случайного проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def find_random_name_project(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_SEARCHING_FIELD)
            self.find_element(ProjectPageLocators.LOCATOR_PTK_SEARCHING_FIELD).send_keys(random_project_name)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Ввод в строку поиска названия случайного проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Ввод в строку поиска названия случайного проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Ввод в строку поиска названия случайного проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Ввод в строку поиска названия случайного проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def assert_random_id_project_found(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_TABLE_PROJECT_ID)
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_TABLE_PROJECT_ID).text == random_project_id
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Проект со случайным названием найден. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проект со случайным названием найден. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Проект со случайным названием найден. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проект со случайным названием найден. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def assert_random_project_found(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_TABLE_PROJECT_NAME)
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_TABLE_PROJECT_NAME).text == random_project_name
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Проект со случайным названием найден. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проект со случайным названием найден. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Проект со случайным названием найден. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проект со случайным названием найден. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def name_personal_project(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_FILL_NAME_PERSONAL_PROJECT)
            self.find_element(ProjectPageLocators.LOCATOR_PTK_FILL_NAME_PERSONAL_PROJECT).send_keys(
                'Тестовый проект')
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Личный проект назван. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Комментарий в поле комментирования рассчетов оставлен. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Комментарий в поле комментирования рассчетов оставлен. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Комментарий в поле комментирования рассчетов оставлен. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def free_name_personal_project(self, name):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_FILL_NAME_PERSONAL_PROJECT)
            self.find_element(ProjectPageLocators.LOCATOR_PTK_FILL_NAME_PERSONAL_PROJECT).send_keys(
                Keys.CONTROL + 'A',
                Keys.DELETE)
            self.find_element(ProjectPageLocators.LOCATOR_PTK_FILL_NAME_PERSONAL_PROJECT).send_keys(name)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Личный проект назван. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Комментарий в поле комментирования рассчетов оставлен. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Комментарий в поле комментирования рассчетов оставлен. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Комментарий в поле комментирования рассчетов оставлен. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def assert_free_project_name(self):
        try:
            free_name_text = self.find_element(ProjectPageLocators.LOCATOR_PTK_PROJECT_NAME)
            assert free_name_text.text == "Случайное название"
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Проверил, что название проекта совпадает с внесенным изменением. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверил, что название проекта совпадает с внесенным изменением. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Проверил, что название проекта совпадает с внесенным изменением. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверил, что название проекта совпадает с внесенным изменением. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def project_created_notification(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_PROJECT_CREATED_NOTIFICATION)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Получил уведомление о том, что проект создан. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Получил уведомление о том, что проект создан. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Получил уведомление о том, что проект создан. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Получил уведомление о том, что проект создан. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def find_selected_filial_ural(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_URAL_FILIAL_SELECTED)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Филиал МЭС Урала выбран. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Филиал МЭС Урала выбран. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Филиал МЭС Урала выбран. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Филиал МЭС Урала выбран. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def find_selected_filial(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_NORTH_WEST_FILIAL_SELECTED)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Филиал выбран. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Филиал выбран. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Филиал выбран. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Филиал выбран. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def personal_project_dropdown_filial(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_FILL_FILIAL_PERSONAL_PROJECT)
            self.find_clickable_element(ProjectPageLocators.LOCATOR_PTK_FILL_FILIAL_PERSONAL_PROJECT)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Клик на выпадающее меню филиала. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на выпадающее меню филиала. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Клик на выпадающее меню филиала. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на выпадающее меню филиала. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def select_filial_ural(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_SELECT_FILIAL_URAL)
            self.find_clickable_element(ProjectPageLocators.LOCATOR_PTK_SELECT_FILIAL_URAL)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Клик на МЭС Урал. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на МЭС Урал. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Клик на МЭС Урал. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на МЭС Урал. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def select_filial_north_west(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_SELECT_FILIAL_NORTH_WEST)
            self.find_clickable_element(ProjectPageLocators.LOCATOR_PTK_SELECT_FILIAL_NORTH_WEST)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Клик на МЭС Северо-Запад. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на МЭС Северо-Запад. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Клик на МЭС Северо-Запад. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на МЭС Северо-Запад. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def id_personal_project(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_FILL_ID_PERSONAL_PROJECT)
            self.find_element(ProjectPageLocators.LOCATOR_PTK_FILL_ID_PERSONAL_PROJECT).send_keys(
                'Тестовый идентификатор')
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Название личного проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Название личного проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Название личного проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Название личного проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def free_id_personal_project(self, id):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_FILL_ID_PERSONAL_PROJECT)
            self.find_element(ProjectPageLocators.LOCATOR_PTK_FILL_ID_PERSONAL_PROJECT).clear()
            self.find_element(ProjectPageLocators.LOCATOR_PTK_FILL_ID_PERSONAL_PROJECT).send_keys(
                id)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Название личного проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Название личного проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Название личного проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Название личного проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def assert_free_id_personal_project(self):
        try:
            free_id_text = self.find_element(ProjectPageLocators.LOCATOR_PTK_PROJECT_ID)
            assert free_id_text.text == "Случайный идентификатор"
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Проверил, что идентификатор проекта совпадает с внесенным изменением. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверил, что идентификатор проекта совпадает с внесенным изменением. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Проверил, что идентификатор проекта совпадает с внесенным изменением. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверил, что идентификатор проекта совпадает с внесенным изменением. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def comment_calculation(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_COMMENT_CALCULATION)
            self.find_element(ProjectPageLocators.LOCATOR_PTK_COMMENT_CALCULATION).send_keys(
                'Проверка комментирования рассчетов')
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Комментарий в поле комментирования рассчетов оставлен. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Комментарий в поле комментирования рассчетов оставлен. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Комментарий в поле комментирования рассчетов оставлен. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Комментарий в поле комментирования рассчетов оставлен. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def personal_project_save_disabled(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_PERSONAL_PROJECT_SAVE_BUTTON_DISABLED)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Кнопка "Сохранить" отключена. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Кнопка "Сохранить" отключена. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Кнопка "Сохранить" отключена. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Кнопка "Сохранить" отключена. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def assert_project_card_verified(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_VERIFY_TABLE)
            find_bugs = self.find_element(ProjectPageLocators.LOCATOR_PTK_VERIFY_TABLE)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Проверка, что проект проверен. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка, что проект проверен. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Проверка, что проект проверен. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка, что проект проверен. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def click_bifurcations_with_cells(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_BIFURCATIONS_WITH_CELLS)
            self.find_clickable_element(ProjectPageLocators.LOCATOR_PTK_BIFURCATIONS_WITH_CELLS)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Клик на чек-бокс Задвоения с ячейками. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на чек-бокс Задвоения с ячейками. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Клик на чек-бокс Задвоения с ячейками. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на чек-бокс Задвоения с ячейками. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def click_other_burifications(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_OTHER_BURIFICATION)
            self.find_clickable_element(ProjectPageLocators.LOCATOR_PTK_OTHER_BURIFICATION)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Клик на чек-бокс Задвоения прочие. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на чек-бокс Задвоения прочие. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Клик на чек-бокс Задвоения прочие. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на чек-бокс Задвоения прочие. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def click_preparation_of_the_territory(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_PREPARATION_OF_THE_TERRITORY)
            self.find_clickable_element(ProjectPageLocators.LOCATOR_PTK_PREPARATION_OF_THE_TERRITORY)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Клик на чек-бокс Подготовка территории. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на чек-бокс Подготовка территории. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Клик на чек-бокс Подготовка территории. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на чек-бокс Подготовка территории. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def project_card_click_comment_icon(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_COMMENT_ICON)
            self.find_clickable_element(ProjectPageLocators.LOCATOR_PTK_COMMENT_ICON)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Клик на иконку комментария. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на иконку комментария. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Клик на иконку комментария. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на иконку комментария. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def project_card_click_verify_project(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_VERIFY_PROJECT)
            self.find_clickable_element(ProjectPageLocators.LOCATOR_PTK_VERIFY_PROJECT)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Клик на кнопку "Проверить проект". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на кнопку "Проверить проект". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Клик на кнопку "Проверить проект". Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на кнопку "Проверить проект". Успешный.',
                              attachment_type=AttachmentType.PNG)

    def project_card_click_verify_project_button(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_CLICK_VERIFY_PROJECT)
            self.find_clickable_element(ProjectPageLocators.LOCATOR_PTK_CLICK_VERIFY_PROJECT)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Клик на кнопку "Проверить проект". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на кнопку "Проверить проект". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Клик на кнопку "Проверить проект". Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на кнопку "Проверить проект". Успешный.',
                              attachment_type=AttachmentType.PNG)

    def project_card_click_edit_comment(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_EDIT_COMMENT)
            self.find_clickable_element(ProjectPageLocators.LOCATOR_PTK_EDIT_COMMENT)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Клик на иконку комментария. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на иконку комментария. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Клик на иконку комментария. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на иконку комментария. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def project_card_save_comment_button(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_SAVE_COMMENT_BUTTON)
            self.find_clickable_element(ProjectPageLocators.LOCATOR_PTK_SAVE_COMMENT_BUTTON)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Сохранил комментарий. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Сохранил комментарий. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Сохранил комментарий. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Сохранил комментарий. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def project_card_fill_comment_textarea(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_COMMENT_TEXTAREA)
            self.find_element(ProjectPageLocators.LOCATOR_PTK_COMMENT_TEXTAREA).send_keys(
                'QA проверка')
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Клик на иконку комментария. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на иконку комментария. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Клик на иконку комментария. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на иконку комментария. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def project_card_clear_comment_textarea(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_COMMENT_TEXTAREA)
            self.find_element(ProjectPageLocators.LOCATOR_PTK_COMMENT_TEXTAREA).send_keys(
                Keys.CONTROL + 'A',
                Keys.DELETE)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Клик на иконку комментария. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на иконку комментария. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Клик на иконку комментария. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на иконку комментария. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def click_three_dots_icon(self):
        try:
            self.find_clickable_element(ProjectPageLocators.LOCATOR_PTK_THREE_DOTS_ICON)
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_SETTINGS_BUTTON_TEXT).text == "Доп. функции"
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Клик на кнопку с троеточием. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на кнопку с троеточием. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Клик на кнопку с троеточием. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на кнопку с троеточием. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def go_to_project_card(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_FIND_PROJECT_BY_3_LAST_NUMBERS)
            self.find_clickable_element(ProjectPageLocators.LOCATOR_PTK_FIND_PROJECT_BY_3_LAST_NUMBERS)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Переход на карту проекта. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Переход на карту проекта. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Переход на карту проекта. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Переход на карту проекта. Успешный',
                              attachment_type=AttachmentType.PNG)

    def project_card_find_comment_text(self):
        try:
            assert self.find_element(
                ProjectPageLocators.LOCATOR_PTK_COMMENT_TEXTAREA).text == 'QA проверка'
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Клик на иконку комментария. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на иконку комментария. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Клик на иконку комментария. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на иконку комментария. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def project_card_close_comment_textarea(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_CANCEL_CALCULATION)
            self.find_clickable_element(ProjectPageLocators.LOCATOR_PTK_CANCEL_CALCULATION)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Клик на иконку крестика, для закрытия комментария. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на иконку крестика, для закрытия комментария. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Клик на иконку крестика, для закрытия комментария. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на иконку крестика, для закрытия комментария. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def go_to_another_personal_project(self):
        try:
            self.driver.get(ProjectPageLocators.LOCATOR_PTK_GO_TO_PERSONAL_PROJECT_2)
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_RASSCHET_BUTTON).text == "Рассчитать"
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Переход на страницу логина. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Переход на страницу логина. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Переход на страницу логина. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Переход на страницу логина. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def save_unc_button(self):
        try:
            assert self.find_element(
                ProjectPageLocators.LOCATOR_PTK_SAVE_UNC_BUTTON)
            save_button_unc = self.find_element(
                ProjectPageLocators.LOCATOR_PTK_SAVE_UNC_BUTTON)
            self.driver.execute_script("arguments[0].click()", save_button_unc)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Клик на кнопку "Сохранить" в таблице расценок УНЦ. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на кнопку "Сохранить" в таблице расценок УНЦ. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Клик на кнопку "Сохранить" в таблице расценок УНЦ. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на кнопку "Сохранить" в таблице расценок УНЦ. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def project_edit_icon(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_EDIT_ICON)
            self.find_clickable_element(ProjectPageLocators.LOCATOR_PTK_EDIT_ICON)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Клик на кнопку редактирования проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на кнопку редактирования проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                ''
                '"Passed"}}')
            with allure.step('Клик на кнопку редактирования проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на кнопку редактирования проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def project_click_region_selector(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_CLICK_REGION_SELECTOR)
            self.find_clickable_element(ProjectPageLocators.LOCATOR_PTK_CLICK_REGION_SELECTOR)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Клик на кнопку выбора региона проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на кнопку выбора региона проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Клик на кнопку выбора региона проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на кнопку выбора региона проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def project_click_region_selector_after_save(self):
        try:
            assert self.find_element(
                ProjectPageLocators.LOCATOR_PTK_CLICK_REGION_SELECTOR_AFTER_SAVE)
            self.find_clickable_element(
                ProjectPageLocators.LOCATOR_PTK_CLICK_REGION_SELECTOR_AFTER_SAVE)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Клик на кнопку выбора региона проекта после сохранения. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на кнопку выбора региона проекта после сохранения. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Клик на кнопку выбора региона проекта после сохранения. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на кнопку выбора региона проекта после сохранения. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def go_to_personal_project(self):
        try:
            self.driver.get(ProjectPageLocators.LOCATOR_PTK_GO_TO_PERSONAL_PROJECT)
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_RASSCHET_BUTTON).text == "Рассчитать"
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Переход на страницу личного проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Переход на страницу личного проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Переход на страницу личного проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Переход на страницу личного проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def go_to_test_project(self):
        try:
            self.driver.get(ProjectPageLocators.LOCATOR_PTK_GO_TO_TEST_PROJECT)
            assert self.find_elements(ProjectPageLocators.LOCATOR_PTK_RASSCHET_BUTTON).text == "Рассчитать"
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Переход на страницу личного проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Переход на страницу личного проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Переход на страницу тестового проект. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Переход на страницу тестового проект. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def get_project_id(self):
        try:
            global get_project_id
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_FIND_PROJECT_BY_3_LAST_NUMBERS)
            get_project_id = self.find_element(
                ProjectPageLocators.LOCATOR_PTK_FIND_PROJECT_BY_3_LAST_NUMBERS).get_attribute(
                'innerText')
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Получение идентификатора проекта. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Получение идентификатора проекта. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Получение идентификатора проекта. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='ППолучение идентификатора проекта. Успешный',
                              attachment_type=AttachmentType.PNG)

    def assert_project_id(self):
        try:
            assert self.find_element(
                ProjectPageLocators.LOCATOR_PTK_ASSERT_PROJECT_ID_IN_CARD).text == get_project_id
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "ID '
                'not equal"}}')
            with allure.step('Проверка идентичности идентификаторов. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка идентичности идентификаторов. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "ID '
                'equals"}}')
            with allure.step('Проверка идентичности идентификаторов. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка идентичности идентификаторов. Успешный',
                              attachment_type=AttachmentType.PNG)

    def assert_nds_disabled(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_NDS_DISABLED)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Проверка, что кнопка НДС неактивна. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка, что кнопка НДС неактивна. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Проверка, что кнопка НДС неактивна. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка, что кнопка НДС неактивна. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def back_to_main_page(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_BACK_TO_MAIN_PAGE_ARROW)
            back_to_main_page = self.find_element(ProjectPageLocators.LOCATOR_PTK_BACK_TO_MAIN_PAGE_ARROW)
            self.driver.execute_script("arguments[0].click()", back_to_main_page)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Клик на кнопку возвращения к главной странице. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на кнопку возвращения к главной странице. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Поиск кнопки возвращения к главной странице. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск кнопки возвращения к главной странице. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def assert_comment_disabled(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_COMMENT_DISABLED)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Проверка того, что комментарий отключен. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка того, что комментарий отключен. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Проверка того, что комментарий отключен. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка того, что комментарий отключен. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def assert_region_disabled(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_REGION_DISABLED)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Проверка того, что селектор региона неактивен. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка того, что селектор региона неактивен. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Проверка того, что селектор региона неактивен. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка того, что селектор региона неактивен. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def click_archive_form_20(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_CLICK_ARCHIVE_FORM_20)
            self.find_clickable_element(ProjectPageLocators.LOCATOR_PTK_CLICK_ARCHIVE_FORM_20)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Клик на кнопку выгрузки "Архив форм 20". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на кнопку выгрузки "Архив форм 20". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Клик на кнопку выгрузки "Архив форм 20". Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на кнопку выгрузки "Архив форм 20". Успешный.',
                              attachment_type=AttachmentType.PNG)

    def click_excel_form(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_CLICK_EXCEL_FORM)
            self.find_clickable_element(ProjectPageLocators.LOCATOR_PTK_CLICK_EXCEL_FORM)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Клик на кнопку выгрузки эксель формы. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на кнопку выгрузки эксель формы. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Клик на кнопку выгрузки эксель формы. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на кнопку выгрузки эксель формы. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def personal_project_random_id(self):
        try:
            global random_project_id
            letters_names = (
                "а", "б", "в", "г", "д", "ж", "з", "е", "и", "к", "л", "ы", "н", "к", "о", "щ", "х", "ъ", "й", "ц", "у",
                "ф",
                "ч",
                "с", "м", "т", "ь", "д")
            all_combos_names = list(itertools.combinations(letters_names, 7))
            all_combos_names = [''.join(combo) for combo in all_combos_names]
            random_project_id = random.sample(all_combos_names, 1)[0]
            clear_field = self.find_element(ProjectPageLocators.LOCATOR_PTK_FILL_ID_PERSONAL_PROJECT).clear()
            send_keys = self.find_element(ProjectPageLocators.LOCATOR_PTK_FILL_ID_PERSONAL_PROJECT).send_keys(
                random_project_id)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Генерация случайного идентификатора. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Генерация случайного идентификатора. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Генерация случайного идентификатора. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Генерация случайного идентификатора. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def personal_project_random_name(self):
        try:
            global random_project_name
            letters_names = (
                "а", "б", "в", "г", "д", "ж", "з", "е", "и", "к", "л", "ы", "н", "к", "о", "щ", "х", "ъ", "й", "ц", "у",
                "ф",
                "ч",
                "с", "м", "т", "ь", "д")
            all_combos_names = list(itertools.combinations(letters_names, 7))
            all_combos_names = [''.join(combo) for combo in all_combos_names]
            random_project_name = random.sample(all_combos_names, 1)[0]
            clear_field = self.find_element(ProjectPageLocators.LOCATOR_PTK_FILL_NAME_PERSONAL_PROJECT).clear()
            send_keys = self.find_element(ProjectPageLocators.LOCATOR_PTK_FILL_NAME_PERSONAL_PROJECT).send_keys(
                random_project_name)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Генерация случайного имени проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Генерация случайного имени проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Генерация случайного имени проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Генерация случайного имени проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def searching_by_incorrect_ID(self):
        try:
            assert self.find_element(
                ProjectPageLocators.LOCATOR_PTK_ARCHIVE_FORM_20_SEARCHING_BY_ID)
            self.find_element(
                ProjectPageLocators.LOCATOR_PTK_ARCHIVE_FORM_20_SEARCHING_BY_ID).send_keys('randomid')
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Поиск проекта по некорректному идентификатору. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск проекта по некорректному идентификатору. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Поиск проекта по некорректному идентификатору. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск проекта по некорректному идентификатору. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def download_button(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_DOWNLOAD_BUTTON)
            self.find_clickable_element(ProjectPageLocators.LOCATOR_PTK_DOWNLOAD_BUTTON)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Проверка кнопки загрузки. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Проверка кнопки загрузки. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Проверка кнопки загрузки. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Проверка кнопки загрузки. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def project_card_assert_deleted(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_ASSERT_DELETED)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Проверка удаления карты проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка удаления карты проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Проверка удаления карты проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка удаления карты проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def project_card_add_comment(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_ADD_COMMENT)
            self.find_element(ProjectPageLocators.LOCATOR_PTK_ADD_COMMENT).send_keys(
                'Тестовый комментарий' + time.ctime())
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Проверка добавления комментария в карточку проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка добавления комментария в карточку проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Проверка добавления комментария в карточку проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка добавления комментария в карточку проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def project_card_click_on_instructions(self):
        try:
            assert self.find_element(
                ProjectPageLocators.LOCATOR_PTK_CLICK_INSTRUCTIONS)
            self.find_clickable_element(
                ProjectPageLocators.LOCATOR_PTK_CLICK_INSTRUCTIONS)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Клик на инструкцию в карточке проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на инструкцию в карточке проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Клик на инструкцию в карточке проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на инструкцию в карточке проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def project_card_find_instructions(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_FIND_ISTRUCTIONS)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Поиск инструкции в карточке проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск инструкции в карточке проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Поиск инструкции в карточке проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск инструкции в карточке проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def go_to_test_calc(self):
        try:
            self.driver.get(ProjectPageLocators.LOCATOR_PTK_CALCULATION_TEST_CARD)
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_RASSCHET_BUTTON).text == "Рассчитать"
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Переход в тестовый проект для калькуляции. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Переход в тестовый проект для калькуляции. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Переход в тестовый проект для калькуляции. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Переход в тестовый проект для калькуляции. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def project_card_fill_amount_4(self):
        try:
            assert self.find_element(
                ProjectPageLocators.LOCATOR_PTK_FILL_AMOUNT_4)
            self.find_element(
                ProjectPageLocators.LOCATOR_PTK_FILL_AMOUNT_4).send_keys('2516586')
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Заполнение продукта в четвертой строке. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Заполнение продукта в четвертой строке. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Заполнение продукта в четвертой строке. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Переход в тестовый проект для калькуляции. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def project_card_pick_table_4(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_PICK_TABLE_4)
            find_table_4 = self.find_element(ProjectPageLocators.LOCATOR_PTK_PICK_TABLE_4)
            self.driver.execute_script("arguments[0].click()", find_table_4)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Выбор четвертой таблицы в карточке проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбор четвертой таблицы в карточке проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.find_element(ProjectPageLocators.LOCATOR_PTK_PICK_TABLE_4)
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Выбор четвертой таблицы в карточке проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбор четвертой таблицы в карточке проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def project_card_pick_table_5(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_PICK_TABLE_5)
            find_table_5 = self.find_element(ProjectPageLocators.LOCATOR_PTK_PICK_TABLE_5)
            self.driver.execute_script("arguments[0].click()", find_table_5)
            self.find_element(ProjectPageLocators.LOCATOR_PTK_PICK_TABLE_5)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Выбор пятой таблицы в карточке проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбор четвертой таблицы в карточке проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Выбор пятой таблицы в карточке проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбор четвертой таблицы в карточке проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def project_card_click_on_plus(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_CLICK_ON_PLUS)
            self.find_clickable_element(ProjectPageLocators.LOCATOR_PTK_CLICK_ON_PLUS)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Клик на плюс в карточке проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на плюс в карточке проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Клик на плюс в карточке проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на плюс в карточке проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def assert_invisible_plus(self):
        try:
            assert self.invisible_element(ProjectPageLocators.LOCATOR_PTK_CLICK_ON_PLUS)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Проверка отсутствия плюса в карточке проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка отсутствия плюса в карточке проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Проверка отсутствия плюса в карточке проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка отсутствия плюса в карточке проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def project_card_pick_table_2(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_PICK_TABLE_2)
            self.find_clickable_element(ProjectPageLocators.LOCATOR_PTK_PICK_TABLE_2)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Выбор второй таблицы в карточке проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбор второй таблицы в карточке проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Выбор второй таблицы в карточке проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбор второй таблицы в карточке проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def project_card_fill_amount_5(self):
        try:
            assert self.find_element(
                ProjectPageLocators.LOCATOR_PTK_FILL_AMOUNT_5)
            self.find_element(
                ProjectPageLocators.LOCATOR_PTK_FILL_AMOUNT_5).send_keys('2516586')
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Заполнение количества продукта в пятой строке. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Заполнение количества продукта в пятой строке. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Заполнение количества продукта в пятой строке. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Заполнение количества продукта в пятой строке. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def project_card_fill_amount(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_FILL_AMOUNT)
            self.find_element(ProjectPageLocators.LOCATOR_PTK_FILL_AMOUNT).send_keys(
                '2516586')
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Заполнение количества продукта в первой строке. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Заполнение количества продукта в первой строке. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Заполнение количества продукта в первой строке. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Заполнение количества продукта в первой строке. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def project_card_fill_amount_2(self):
        try:
            assert self.find_element(
                ProjectPageLocators.LOCATOR_PTK_FILL_AMOUNT_TABLE_2)
            self.find_element(
                ProjectPageLocators.LOCATOR_PTK_FILL_AMOUNT_TABLE_2).send_keys('2516586')
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Заполнение количества продукта во второй строке. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Заполнение количества продукта во второй строке. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Заполнение количества продукта во второй строке. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Заполнение количества продукта во второй строке. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def project_card_save_button(self):
        try:
            assert self.find_element(
                ProjectPageLocators.LOCATOR_PTK_SAVE_COMMENT_BUTTON)
            self.find_clickable_element(
                ProjectPageLocators.LOCATOR_PTK_SAVE_COMMENT_BUTTON)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Клик на кнопку "Сохранить" в расчетной части. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на кнопку "Сохранить" в расчетной части. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Клик на кнопку "Сохранить" в расчетной части. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на кнопку "Сохранить" в расчетной части. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def project_card_assert_value_amount(self):
        try:
            assert self.find_element(
                ProjectPageLocators.LOCATOR_PTK_ASSERT_VALUE_AMOUNT)
            get_value_amount = self.find_element(ProjectPageLocators.LOCATOR_PTK_ASSERT_VALUE_AMOUNT).get_attribute(
                'value')
            assert get_value_amount == '2516586'
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_CALCULATION_UNC).text != '0.00'
            find_cross = self.find_element(ProjectPageLocators.LOCATOR_PTK_DELETE_5)
            self.driver.execute_script("arguments[0].click()", find_cross)
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_CALCULATION_TABLE_1_CLEAR).text == "0.00"
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Проверка введенного и отображенного значения в таблице рассчетов. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка введенного и отображенного значения в таблице рассчетов. Провален.',
                              attachment_type=AttachmentType.PNG)
            with allure.step('Удаление записи через крестик. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Удаление записи через крестик. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Проверка введенного и отображенного значения в таблице рассчетов. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка введенного и отображенного значения в таблице рассчетов. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def project_card_assert_value_amount_5(self):
        try:
            assert self.find_element(
                ProjectPageLocators.LOCATOR_PTK_ASSERT_VALUE_AMOUNT_5)
            get_value_amount = self.find_element(ProjectPageLocators.LOCATOR_PTK_ASSERT_VALUE_AMOUNT_5).get_attribute(
                'value')
            assert get_value_amount == '2516586'
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_DELETE_5)
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_CALCULATION_UNC).text != '0.00'
            find_cross = self.find_element(ProjectPageLocators.LOCATOR_PTK_DELETE_5)
            self.driver.execute_script("arguments[0].click()", find_cross)
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_CALCULATION_TABLE_5_CLEAR).text == "0.00"
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step(
                    'Проверка введенного и отображенного значения в пятой строке таблицы рассчетов. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка введенного и отображенного значения в пятой строке таблицы рассчетов. '
                                   'Провален.',
                              attachment_type=AttachmentType.PNG)
            with allure.step('Удаление записи через крестик. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Удаление записи через крестик. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step(
                    'Проверка введенного и отображенного значения в пятой строке таблицы рассчетов. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка введенного и отображенного значения в пятой строке таблицы рассчетов. '
                                   'Успешный.',
                              attachment_type=AttachmentType.PNG)

    def project_card_assert_value_amount_4(self):
        try:
            assert self.find_element(
                ProjectPageLocators.LOCATOR_PTK_ASSERT_VALUE_AMOUNT_4)
            get_value_amount = self.find_element(ProjectPageLocators.LOCATOR_PTK_ASSERT_VALUE_AMOUNT_4).get_attribute(
                'value')
            assert get_value_amount == '2516586'
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_DELETE_5)
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_CALCULATION_UNC).text != '0.00'
            find_cross = self.find_element(ProjectPageLocators.LOCATOR_PTK_DELETE_5)
            self.driver.execute_script("arguments[0].click()", find_cross)
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_CALCULATION_TABLE_4_CLEAR).text == "0.00"
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step(
                    'Проверка введенного и отображенного значения в четвертой строке таблицы рассчетов. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка введенного и отображенного значения в четвертой строке таблицы '
                                   'рассчетов. Провален.',
                              attachment_type=AttachmentType.PNG)
            with allure.step('Удаление записи через крестик. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Удаление записи через крестик. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step(
                    'Проверка введенного и отображенного значения в четвертой строке таблицы рассчетов. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка введенного и отображенного значения в четвертой строке таблицы '
                                   'рассчетов. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def project_card_assert_value_amount_2(self):
        try:
            assert self.find_element(
                ProjectPageLocators.LOCATOR_PTK_ASSERT_VALUE_AMOUNT_2)
            get_value_amount = self.find_element(ProjectPageLocators.LOCATOR_PTK_ASSERT_VALUE_AMOUNT_2).get_attribute(
                'value')
            assert get_value_amount == '2516586'
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_DELETE_5)
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_CALCULATION_UNC).text != '0.00'
            find_cross = self.find_element(ProjectPageLocators.LOCATOR_PTK_DELETE_5)
            self.driver.execute_script("arguments[0].click()", find_cross)
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_CALCULATION_TABLE_2_CLEAR).text == "0.00"
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step(
                    'Проверка введенного и отображенного значения во второй строке таблицы рассчетов. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка введенного и отображенного значения во второй строке таблицы рассчетов. '
                                   'Провален.',
                              attachment_type=AttachmentType.PNG)
            with allure.step('Удаление записи через крестик. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Удаление записи через крестик. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step(
                    'Проверка введенного и отображенного значения во второй строке таблицы рассчетов. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка введенного и отображенного значения во второй строке таблицы рассчетов. '
                                   'Успешный.',
                              attachment_type=AttachmentType.PNG)

    def project_card_select_product(self):
        try:
            assert self.find_element(
                ProjectPageLocators.LOCATOR_PTK_SELECT_PRODUCT)
            self.find_clickable_element(
                ProjectPageLocators.LOCATOR_PTK_SELECT_PRODUCT)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step(
                    'Выбор продукта в карточке продукта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбор продукта в карточке продукта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step(
                    'Выбор продукта в карточке продукта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбор продукта в карточке продукта. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def project_card_select_table_1(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_SELECT_TABLE_1)
            self.find_clickable_element(ProjectPageLocators.LOCATOR_PTK_SELECT_TABLE_1)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step(
                    'Выбор таблицы один в карточке проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбор таблицы один в карточке проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step(
                    'Выбор таблицы один в карточке проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбор таблицы один в карточке проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def project_card_select_b_1(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_SELECT_B_1)
            self.find_clickable_element(ProjectPageLocators.LOCATOR_PTK_SELECT_B_1)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step(
                    'Выбор раздела Б1 в таблице карточки проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбор раздела Б1 в таблице карточки проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step(
                    'Выбор раздела Б1 в таблице карточки проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбор раздела Б1 в таблице карточки проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def project_card_pick_table_3(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_PICK_TABLE_3)
            find_table_3 = self.find_element(ProjectPageLocators.LOCATOR_PTK_PICK_TABLE_3)
            self.driver.execute_script("arguments[0].click()", find_table_3)
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_PICK_TABLE_3)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step(
                    'Выбор таблицы три в карточке проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбор таблицы три в карточке проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step(
                    'Выбор таблицы три в карточке проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбор таблицы три в карточке проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def project_card_fill_amount_3(self):
        try:
            assert self.find_element(
                ProjectPageLocators.LOCATOR_PTK_FILL_AMOUNT_TABLE_3)
            self.find_element(
                ProjectPageLocators.LOCATOR_PTK_FILL_AMOUNT_TABLE_3).send_keys('2516586')
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step(
                    'Заполнение количества продукта в строке три. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Заполнение количества продукта в строке три. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step(
                    'Заполнение количества продукта в строке три. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Заполнение количества продукта в строке три. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def project_card_assert_value_amount_3(self):
        try:
            assert self.find_element(
                ProjectPageLocators.LOCATOR_PTK_ASSERT_VALUE_AMOUNT_3)
            get_value_amount = self.find_element(ProjectPageLocators.LOCATOR_PTK_ASSERT_VALUE_AMOUNT_3).get_attribute(
                'value')
            assert get_value_amount == '2516586'
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_DELETE_5)
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_CALCULATION_UNC).text != '0.00'
            find_cross = self.find_element(ProjectPageLocators.LOCATOR_PTK_DELETE_5)
            self.driver.execute_script("arguments[0].click()", find_cross)
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_CALCULATION_TABLE_3_CLEAR).text == "0.00"
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step(
                    'Проверка введенного и отображенного значения в третьей строке таблицы рассчетов. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка введенного и отображенного значения в третьей строке таблицы рассчетов. '
                                   'Провален',
                              attachment_type=AttachmentType.PNG)
            with allure.step('Удаление записи через крестик. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Удаление записи через крестик. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step(
                    'Проверка введенного и отображенного значения в третьей строке таблицы рассчетов. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка введенного и отображенного значения в третьей строке таблицы рассчетов. '
                                   'Успешный',
                              attachment_type=AttachmentType.PNG)

    def assert_calculation(self):
        try:
            assert self.find_element(
                ProjectPageLocators.LOCATOR_PTK_CALCULATION_UNC)
            assert self.find_element(
                ProjectPageLocators.LOCATOR_PTK_CALCULATION_UNC_I_NDS)
            assert self.find_element(
                ProjectPageLocators.LOCATOR_PTK_CALCULATION_UNC_PROGNOZ)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Поиск элементов рассчетной части. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск элементов рассчетной части. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Поиск элементов рассчетной части. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск элементов рассчетной части. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def cancel_calculation(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_CANCEL_CALCULATION)
            self.find_clickable_element(ProjectPageLocators.LOCATOR_PTK_CANCEL_CALCULATION)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Выход из рассчетной части. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выход из рассчетной части. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Выход из рассчетной части. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выход из рассчетной части. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def assert_calculation_canceled(self):
        try:
            assert self.invisible_element(
                ProjectPageLocators.LOCATOR_PTK_CALCULATION_BLOCK_1)
        except:
            with allure.step('Проверка выхода из рассчетной части. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка выхода из рассчетной части. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Проверка выхода из рассчетной части. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка выхода из рассчетной части. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def summa_s_nds(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_EDITED_SUMMA_S_NDS)
            self.find_element(ProjectPageLocators.LOCATOR_PTK_EDITED_SUMMA_S_NDS).send_keys(
                Keys.CONTROL,
                "A",
                Keys.DELETE)
            self.find_element(ProjectPageLocators.LOCATOR_PTK_EDITED_SUMMA_S_NDS).send_keys('1 234')
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Ввод значения в поле "Сумма с НДС". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Ввод значения в поле "Сумма с НДС". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Ввод значения в поле "Сумма с НДС". Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Ввод значения в поле "Сумма с НДС". Успешный.',
                              attachment_type=AttachmentType.PNG)

    def send_keys_to_nds(self):
        try:
            self.find_element(ProjectPageLocators.LOCATOR_PTK_EDITED_SUMMA_S_NDS).send_keys('1 234')
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Ввод значения в поле "Сумма с НДС". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Ввод значения в поле "Сумма с НДС". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Ввод значения в поле "Сумма с НДС". Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Ввод значения в поле "Сумма с НДС". Успешный.',
                              attachment_type=AttachmentType.PNG)

    def giant_nds(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_SUMMA_S_NDS)
            self.find_element(ProjectPageLocators.LOCATOR_PTK_SUMMA_S_NDS).send_keys(Keys.CONTROL,
                                                                                     "A",
                                                                                     Keys.DELETE)
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_SUMMA_S_NDS)
            self.find_element(ProjectPageLocators.LOCATOR_PTK_SUMMA_S_NDS).send_keys(
                '1234567890123')
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            assert self.find_clickable_element(ProjectPageLocators.LOCATOR_PTK_CANCEL_CALCULATION)
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            assert self.find_element(
                ProjectPageLocators.LOCATOR_PTK_INFO_BLOCK).text == '1234567890123.00'
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Ввод двенадцати значного значения в поле "Сумма с НДС". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Ввод двенадцати значного значения в поле "Сумма с НДС". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Ввод двенадцати значного значения в поле "Сумма с НДС". Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Ввод двенадцати значного значения в поле "Сумма с НДС". Успешный.',
                              attachment_type=AttachmentType.PNG)

    def assert_summa_s_nds(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_ASSERT_SUMMA_S_NDS).text == (
                '1 234.00')
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Проверка отображаемого значения в поле "Сумма с НДС". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка отображаемого значения в поле "Сумма с НДС". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Проверка отображаемого значения в поле "Сумма с НДС". Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка отображаемого значения в поле "Сумма с НДС". Успешный.',
                              attachment_type=AttachmentType.PNG)

    def click_on_calculation(self):
        try:
            assert self.find_element(
                ProjectPageLocators.LOCATOR_PTK_CLICK_ON_CALCULATION)
            self.find_clickable_element(
                ProjectPageLocators.LOCATOR_PTK_CLICK_ON_CALCULATION)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Клик на кнопку "Рассчитать". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на кнопку "Рассчитать". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Клик на кнопку "Рассчитать". Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка отображаемого значения в поле "Сумма с НДС". Успешный.',
                              attachment_type=AttachmentType.PNG)

    def project_card_select_altay_region(self):
        try:
            assert self.find_element(
                ProjectPageLocators.LOCATOR_PTK_SELECT_ALTAY_REGION)
            select_altay = self.find_element(
                ProjectPageLocators.LOCATOR_PTK_SELECT_ALTAY_REGION)
            self.driver.execute_script("arguments[0].click()", select_altay)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Выбор региона Алтай в карточке проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбор региона Алтай в карточке проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Выбор региона Алтай в карточке проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбор региона Алтай в карточке проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def project_card_select_amur_region(self):
        try:
            assert self.find_element(
                ProjectPageLocators.LOCATOR_PTK_SELECT_AMUR_REGION)
            select_amur = self.find_element(
                ProjectPageLocators.LOCATOR_PTK_SELECT_AMUR_REGION)
            self.driver.execute_script("arguments[0].click()", select_amur)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Выбор региона Амур в карточке проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбор региона Амур в карточке проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Выбор региона Амур в карточке проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбор региона Амур в карточке проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def project_card_assert_altay_selected(self):
        try:
            assert self.find_element(
                ProjectPageLocators.LOCATOR_PTK_CHECK_ALTAY_SELECTED)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Проверка отображения региона Алтай после выбора в карточке проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка отображения региона Алтай после выбора в карточке проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Проверка отображения региона Алтай после выбора в карточке проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка отображения региона Алтай после выбора в карточке проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def project_card_assert_amur_selected(self):
        try:
            assert self.find_element(
                ProjectPageLocators.LOCATOR_PTK_CHECK_AMUR_SELECTED)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Проверка отображения региона Амур после выбора в карточке проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка отображения региона Амур после выбора в карточке проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Проверка отображения региона Амур после выбора в карточке проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка отображения региона Амур после выбора в карточке проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def project_card_click_on_body(self):
        try:
            assert self.find_element(
                ProjectPageLocators.LOCATOR_PTK_CLICK_ON_BODY)
            body_click = self.find_element(
                ProjectPageLocators.LOCATOR_PTK_CLICK_ON_BODY)
            self.driver.execute_script("arguments[0].click()", body_click)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Клик на тело страницы карточки проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на тело страницы карточки проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Клик на тело страницы карточки проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на тело страницы карточки проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def project_card_commentary(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_COMMENTARY)
            project_card_commentary = self.find_element(ProjectPageLocators.LOCATOR_PTK_COMMENTARY)
            project_card_commentary.send_keys(
                'Тестовый комментарий' + time.ctime())
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Поиск поля для комментария в карточке страницы. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск поля для комментария в карточке страницы. Провален.',
                              attachment_type=AttachmentType.PNG)
            with allure.step('Ввод комментария с текущей датой в карточке страницы. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск поля для комментария в карточке страницы. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Ввод комментария с текущей датой в карточке страницы. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск поля для комментария в карточке страницы. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def get_comment_value(self):
        try:
            global get_comment_value
            get_comment_value = self.find_element(ProjectPageLocators.LOCATOR_PTK_COMMENT).get_attribute('value')
            self.find_element(ProjectPageLocators.LOCATOR_PTK_COMMENT).get_attribute('value')
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Забираем значение комментария. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Забираем значение комментария. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Забираем значение комментария. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Забираем значение комментария. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def find_equal_commentary(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_COMMENT).get_attribute('value')
            get_value = self.find_element(ProjectPageLocators.LOCATOR_PTK_COMMENT).get_attribute('value')
            assert get_comment_value == get_value
            self.find_element(ProjectPageLocators.LOCATOR_PTK_COMMENT).send_keys(
                Keys.CONTROL + 'A' + Keys.DELETE)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Находим искомый комментарий. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Находим искомый комментарий. Провален.',
                              attachment_type=AttachmentType.PNG)
            with allure.step('Сравниваем значения искомого комментария с найденным. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Сравниваем значения искомого комментария с найденным. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Сравниваем значения искомого комментария с найденным. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Сравниваем значения искомого комментария с найденным. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def first_click_download_xlsx_project_card(self):
        try:
            assert self.find_element(
                ProjectPageLocators.LOCATOR_PTK_FIRST_CLICK_DOWNLOAD_XLSX)
            self.find_clickable_element(
                ProjectPageLocators.LOCATOR_PTK_FIRST_CLICK_DOWNLOAD_XLSX)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Выбираем эксель выгрузку на карточке проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбираем эксель выгрузку на карточке проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Выбираем эксель выгрузку на карточке проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбираем эксель выгрузку на карточке проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def first_click_download_tech_params_project_card(self):
        try:
            assert self.find_element(
                ProjectPageLocators.LOCATOR_PTK_FIRST_CLICK_DOWNLOAD_TECH_PARAMS)
            self.find_clickable_element(
                ProjectPageLocators.LOCATOR_PTK_FIRST_CLICK_DOWNLOAD_TECH_PARAMS)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Выбираем технических параметров выгрузку на карточке проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбираем технических параметров выгрузку на карточке проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Выбираем технических параметров выгрузку на карточке проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбираем технических параметров выгрузку на карточке проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def first_click_download_summary_table_project_card(self):
        try:
            assert self.find_element(
                ProjectPageLocators.LOCATOR_PTK_FIRST_CLICK_DOWNLOAD_SUMMARY_TABLE)
            self.find_clickable_element(
                ProjectPageLocators.LOCATOR_PTK_FIRST_CLICK_DOWNLOAD_SUMMARY_TABLE)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Выбираем сводную таблицу на карточке проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбираем сводную таблицу на карточке проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Выбираем сводную таблицу на карточке проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбираем сводную таблицу на карточке проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def first_click_download_unc_oa_project_card(self):
        try:
            assert self.find_element(
                ProjectPageLocators.LOCATOR_PTK_FIRST_CLICK_DOWNLOAD_UNC_OA)
            self.find_clickable_element(
                ProjectPageLocators.LOCATOR_PTK_FIRST_CLICK_DOWNLOAD_UNC_OA)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Выбираем УНЦ ОА на карточке проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбираем УНЦ ОА на карточке проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Выбираем УНЦ ОА на карточке проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбираем УНЦ ОА на карточке проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def first_click_download_SD_project_card(self):
        try:
            assert self.find_element(
                ProjectPageLocators.LOCATOR_PTK_FIRST_CLICK_DOWNLOAD_SD)
            self.find_clickable_element(
                ProjectPageLocators.LOCATOR_PTK_FIRST_CLICK_DOWNLOAD_SD)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Выбираем СД на карточке проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбираем СД на карточке проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Выбираем СД на карточке проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбираем СД на карточке проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def first_click_download_equipment_project_card(self):
        try:
            assert self.find_element(
                ProjectPageLocators.LOCATOR_PTK_FIRST_CLICK_DOWNLOAD_EQUIPMENT)
            self.find_clickable_element(
                ProjectPageLocators.LOCATOR_PTK_FIRST_CLICK_DOWNLOAD_EQUIPMENT)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Выбираем оборудование на карточке проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбираем оборудование на карточке проекта Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Выбираем оборудование на карточке проекта Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбираем оборудование на карточке проекта Успешный.',
                              attachment_type=AttachmentType.PNG)

    def check_equipment_downloaded(self):
        try:
            assert self.driver.execute_script(
                'browserstack_executor: {"action": "fileExists", "arguments": {"fileName": "equipment.xlsx"}}')
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Скачивание файла "Оборудование". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Скачивание файла "Оборудование". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Скачивание файла "Оборудование". Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Скачивание файла "Оборудование". Успешный.',
                              attachment_type=AttachmentType.PNG)

    def check_SD_downloaded(self):
        try:
            assert self.driver.execute_script(
                'browserstack_executor: {"action": "fileExists", "arguments": {"fileName": "sd.xlsx"}}')
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Скачивание файла УНЦ ОА. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Скачивание файла УНЦ ОА. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Скачивание файла УНЦ ОА. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Скачивание файла УНЦ ОА. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def check_unc_oa_downloaded(self):
        try:
            assert self.driver.execute_script(
                'browserstack_executor: {"action": "fileExists", "arguments": {"fileName": "unc_oa.xlsx"}}')
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Скачивание файла УНЦ ОА. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Скачивание файла УНЦ ОА. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Скачивание файла УНЦ ОА. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Скачивание файла УНЦ ОА. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def check_f20_downloaded(self):
        try:
            assert self.driver.execute_script(
                'browserstack_executor: {"action": "fileExists", "arguments": {"fileName": "K_3335473_f20.xlsx"}}')
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Скачивание файла Ф20. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Скачивание файла Ф20. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Скачивание файла Ф20. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Скачивание файла Ф20. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def check_tech_params_downloaded(self):
        try:
            assert self.driver.execute_script(
                'browserstack_executor: {"action": "fileExists", "arguments": {"fileName": "K_3335473_parameters.xlsx"}}')
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Скачивание файла Технических параметров. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Скачивание файла Технических параметров. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Скачивание файла Технических параметров. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Скачивание файла Технических параметров. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def check_summary_table_downloaded(self):
        try:
            assert self.driver.execute_script('browserstack_executor: {"action": "fileExists"}')
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Скачивание сводной таблицы. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Скачивание сводной таблицы. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Скачивание сводной таблицы. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Скачивание сводной таблицы. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def click_download_xlsx_project_card(self):
        try:
            assert self.find_element(
                ProjectPageLocators.LOCATOR_PTK_SECOND_CLICK_DOWNLOAD_XLSX)
            self.find_clickable_element(
                ProjectPageLocators.LOCATOR_PTK_SECOND_CLICK_DOWNLOAD_XLSX)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Загружаем эксель выгрузку на карточке проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Загружаем эксель выгрузку на карточке проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Загружаем эксель выгрузку на карточке проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Загружаем эксель выгрузку на карточке проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)

    def project_description_text(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_PROJECT_DESCRIPTION_TEXT)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Поиск поля с описанием проекта. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Поиск поля с описанием проекта. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Поиск поля с описанием проекта. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Поиск поля с описанием проекта. Успешный',
                              attachment_type=AttachmentType.PNG)

    def project_description_text_part_2(self):
        try:
            assert self.find_element(
                ProjectPageLocators.LOCATOR_PTK_PROJECT_DESCRIPTION_TEXT_PART_2)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Поиск поля с описанием проекта, продолжение. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск поля с описанием проекта, продолжение. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Поиск поля с описанием проекта, продолжение. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск поля с описанием проекта, продолжение. Успешный',
                              attachment_type=AttachmentType.PNG)

    def show_button(self):
        try:
            assert self.find_element(ProjectPageLocators.LOCATOR_PTK_SHOW_BUTTON)
            self.find_clickable_element(ProjectPageLocators.LOCATOR_PTK_SHOW_BUTTON)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": '
                '"Failed"}}')
            with allure.step('Проверка кнопки "Показать". Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка кнопки "Показать". Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            assert False
        else:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": '
                '"Passed"}}')
            with allure.step('Проверка кнопки "Показать". Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка кнопки "Показать". Успешный',
                              attachment_type=AttachmentType.PNG)
