import time

from selenium.common.exceptions import StaleElementReferenceException
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

class BasePage:
    try:
        def __init__(self, driver):
            self.driver = driver
            self.base_url = "https://dev.modptk.unc-esk.ru/login"

        def find_element(self, locator, time=10):
            return WebDriverWait(self.driver, time).until(
                EC.presence_of_element_located(locator),
                message=f"Can't find element by locator {locator}")

        def find_elements(self, locator, time=300):
            return WebDriverWait(self.driver, time).until(EC.presence_of_all_elements_located(locator),
                                                          message=f"Can't find elements by locator {locator}")

        def find_clickable_element(self, locator, time=5):
            return WebDriverWait(self.driver, time).until(EC.element_to_be_clickable(locator),
                                                          message=f"Can't find elements by locator {locator}").click()

        def find_selected_element(self, locator, time=30):
            return WebDriverWait(self.driver, time).until(EC.frame_to_be_available_and_switch_to_it(locator),
                                                          message=f"Can't find elements by locator {locator}").click()

        def visibility_of_element(self, locator, time=30):
            return WebDriverWait(self.driver, time).until(EC.presence_of_element_located(locator),
                                                          message=f"Can't find elements by locator {locator}")

        def go_to_site(self):
            return self.driver.get(self.base_url)

        def invisible_element(self, locator, time=30):
            return WebDriverWait(self.driver, time).until(EC.invisibility_of_element(locator),
                                                          message=f"Can't find elements by locator {locator}")

        def close_another_window(self):
            self.driver.switch_to.window(self.driver.window_handles[0])
            self.driver.close()
            self.driver.switch_to.window(self.driver.window_handles[-1])

        def refresh(self):
            self.driver.refresh()

        def wait_for_loading(self):
            time.sleep(5)

        def teardown(self):
            yield self.driver
            self.driver.quit()


    except StaleElementReferenceException:
        pass
